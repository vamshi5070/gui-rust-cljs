goog.provide('reagent.debug');
reagent.debug.has_console = (typeof console !== 'undefined');
reagent.debug.tracking = false;
if((typeof reagent !== 'undefined') && (typeof reagent.debug !== 'undefined') && (typeof reagent.debug.warnings !== 'undefined')){
} else {
reagent.debug.warnings = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
}
if((typeof reagent !== 'undefined') && (typeof reagent.debug !== 'undefined') && (typeof reagent.debug.track_console !== 'undefined')){
} else {
reagent.debug.track_console = (function (){var o = ({});
(o.warn = (function() { 
var G__20844__delegate = function (args){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(reagent.debug.warnings,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"warn","warn",-436710552)], null),cljs.core.conj,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,args)], 0));
};
var G__20844 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__20845__i = 0, G__20845__a = new Array(arguments.length -  0);
while (G__20845__i < G__20845__a.length) {G__20845__a[G__20845__i] = arguments[G__20845__i + 0]; ++G__20845__i;}
  args = new cljs.core.IndexedSeq(G__20845__a,0,null);
} 
return G__20844__delegate.call(this,args);};
G__20844.cljs$lang$maxFixedArity = 0;
G__20844.cljs$lang$applyTo = (function (arglist__20846){
var args = cljs.core.seq(arglist__20846);
return G__20844__delegate(args);
});
G__20844.cljs$core$IFn$_invoke$arity$variadic = G__20844__delegate;
return G__20844;
})()
);

(o.error = (function() { 
var G__20847__delegate = function (args){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(reagent.debug.warnings,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"error","error",-978969032)], null),cljs.core.conj,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,args)], 0));
};
var G__20847 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__20848__i = 0, G__20848__a = new Array(arguments.length -  0);
while (G__20848__i < G__20848__a.length) {G__20848__a[G__20848__i] = arguments[G__20848__i + 0]; ++G__20848__i;}
  args = new cljs.core.IndexedSeq(G__20848__a,0,null);
} 
return G__20847__delegate.call(this,args);};
G__20847.cljs$lang$maxFixedArity = 0;
G__20847.cljs$lang$applyTo = (function (arglist__20849){
var args = cljs.core.seq(arglist__20849);
return G__20847__delegate(args);
});
G__20847.cljs$core$IFn$_invoke$arity$variadic = G__20847__delegate;
return G__20847;
})()
);

return o;
})();
}
reagent.debug.track_warnings = (function reagent$debug$track_warnings(f){
(reagent.debug.tracking = true);

cljs.core.reset_BANG_(reagent.debug.warnings,null);

(f.cljs$core$IFn$_invoke$arity$0 ? f.cljs$core$IFn$_invoke$arity$0() : f.call(null));

var warns = cljs.core.deref(reagent.debug.warnings);
cljs.core.reset_BANG_(reagent.debug.warnings,null);

(reagent.debug.tracking = false);

return warns;
});

//# sourceMappingURL=reagent.debug.js.map
