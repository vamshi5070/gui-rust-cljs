goog.provide('cljs.repl');
cljs.repl.print_doc = (function cljs$repl$print_doc(p__20138){
var map__20139 = p__20138;
var map__20139__$1 = cljs.core.__destructure_map(map__20139);
var m = map__20139__$1;
var n = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20139__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var nm = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20139__$1,new cljs.core.Keyword(null,"name","name",1843675177));
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["-------------------------"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function (){var or__5045__auto__ = new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return [(function (){var temp__5804__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__5804__auto__)){
var ns = temp__5804__auto__;
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(ns),"/"].join('');
} else {
return null;
}
})(),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('');
}
})()], 0));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Protocol"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__20140_20342 = cljs.core.seq(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__20141_20343 = null;
var count__20142_20344 = (0);
var i__20143_20345 = (0);
while(true){
if((i__20143_20345 < count__20142_20344)){
var f_20346 = chunk__20141_20343.cljs$core$IIndexed$_nth$arity$2(null,i__20143_20345);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["  ",f_20346], 0));


var G__20347 = seq__20140_20342;
var G__20348 = chunk__20141_20343;
var G__20349 = count__20142_20344;
var G__20350 = (i__20143_20345 + (1));
seq__20140_20342 = G__20347;
chunk__20141_20343 = G__20348;
count__20142_20344 = G__20349;
i__20143_20345 = G__20350;
continue;
} else {
var temp__5804__auto___20351 = cljs.core.seq(seq__20140_20342);
if(temp__5804__auto___20351){
var seq__20140_20352__$1 = temp__5804__auto___20351;
if(cljs.core.chunked_seq_QMARK_(seq__20140_20352__$1)){
var c__5568__auto___20353 = cljs.core.chunk_first(seq__20140_20352__$1);
var G__20354 = cljs.core.chunk_rest(seq__20140_20352__$1);
var G__20355 = c__5568__auto___20353;
var G__20356 = cljs.core.count(c__5568__auto___20353);
var G__20357 = (0);
seq__20140_20342 = G__20354;
chunk__20141_20343 = G__20355;
count__20142_20344 = G__20356;
i__20143_20345 = G__20357;
continue;
} else {
var f_20358 = cljs.core.first(seq__20140_20352__$1);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["  ",f_20358], 0));


var G__20359 = cljs.core.next(seq__20140_20352__$1);
var G__20360 = null;
var G__20361 = (0);
var G__20362 = (0);
seq__20140_20342 = G__20359;
chunk__20141_20343 = G__20360;
count__20142_20344 = G__20361;
i__20143_20345 = G__20362;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_20363 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__5045__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([arglists_20363], 0));
} else {
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first(arglists_20363)))?cljs.core.second(arglists_20363):arglists_20363)], 0));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Special Form"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m)], 0));

if(cljs.core.contains_QMARK_(m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n  Please see http://clojure.org/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join('')], 0));
} else {
return null;
}
} else {
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n  Please see http://clojure.org/special_forms#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('')], 0));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Macro"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Spec"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["REPL Special Function"], 0));
} else {
}

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m)], 0));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__20144_20365 = cljs.core.seq(new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__20145_20366 = null;
var count__20146_20367 = (0);
var i__20147_20368 = (0);
while(true){
if((i__20147_20368 < count__20146_20367)){
var vec__20158_20369 = chunk__20145_20366.cljs$core$IIndexed$_nth$arity$2(null,i__20147_20368);
var name_20370 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__20158_20369,(0),null);
var map__20161_20371 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__20158_20369,(1),null);
var map__20161_20372__$1 = cljs.core.__destructure_map(map__20161_20371);
var doc_20373 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20161_20372__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_20374 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20161_20372__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println();

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",name_20370], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",arglists_20374], 0));

if(cljs.core.truth_(doc_20373)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",doc_20373], 0));
} else {
}


var G__20375 = seq__20144_20365;
var G__20376 = chunk__20145_20366;
var G__20377 = count__20146_20367;
var G__20378 = (i__20147_20368 + (1));
seq__20144_20365 = G__20375;
chunk__20145_20366 = G__20376;
count__20146_20367 = G__20377;
i__20147_20368 = G__20378;
continue;
} else {
var temp__5804__auto___20379 = cljs.core.seq(seq__20144_20365);
if(temp__5804__auto___20379){
var seq__20144_20380__$1 = temp__5804__auto___20379;
if(cljs.core.chunked_seq_QMARK_(seq__20144_20380__$1)){
var c__5568__auto___20381 = cljs.core.chunk_first(seq__20144_20380__$1);
var G__20382 = cljs.core.chunk_rest(seq__20144_20380__$1);
var G__20383 = c__5568__auto___20381;
var G__20384 = cljs.core.count(c__5568__auto___20381);
var G__20385 = (0);
seq__20144_20365 = G__20382;
chunk__20145_20366 = G__20383;
count__20146_20367 = G__20384;
i__20147_20368 = G__20385;
continue;
} else {
var vec__20163_20386 = cljs.core.first(seq__20144_20380__$1);
var name_20387 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__20163_20386,(0),null);
var map__20166_20388 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__20163_20386,(1),null);
var map__20166_20389__$1 = cljs.core.__destructure_map(map__20166_20388);
var doc_20390 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20166_20389__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_20391 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20166_20389__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println();

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",name_20387], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",arglists_20391], 0));

if(cljs.core.truth_(doc_20390)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",doc_20390], 0));
} else {
}


var G__20392 = cljs.core.next(seq__20144_20380__$1);
var G__20393 = null;
var G__20394 = (0);
var G__20395 = (0);
seq__20144_20365 = G__20392;
chunk__20145_20366 = G__20393;
count__20146_20367 = G__20394;
i__20147_20368 = G__20395;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(n)){
var temp__5804__auto__ = cljs.spec.alpha.get_spec(cljs.core.symbol.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.ns_name(n)),cljs.core.name(nm)));
if(cljs.core.truth_(temp__5804__auto__)){
var fnspec = temp__5804__auto__;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Spec"], 0));

var seq__20169 = cljs.core.seq(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"args","args",1315556576),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword(null,"fn","fn",-1175266204)], null));
var chunk__20170 = null;
var count__20171 = (0);
var i__20172 = (0);
while(true){
if((i__20172 < count__20171)){
var role = chunk__20170.cljs$core$IIndexed$_nth$arity$2(null,i__20172);
var temp__5804__auto___20396__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(fnspec,role);
if(cljs.core.truth_(temp__5804__auto___20396__$1)){
var spec_20397 = temp__5804__auto___20396__$1;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n ",cljs.core.name(role),":"].join(''),cljs.spec.alpha.describe(spec_20397)], 0));
} else {
}


var G__20398 = seq__20169;
var G__20399 = chunk__20170;
var G__20400 = count__20171;
var G__20401 = (i__20172 + (1));
seq__20169 = G__20398;
chunk__20170 = G__20399;
count__20171 = G__20400;
i__20172 = G__20401;
continue;
} else {
var temp__5804__auto____$1 = cljs.core.seq(seq__20169);
if(temp__5804__auto____$1){
var seq__20169__$1 = temp__5804__auto____$1;
if(cljs.core.chunked_seq_QMARK_(seq__20169__$1)){
var c__5568__auto__ = cljs.core.chunk_first(seq__20169__$1);
var G__20402 = cljs.core.chunk_rest(seq__20169__$1);
var G__20403 = c__5568__auto__;
var G__20404 = cljs.core.count(c__5568__auto__);
var G__20405 = (0);
seq__20169 = G__20402;
chunk__20170 = G__20403;
count__20171 = G__20404;
i__20172 = G__20405;
continue;
} else {
var role = cljs.core.first(seq__20169__$1);
var temp__5804__auto___20406__$2 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(fnspec,role);
if(cljs.core.truth_(temp__5804__auto___20406__$2)){
var spec_20407 = temp__5804__auto___20406__$2;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n ",cljs.core.name(role),":"].join(''),cljs.spec.alpha.describe(spec_20407)], 0));
} else {
}


var G__20408 = cljs.core.next(seq__20169__$1);
var G__20409 = null;
var G__20410 = (0);
var G__20411 = (0);
seq__20169 = G__20408;
chunk__20170 = G__20409;
count__20171 = G__20410;
i__20172 = G__20411;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Constructs a data representation for a Error with keys:
 *  :cause - root cause message
 *  :phase - error phase
 *  :via - cause chain, with cause keys:
 *           :type - exception class symbol
 *           :message - exception message
 *           :data - ex-data
 *           :at - top stack element
 *  :trace - root cause stack elements
 */
cljs.repl.Error__GT_map = (function cljs$repl$Error__GT_map(o){
var base = (function (t){
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),(((t instanceof cljs.core.ExceptionInfo))?new cljs.core.Symbol("cljs.core","ExceptionInfo","cljs.core/ExceptionInfo",701839050,null):(((t instanceof Error))?cljs.core.symbol.cljs$core$IFn$_invoke$arity$2("js",t.name):null
))], null),(function (){var temp__5804__auto__ = cljs.core.ex_message(t);
if(cljs.core.truth_(temp__5804__auto__)){
var msg = temp__5804__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"message","message",-406056002),msg], null);
} else {
return null;
}
})(),(function (){var temp__5804__auto__ = cljs.core.ex_data(t);
if(cljs.core.truth_(temp__5804__auto__)){
var ed = temp__5804__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),ed], null);
} else {
return null;
}
})()], 0));
});
var via = (function (){var via = cljs.core.PersistentVector.EMPTY;
var t = o;
while(true){
if(cljs.core.truth_(t)){
var G__20412 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(via,t);
var G__20413 = cljs.core.ex_cause(t);
via = G__20412;
t = G__20413;
continue;
} else {
return via;
}
break;
}
})();
var root = cljs.core.peek(via);
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"via","via",-1904457336),cljs.core.vec(cljs.core.map.cljs$core$IFn$_invoke$arity$2(base,via)),new cljs.core.Keyword(null,"trace","trace",-1082747415),null], null),(function (){var temp__5804__auto__ = cljs.core.ex_message(root);
if(cljs.core.truth_(temp__5804__auto__)){
var root_msg = temp__5804__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"cause","cause",231901252),root_msg], null);
} else {
return null;
}
})(),(function (){var temp__5804__auto__ = cljs.core.ex_data(root);
if(cljs.core.truth_(temp__5804__auto__)){
var data = temp__5804__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),data], null);
} else {
return null;
}
})(),(function (){var temp__5804__auto__ = new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358).cljs$core$IFn$_invoke$arity$1(cljs.core.ex_data(o));
if(cljs.core.truth_(temp__5804__auto__)){
var phase = temp__5804__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"phase","phase",575722892),phase], null);
} else {
return null;
}
})()], 0));
});
/**
 * Returns an analysis of the phase, error, cause, and location of an error that occurred
 *   based on Throwable data, as returned by Throwable->map. All attributes other than phase
 *   are optional:
 *  :clojure.error/phase - keyword phase indicator, one of:
 *    :read-source :compile-syntax-check :compilation :macro-syntax-check :macroexpansion
 *    :execution :read-eval-result :print-eval-result
 *  :clojure.error/source - file name (no path)
 *  :clojure.error/line - integer line number
 *  :clojure.error/column - integer column number
 *  :clojure.error/symbol - symbol being expanded/compiled/invoked
 *  :clojure.error/class - cause exception class symbol
 *  :clojure.error/cause - cause exception message
 *  :clojure.error/spec - explain-data for spec error
 */
cljs.repl.ex_triage = (function cljs$repl$ex_triage(datafied_throwable){
var map__20182 = datafied_throwable;
var map__20182__$1 = cljs.core.__destructure_map(map__20182);
var via = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20182__$1,new cljs.core.Keyword(null,"via","via",-1904457336));
var trace = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20182__$1,new cljs.core.Keyword(null,"trace","trace",-1082747415));
var phase = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__20182__$1,new cljs.core.Keyword(null,"phase","phase",575722892),new cljs.core.Keyword(null,"execution","execution",253283524));
var map__20183 = cljs.core.last(via);
var map__20183__$1 = cljs.core.__destructure_map(map__20183);
var type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20183__$1,new cljs.core.Keyword(null,"type","type",1174270348));
var message = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20183__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var data = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20183__$1,new cljs.core.Keyword(null,"data","data",-232669377));
var map__20184 = data;
var map__20184__$1 = cljs.core.__destructure_map(map__20184);
var problems = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20184__$1,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814));
var fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20184__$1,new cljs.core.Keyword("cljs.spec.alpha","fn","cljs.spec.alpha/fn",408600443));
var caller = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20184__$1,new cljs.core.Keyword("cljs.spec.test.alpha","caller","cljs.spec.test.alpha/caller",-398302390));
var map__20185 = new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.first(via));
var map__20185__$1 = cljs.core.__destructure_map(map__20185);
var top_data = map__20185__$1;
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20185__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3((function (){var G__20186 = phase;
var G__20186__$1 = (((G__20186 instanceof cljs.core.Keyword))?G__20186.fqn:null);
switch (G__20186__$1) {
case "read-source":
var map__20187 = data;
var map__20187__$1 = cljs.core.__destructure_map(map__20187);
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20187__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20187__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var G__20188 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.second(via)),top_data], 0));
var G__20188__$1 = (cljs.core.truth_(source)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__20188,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__20188);
var G__20188__$2 = (cljs.core.truth_((function (){var fexpr__20189 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__20189.cljs$core$IFn$_invoke$arity$1 ? fexpr__20189.cljs$core$IFn$_invoke$arity$1(source) : fexpr__20189.call(null,source));
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(G__20188__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__20188__$1);
if(cljs.core.truth_(message)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__20188__$2,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__20188__$2;
}

break;
case "compile-syntax-check":
case "compilation":
case "macro-syntax-check":
case "macroexpansion":
var G__20190 = top_data;
var G__20190__$1 = (cljs.core.truth_(source)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__20190,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__20190);
var G__20190__$2 = (cljs.core.truth_((function (){var fexpr__20191 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__20191.cljs$core$IFn$_invoke$arity$1 ? fexpr__20191.cljs$core$IFn$_invoke$arity$1(source) : fexpr__20191.call(null,source));
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(G__20190__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__20190__$1);
var G__20190__$3 = (cljs.core.truth_(type)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__20190__$2,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__20190__$2);
var G__20190__$4 = (cljs.core.truth_(message)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__20190__$3,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__20190__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__20190__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__20190__$4;
}

break;
case "read-eval-result":
case "print-eval-result":
var vec__20209 = cljs.core.first(trace);
var source__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__20209,(0),null);
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__20209,(1),null);
var file = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__20209,(2),null);
var line = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__20209,(3),null);
var G__20212 = top_data;
var G__20212__$1 = (cljs.core.truth_(line)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__20212,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),line):G__20212);
var G__20212__$2 = (cljs.core.truth_(file)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__20212__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file):G__20212__$1);
var G__20212__$3 = (cljs.core.truth_((function (){var and__5043__auto__ = source__$1;
if(cljs.core.truth_(and__5043__auto__)){
return method;
} else {
return and__5043__auto__;
}
})())?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__20212__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null))):G__20212__$2);
var G__20212__$4 = (cljs.core.truth_(type)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__20212__$3,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__20212__$3);
if(cljs.core.truth_(message)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__20212__$4,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__20212__$4;
}

break;
case "execution":
var vec__20220 = cljs.core.first(trace);
var source__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__20220,(0),null);
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__20220,(1),null);
var file = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__20220,(2),null);
var line = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__20220,(3),null);
var file__$1 = cljs.core.first(cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p1__20180_SHARP_){
var or__5045__auto__ = (p1__20180_SHARP_ == null);
if(or__5045__auto__){
return or__5045__auto__;
} else {
var fexpr__20223 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__20223.cljs$core$IFn$_invoke$arity$1 ? fexpr__20223.cljs$core$IFn$_invoke$arity$1(p1__20180_SHARP_) : fexpr__20223.call(null,p1__20180_SHARP_));
}
}),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(caller),file], null)));
var err_line = (function (){var or__5045__auto__ = new cljs.core.Keyword(null,"line","line",212345235).cljs$core$IFn$_invoke$arity$1(caller);
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return line;
}
})();
var G__20230 = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type], null);
var G__20230__$1 = (cljs.core.truth_(err_line)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__20230,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),err_line):G__20230);
var G__20230__$2 = (cljs.core.truth_(message)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__20230__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__20230__$1);
var G__20230__$3 = (cljs.core.truth_((function (){var or__5045__auto__ = fn;
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
var and__5043__auto__ = source__$1;
if(cljs.core.truth_(and__5043__auto__)){
return method;
} else {
return and__5043__auto__;
}
}
})())?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__20230__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(function (){var or__5045__auto__ = fn;
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return (new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null));
}
})()):G__20230__$2);
var G__20230__$4 = (cljs.core.truth_(file__$1)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__20230__$3,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file__$1):G__20230__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__20230__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__20230__$4;
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__20186__$1)].join('')));

}
})(),new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358),phase);
});
/**
 * Returns a string from exception data, as produced by ex-triage.
 *   The first line summarizes the exception phase and location.
 *   The subsequent lines describe the cause.
 */
cljs.repl.ex_str = (function cljs$repl$ex_str(p__20276){
var map__20280 = p__20276;
var map__20280__$1 = cljs.core.__destructure_map(map__20280);
var triage_data = map__20280__$1;
var phase = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20280__$1,new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358));
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20280__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20280__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20280__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var symbol = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20280__$1,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994));
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20280__$1,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890));
var cause = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20280__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742));
var spec = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20280__$1,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595));
var loc = [cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__5045__auto__ = source;
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return "<cljs repl>";
}
})()),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__5045__auto__ = line;
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return (1);
}
})()),(cljs.core.truth_(column)?[":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column)].join(''):"")].join('');
var class_name = cljs.core.name((function (){var or__5045__auto__ = class$;
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return "";
}
})());
var simple_class = class_name;
var cause_type = ((cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["RuntimeException",null,"Exception",null], null), null),simple_class))?"":[" (",simple_class,")"].join(''));
var format = goog.string.format;
var G__20300 = phase;
var G__20300__$1 = (((G__20300 instanceof cljs.core.Keyword))?G__20300.fqn:null);
switch (G__20300__$1) {
case "read-source":
return (format.cljs$core$IFn$_invoke$arity$3 ? format.cljs$core$IFn$_invoke$arity$3("Syntax error reading source at (%s).\n%s\n",loc,cause) : format.call(null,"Syntax error reading source at (%s).\n%s\n",loc,cause));

break;
case "macro-syntax-check":
var G__20301 = "Syntax error macroexpanding %sat (%s).\n%s";
var G__20302 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__20303 = loc;
var G__20304 = (cljs.core.truth_(spec)?(function (){var sb__5690__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__20305_20435 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__20306_20436 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__20307_20437 = true;
var _STAR_print_fn_STAR__temp_val__20308_20438 = (function (x__5691__auto__){
return sb__5690__auto__.append(x__5691__auto__);
});
(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__20307_20437);

(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__20308_20438);

try{cljs.spec.alpha.explain_out(cljs.core.update.cljs$core$IFn$_invoke$arity$3(spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),(function (probs){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__20267_SHARP_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(p1__20267_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
}),probs);
}))
);
}finally {(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__20306_20436);

(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__20305_20435);
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__5690__auto__);
})():(format.cljs$core$IFn$_invoke$arity$2 ? format.cljs$core$IFn$_invoke$arity$2("%s\n",cause) : format.call(null,"%s\n",cause)));
return (format.cljs$core$IFn$_invoke$arity$4 ? format.cljs$core$IFn$_invoke$arity$4(G__20301,G__20302,G__20303,G__20304) : format.call(null,G__20301,G__20302,G__20303,G__20304));

break;
case "macroexpansion":
var G__20310 = "Unexpected error%s macroexpanding %sat (%s).\n%s\n";
var G__20311 = cause_type;
var G__20312 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__20313 = loc;
var G__20314 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__20310,G__20311,G__20312,G__20313,G__20314) : format.call(null,G__20310,G__20311,G__20312,G__20313,G__20314));

break;
case "compile-syntax-check":
var G__20315 = "Syntax error%s compiling %sat (%s).\n%s\n";
var G__20316 = cause_type;
var G__20317 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__20318 = loc;
var G__20319 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__20315,G__20316,G__20317,G__20318,G__20319) : format.call(null,G__20315,G__20316,G__20317,G__20318,G__20319));

break;
case "compilation":
var G__20320 = "Unexpected error%s compiling %sat (%s).\n%s\n";
var G__20321 = cause_type;
var G__20322 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__20323 = loc;
var G__20324 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__20320,G__20321,G__20322,G__20323,G__20324) : format.call(null,G__20320,G__20321,G__20322,G__20323,G__20324));

break;
case "read-eval-result":
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5("Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause) : format.call(null,"Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause));

break;
case "print-eval-result":
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5("Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause) : format.call(null,"Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause));

break;
case "execution":
if(cljs.core.truth_(spec)){
var G__20326 = "Execution error - invalid arguments to %s at (%s).\n%s";
var G__20327 = symbol;
var G__20328 = loc;
var G__20329 = (function (){var sb__5690__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__20330_20439 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__20331_20440 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__20332_20441 = true;
var _STAR_print_fn_STAR__temp_val__20333_20442 = (function (x__5691__auto__){
return sb__5690__auto__.append(x__5691__auto__);
});
(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__20332_20441);

(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__20333_20442);

try{cljs.spec.alpha.explain_out(cljs.core.update.cljs$core$IFn$_invoke$arity$3(spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),(function (probs){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__20271_SHARP_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(p1__20271_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
}),probs);
}))
);
}finally {(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__20331_20440);

(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__20330_20439);
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__5690__auto__);
})();
return (format.cljs$core$IFn$_invoke$arity$4 ? format.cljs$core$IFn$_invoke$arity$4(G__20326,G__20327,G__20328,G__20329) : format.call(null,G__20326,G__20327,G__20328,G__20329));
} else {
var G__20334 = "Execution error%s at %s(%s).\n%s\n";
var G__20335 = cause_type;
var G__20336 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__20337 = loc;
var G__20338 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__20334,G__20335,G__20336,G__20337,G__20338) : format.call(null,G__20334,G__20335,G__20336,G__20337,G__20338));
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__20300__$1)].join('')));

}
});
cljs.repl.error__GT_str = (function cljs$repl$error__GT_str(error){
return cljs.repl.ex_str(cljs.repl.ex_triage(cljs.repl.Error__GT_map(error)));
});

//# sourceMappingURL=cljs.repl.js.map
