goog.provide('reagent.dom');
var module$node_modules$react_dom$index=shadow.js.require("module$node_modules$react_dom$index", {});
if((typeof reagent !== 'undefined') && (typeof reagent.dom !== 'undefined') && (typeof reagent.dom.roots !== 'undefined')){
} else {
reagent.dom.roots = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
}
reagent.dom.unmount_comp = (function reagent$dom$unmount_comp(container){
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(reagent.dom.roots,cljs.core.dissoc,container);

return module$node_modules$react_dom$index.unmountComponentAtNode(container);
});
reagent.dom.render_comp = (function reagent$dom$render_comp(comp,container,callback){
var _STAR_always_update_STAR__orig_val__21841 = reagent.impl.util._STAR_always_update_STAR_;
var _STAR_always_update_STAR__temp_val__21842 = true;
(reagent.impl.util._STAR_always_update_STAR_ = _STAR_always_update_STAR__temp_val__21842);

try{return module$node_modules$react_dom$index.render((comp.cljs$core$IFn$_invoke$arity$0 ? comp.cljs$core$IFn$_invoke$arity$0() : comp.call(null)),container,(function (){
var _STAR_always_update_STAR__orig_val__21847 = reagent.impl.util._STAR_always_update_STAR_;
var _STAR_always_update_STAR__temp_val__21848 = false;
(reagent.impl.util._STAR_always_update_STAR_ = _STAR_always_update_STAR__temp_val__21848);

try{cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(reagent.dom.roots,cljs.core.assoc,container,comp);

reagent.impl.batching.flush_after_render();

if((!((callback == null)))){
return (callback.cljs$core$IFn$_invoke$arity$0 ? callback.cljs$core$IFn$_invoke$arity$0() : callback.call(null));
} else {
return null;
}
}finally {(reagent.impl.util._STAR_always_update_STAR_ = _STAR_always_update_STAR__orig_val__21847);
}}));
}finally {(reagent.impl.util._STAR_always_update_STAR_ = _STAR_always_update_STAR__orig_val__21841);
}});
reagent.dom.re_render_component = (function reagent$dom$re_render_component(comp,container){
return reagent.dom.render_comp(comp,container,null);
});
/**
 * Render a Reagent component into the DOM. The first argument may be
 *   either a vector (using Reagent's Hiccup syntax), or a React element.
 *   The second argument should be a DOM node.
 * 
 *   Optionally takes a callback that is called when the component is in place.
 * 
 *   Returns the mounted component instance.
 */
reagent.dom.render = (function reagent$dom$render(var_args){
var G__21851 = arguments.length;
switch (G__21851) {
case 2:
return reagent.dom.render.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return reagent.dom.render.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(reagent.dom.render.cljs$core$IFn$_invoke$arity$2 = (function (comp,container){
return reagent.dom.render.cljs$core$IFn$_invoke$arity$3(comp,container,reagent.impl.template.default_compiler);
}));

(reagent.dom.render.cljs$core$IFn$_invoke$arity$3 = (function (comp,container,callback_or_compiler){
reagent.ratom.flush_BANG_();

var vec__21852 = ((cljs.core.fn_QMARK_(callback_or_compiler))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [reagent.impl.template.default_compiler,callback_or_compiler], null):new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [callback_or_compiler,new cljs.core.Keyword(null,"callback","callback",-705136228).cljs$core$IFn$_invoke$arity$1(callback_or_compiler)], null));
var compiler = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__21852,(0),null);
var callback = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__21852,(1),null);
var f = (function (){
return reagent.impl.protocols.as_element(compiler,((cljs.core.fn_QMARK_(comp))?(comp.cljs$core$IFn$_invoke$arity$0 ? comp.cljs$core$IFn$_invoke$arity$0() : comp.call(null)):comp));
});
return reagent.dom.render_comp(f,container,callback);
}));

(reagent.dom.render.cljs$lang$maxFixedArity = 3);

/**
 * Remove a component from the given DOM node.
 */
reagent.dom.unmount_component_at_node = (function reagent$dom$unmount_component_at_node(container){
return reagent.dom.unmount_comp(container);
});
/**
 * Returns the root DOM node of a mounted component.
 */
reagent.dom.dom_node = (function reagent$dom$dom_node(this$){
return module$node_modules$react_dom$index.findDOMNode(this$);
});
/**
 * Force re-rendering of all mounted Reagent components. This is
 *   probably only useful in a development environment, when you want to
 *   update components in response to some dynamic changes to code.
 * 
 *   Note that force-update-all may not update root components. This
 *   happens if a component 'foo' is mounted with `(render [foo])` (since
 *   functions are passed by value, and not by reference, in
 *   ClojureScript). To get around this you'll have to introduce a layer
 *   of indirection, for example by using `(render [#'foo])` instead.
 */
reagent.dom.force_update_all = (function reagent$dom$force_update_all(){
reagent.ratom.flush_BANG_();

var seq__21857_21878 = cljs.core.seq(cljs.core.deref(reagent.dom.roots));
var chunk__21858_21879 = null;
var count__21859_21880 = (0);
var i__21860_21881 = (0);
while(true){
if((i__21860_21881 < count__21859_21880)){
var vec__21869_21882 = chunk__21858_21879.cljs$core$IIndexed$_nth$arity$2(null,i__21860_21881);
var container_21883 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__21869_21882,(0),null);
var comp_21884 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__21869_21882,(1),null);
reagent.dom.re_render_component(comp_21884,container_21883);


var G__21885 = seq__21857_21878;
var G__21886 = chunk__21858_21879;
var G__21887 = count__21859_21880;
var G__21888 = (i__21860_21881 + (1));
seq__21857_21878 = G__21885;
chunk__21858_21879 = G__21886;
count__21859_21880 = G__21887;
i__21860_21881 = G__21888;
continue;
} else {
var temp__5804__auto___21889 = cljs.core.seq(seq__21857_21878);
if(temp__5804__auto___21889){
var seq__21857_21890__$1 = temp__5804__auto___21889;
if(cljs.core.chunked_seq_QMARK_(seq__21857_21890__$1)){
var c__5568__auto___21891 = cljs.core.chunk_first(seq__21857_21890__$1);
var G__21892 = cljs.core.chunk_rest(seq__21857_21890__$1);
var G__21893 = c__5568__auto___21891;
var G__21894 = cljs.core.count(c__5568__auto___21891);
var G__21895 = (0);
seq__21857_21878 = G__21892;
chunk__21858_21879 = G__21893;
count__21859_21880 = G__21894;
i__21860_21881 = G__21895;
continue;
} else {
var vec__21872_21896 = cljs.core.first(seq__21857_21890__$1);
var container_21897 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__21872_21896,(0),null);
var comp_21898 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__21872_21896,(1),null);
reagent.dom.re_render_component(comp_21898,container_21897);


var G__21899 = cljs.core.next(seq__21857_21890__$1);
var G__21900 = null;
var G__21901 = (0);
var G__21902 = (0);
seq__21857_21878 = G__21899;
chunk__21858_21879 = G__21900;
count__21859_21880 = G__21901;
i__21860_21881 = G__21902;
continue;
}
} else {
}
}
break;
}

return reagent.impl.batching.flush_after_render();
});

//# sourceMappingURL=reagent.dom.js.map
