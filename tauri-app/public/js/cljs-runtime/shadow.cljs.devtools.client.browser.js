goog.provide('shadow.cljs.devtools.client.browser');
shadow.cljs.devtools.client.browser.devtools_msg = (function shadow$cljs$devtools$client$browser$devtools_msg(var_args){
var args__5775__auto__ = [];
var len__5769__auto___21344 = arguments.length;
var i__5770__auto___21345 = (0);
while(true){
if((i__5770__auto___21345 < len__5769__auto___21344)){
args__5775__auto__.push((arguments[i__5770__auto___21345]));

var G__21346 = (i__5770__auto___21345 + (1));
i__5770__auto___21345 = G__21346;
continue;
} else {
}
break;
}

var argseq__5776__auto__ = ((((1) < args__5775__auto__.length))?(new cljs.core.IndexedSeq(args__5775__auto__.slice((1)),(0),null)):null);
return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__5776__auto__);
});

(shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic = (function (msg,args){
if(shadow.cljs.devtools.client.env.log){
if(cljs.core.seq(shadow.cljs.devtools.client.env.log_style)){
return console.log.apply(console,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [["%cshadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join(''),shadow.cljs.devtools.client.env.log_style], null),args)));
} else {
return console.log.apply(console,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [["shadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join('')], null),args)));
}
} else {
return null;
}
}));

(shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$applyTo = (function (seq20918){
var G__20919 = cljs.core.first(seq20918);
var seq20918__$1 = cljs.core.next(seq20918);
var self__5754__auto__ = this;
return self__5754__auto__.cljs$core$IFn$_invoke$arity$variadic(G__20919,seq20918__$1);
}));

shadow.cljs.devtools.client.browser.script_eval = (function shadow$cljs$devtools$client$browser$script_eval(code){
return goog.globalEval(code);
});
shadow.cljs.devtools.client.browser.do_js_load = (function shadow$cljs$devtools$client$browser$do_js_load(sources){
var seq__20920 = cljs.core.seq(sources);
var chunk__20921 = null;
var count__20922 = (0);
var i__20923 = (0);
while(true){
if((i__20923 < count__20922)){
var map__20938 = chunk__20921.cljs$core$IIndexed$_nth$arity$2(null,i__20923);
var map__20938__$1 = cljs.core.__destructure_map(map__20938);
var src = map__20938__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20938__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20938__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20938__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20938__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1($CLJS.SHADOW_ENV.scriptBase),cljs.core.str.cljs$core$IFn$_invoke$arity$1(output_name)].join(''));
}catch (e20939){var e_21347 = e20939;
if(shadow.cljs.devtools.client.env.log){
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_21347);
} else {
}

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_21347.message)].join('')));
}

var G__21348 = seq__20920;
var G__21349 = chunk__20921;
var G__21350 = count__20922;
var G__21351 = (i__20923 + (1));
seq__20920 = G__21348;
chunk__20921 = G__21349;
count__20922 = G__21350;
i__20923 = G__21351;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__20920);
if(temp__5804__auto__){
var seq__20920__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__20920__$1)){
var c__5568__auto__ = cljs.core.chunk_first(seq__20920__$1);
var G__21352 = cljs.core.chunk_rest(seq__20920__$1);
var G__21353 = c__5568__auto__;
var G__21354 = cljs.core.count(c__5568__auto__);
var G__21355 = (0);
seq__20920 = G__21352;
chunk__20921 = G__21353;
count__20922 = G__21354;
i__20923 = G__21355;
continue;
} else {
var map__20940 = cljs.core.first(seq__20920__$1);
var map__20940__$1 = cljs.core.__destructure_map(map__20940);
var src = map__20940__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20940__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20940__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20940__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20940__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1($CLJS.SHADOW_ENV.scriptBase),cljs.core.str.cljs$core$IFn$_invoke$arity$1(output_name)].join(''));
}catch (e20941){var e_21357 = e20941;
if(shadow.cljs.devtools.client.env.log){
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_21357);
} else {
}

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_21357.message)].join('')));
}

var G__21358 = cljs.core.next(seq__20920__$1);
var G__21359 = null;
var G__21360 = (0);
var G__21361 = (0);
seq__20920 = G__21358;
chunk__20921 = G__21359;
count__20922 = G__21360;
i__20923 = G__21361;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.do_js_reload = (function shadow$cljs$devtools$client$browser$do_js_reload(msg,sources,complete_fn,failure_fn){
return shadow.cljs.devtools.client.env.do_js_reload.cljs$core$IFn$_invoke$arity$4(cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(msg,new cljs.core.Keyword(null,"log-missing-fn","log-missing-fn",732676765),(function (fn_sym){
return null;
}),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"log-call-async","log-call-async",183826192),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call async ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
}),new cljs.core.Keyword(null,"log-call","log-call",412404391),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
})], 0)),(function (){
return shadow.cljs.devtools.client.browser.do_js_load(sources);
}),complete_fn,failure_fn);
});
/**
 * when (require '["some-str" :as x]) is done at the REPL we need to manually call the shadow.js.require for it
 * since the file only adds the shadow$provide. only need to do this for shadow-js.
 */
shadow.cljs.devtools.client.browser.do_js_requires = (function shadow$cljs$devtools$client$browser$do_js_requires(js_requires){
var seq__20944 = cljs.core.seq(js_requires);
var chunk__20945 = null;
var count__20946 = (0);
var i__20947 = (0);
while(true){
if((i__20947 < count__20946)){
var js_ns = chunk__20945.cljs$core$IIndexed$_nth$arity$2(null,i__20947);
var require_str_21362 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_21362);


var G__21363 = seq__20944;
var G__21364 = chunk__20945;
var G__21365 = count__20946;
var G__21366 = (i__20947 + (1));
seq__20944 = G__21363;
chunk__20945 = G__21364;
count__20946 = G__21365;
i__20947 = G__21366;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__20944);
if(temp__5804__auto__){
var seq__20944__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__20944__$1)){
var c__5568__auto__ = cljs.core.chunk_first(seq__20944__$1);
var G__21369 = cljs.core.chunk_rest(seq__20944__$1);
var G__21370 = c__5568__auto__;
var G__21371 = cljs.core.count(c__5568__auto__);
var G__21372 = (0);
seq__20944 = G__21369;
chunk__20945 = G__21370;
count__20946 = G__21371;
i__20947 = G__21372;
continue;
} else {
var js_ns = cljs.core.first(seq__20944__$1);
var require_str_21373 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_21373);


var G__21374 = cljs.core.next(seq__20944__$1);
var G__21375 = null;
var G__21376 = (0);
var G__21377 = (0);
seq__20944 = G__21374;
chunk__20945 = G__21375;
count__20946 = G__21376;
i__20947 = G__21377;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.handle_build_complete = (function shadow$cljs$devtools$client$browser$handle_build_complete(runtime,p__20954){
var map__20955 = p__20954;
var map__20955__$1 = cljs.core.__destructure_map(map__20955);
var msg = map__20955__$1;
var info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20955__$1,new cljs.core.Keyword(null,"info","info",-317069002));
var reload_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20955__$1,new cljs.core.Keyword(null,"reload-info","reload-info",1648088086));
var warnings = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.distinct.cljs$core$IFn$_invoke$arity$1((function (){var iter__5523__auto__ = (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__20956(s__20957){
return (new cljs.core.LazySeq(null,(function (){
var s__20957__$1 = s__20957;
while(true){
var temp__5804__auto__ = cljs.core.seq(s__20957__$1);
if(temp__5804__auto__){
var xs__6360__auto__ = temp__5804__auto__;
var map__20963 = cljs.core.first(xs__6360__auto__);
var map__20963__$1 = cljs.core.__destructure_map(map__20963);
var src = map__20963__$1;
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20963__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var warnings = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20963__$1,new cljs.core.Keyword(null,"warnings","warnings",-735437651));
if(cljs.core.not(new cljs.core.Keyword(null,"from-jar","from-jar",1050932827).cljs$core$IFn$_invoke$arity$1(src))){
var iterys__5519__auto__ = ((function (s__20957__$1,map__20963,map__20963__$1,src,resource_name,warnings,xs__6360__auto__,temp__5804__auto__,map__20955,map__20955__$1,msg,info,reload_info){
return (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__20956_$_iter__20958(s__20959){
return (new cljs.core.LazySeq(null,((function (s__20957__$1,map__20963,map__20963__$1,src,resource_name,warnings,xs__6360__auto__,temp__5804__auto__,map__20955,map__20955__$1,msg,info,reload_info){
return (function (){
var s__20959__$1 = s__20959;
while(true){
var temp__5804__auto____$1 = cljs.core.seq(s__20959__$1);
if(temp__5804__auto____$1){
var s__20959__$2 = temp__5804__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__20959__$2)){
var c__5521__auto__ = cljs.core.chunk_first(s__20959__$2);
var size__5522__auto__ = cljs.core.count(c__5521__auto__);
var b__20961 = cljs.core.chunk_buffer(size__5522__auto__);
if((function (){var i__20960 = (0);
while(true){
if((i__20960 < size__5522__auto__)){
var warning = cljs.core._nth(c__5521__auto__,i__20960);
cljs.core.chunk_append(b__20961,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name));

var G__21378 = (i__20960 + (1));
i__20960 = G__21378;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__20961),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__20956_$_iter__20958(cljs.core.chunk_rest(s__20959__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__20961),null);
}
} else {
var warning = cljs.core.first(s__20959__$2);
return cljs.core.cons(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__20956_$_iter__20958(cljs.core.rest(s__20959__$2)));
}
} else {
return null;
}
break;
}
});})(s__20957__$1,map__20963,map__20963__$1,src,resource_name,warnings,xs__6360__auto__,temp__5804__auto__,map__20955,map__20955__$1,msg,info,reload_info))
,null,null));
});})(s__20957__$1,map__20963,map__20963__$1,src,resource_name,warnings,xs__6360__auto__,temp__5804__auto__,map__20955,map__20955__$1,msg,info,reload_info))
;
var fs__5520__auto__ = cljs.core.seq(iterys__5519__auto__(warnings));
if(fs__5520__auto__){
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(fs__5520__auto__,shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__20956(cljs.core.rest(s__20957__$1)));
} else {
var G__21379 = cljs.core.rest(s__20957__$1);
s__20957__$1 = G__21379;
continue;
}
} else {
var G__21380 = cljs.core.rest(s__20957__$1);
s__20957__$1 = G__21380;
continue;
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__5523__auto__(new cljs.core.Keyword(null,"sources","sources",-321166424).cljs$core$IFn$_invoke$arity$1(info));
})()));
if(shadow.cljs.devtools.client.env.log){
var seq__20968_21381 = cljs.core.seq(warnings);
var chunk__20969_21382 = null;
var count__20970_21383 = (0);
var i__20971_21384 = (0);
while(true){
if((i__20971_21384 < count__20970_21383)){
var map__20974_21385 = chunk__20969_21382.cljs$core$IIndexed$_nth$arity$2(null,i__20971_21384);
var map__20974_21386__$1 = cljs.core.__destructure_map(map__20974_21385);
var w_21387 = map__20974_21386__$1;
var msg_21388__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20974_21386__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_21389 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20974_21386__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_21390 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20974_21386__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_21391 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20974_21386__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_21391)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_21389),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_21390),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_21388__$1)].join(''));


var G__21396 = seq__20968_21381;
var G__21397 = chunk__20969_21382;
var G__21398 = count__20970_21383;
var G__21399 = (i__20971_21384 + (1));
seq__20968_21381 = G__21396;
chunk__20969_21382 = G__21397;
count__20970_21383 = G__21398;
i__20971_21384 = G__21399;
continue;
} else {
var temp__5804__auto___21400 = cljs.core.seq(seq__20968_21381);
if(temp__5804__auto___21400){
var seq__20968_21401__$1 = temp__5804__auto___21400;
if(cljs.core.chunked_seq_QMARK_(seq__20968_21401__$1)){
var c__5568__auto___21402 = cljs.core.chunk_first(seq__20968_21401__$1);
var G__21403 = cljs.core.chunk_rest(seq__20968_21401__$1);
var G__21404 = c__5568__auto___21402;
var G__21405 = cljs.core.count(c__5568__auto___21402);
var G__21406 = (0);
seq__20968_21381 = G__21403;
chunk__20969_21382 = G__21404;
count__20970_21383 = G__21405;
i__20971_21384 = G__21406;
continue;
} else {
var map__20975_21407 = cljs.core.first(seq__20968_21401__$1);
var map__20975_21408__$1 = cljs.core.__destructure_map(map__20975_21407);
var w_21409 = map__20975_21408__$1;
var msg_21410__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20975_21408__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_21411 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20975_21408__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_21412 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20975_21408__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_21413 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20975_21408__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_21413)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_21411),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_21412),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_21410__$1)].join(''));


var G__21414 = cljs.core.next(seq__20968_21401__$1);
var G__21415 = null;
var G__21416 = (0);
var G__21417 = (0);
seq__20968_21381 = G__21414;
chunk__20969_21382 = G__21415;
count__20970_21383 = G__21416;
i__20971_21384 = G__21417;
continue;
}
} else {
}
}
break;
}
} else {
}

if((!(shadow.cljs.devtools.client.env.autoload))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(((cljs.core.empty_QMARK_(warnings)) || (shadow.cljs.devtools.client.env.ignore_warnings))){
var sources_to_get = shadow.cljs.devtools.client.env.filter_reload_sources(info,reload_info);
if(cljs.core.not(cljs.core.seq(sources_to_get))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"after-load","after-load",-1278503285)], null)))){
} else {
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("reloading code but no :after-load hooks are configured!",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["https://shadow-cljs.github.io/docs/UsersGuide.html#_lifecycle_hooks"], 0));
}

return shadow.cljs.devtools.client.shared.load_sources(runtime,sources_to_get,(function (p1__20952_SHARP_){
return shadow.cljs.devtools.client.browser.do_js_reload(msg,p1__20952_SHARP_,shadow.cljs.devtools.client.hud.load_end_success,shadow.cljs.devtools.client.hud.load_failure);
}));
}
} else {
return null;
}
}
});
shadow.cljs.devtools.client.browser.page_load_uri = (cljs.core.truth_(goog.global.document)?goog.Uri.parse(document.location.href):null);
shadow.cljs.devtools.client.browser.match_paths = (function shadow$cljs$devtools$client$browser$match_paths(old,new$){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("file",shadow.cljs.devtools.client.browser.page_load_uri.getScheme())){
var rel_new = cljs.core.subs.cljs$core$IFn$_invoke$arity$2(new$,(1));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(old,rel_new)) || (clojure.string.starts_with_QMARK_(old,[rel_new,"?"].join(''))))){
return rel_new;
} else {
return null;
}
} else {
var node_uri = goog.Uri.parse(old);
var node_uri_resolved = shadow.cljs.devtools.client.browser.page_load_uri.resolve(node_uri);
var node_abs = node_uri_resolved.getPath();
var and__5043__auto__ = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$1(shadow.cljs.devtools.client.browser.page_load_uri.hasSameDomainAs(node_uri))) || (cljs.core.not(node_uri.hasDomain())));
if(and__5043__auto__){
var and__5043__auto____$1 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(node_abs,new$);
if(and__5043__auto____$1){
return new$;
} else {
return and__5043__auto____$1;
}
} else {
return and__5043__auto__;
}
}
});
shadow.cljs.devtools.client.browser.handle_asset_update = (function shadow$cljs$devtools$client$browser$handle_asset_update(p__20986){
var map__20988 = p__20986;
var map__20988__$1 = cljs.core.__destructure_map(map__20988);
var msg = map__20988__$1;
var updates = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20988__$1,new cljs.core.Keyword(null,"updates","updates",2013983452));
var reload_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__20988__$1,new cljs.core.Keyword(null,"reload-info","reload-info",1648088086));
var seq__20989 = cljs.core.seq(updates);
var chunk__20991 = null;
var count__20992 = (0);
var i__20993 = (0);
while(true){
if((i__20993 < count__20992)){
var path = chunk__20991.cljs$core$IIndexed$_nth$arity$2(null,i__20993);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__21173_21419 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__21177_21420 = null;
var count__21178_21421 = (0);
var i__21179_21422 = (0);
while(true){
if((i__21179_21422 < count__21178_21421)){
var node_21423 = chunk__21177_21420.cljs$core$IIndexed$_nth$arity$2(null,i__21179_21422);
if(cljs.core.not(node_21423.shadow$old)){
var path_match_21424 = shadow.cljs.devtools.client.browser.match_paths(node_21423.getAttribute("href"),path);
if(cljs.core.truth_(path_match_21424)){
var new_link_21425 = (function (){var G__21211 = node_21423.cloneNode(true);
G__21211.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_21424),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__21211;
})();
(node_21423.shadow$old = true);

(new_link_21425.onload = ((function (seq__21173_21419,chunk__21177_21420,count__21178_21421,i__21179_21422,seq__20989,chunk__20991,count__20992,i__20993,new_link_21425,path_match_21424,node_21423,path,map__20988,map__20988__$1,msg,updates,reload_info){
return (function (e){
var seq__21212_21426 = cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"asset-load","asset-load",-1925902322)], null)));
var chunk__21214_21427 = null;
var count__21215_21428 = (0);
var i__21216_21429 = (0);
while(true){
if((i__21216_21429 < count__21215_21428)){
var map__21220_21430 = chunk__21214_21427.cljs$core$IIndexed$_nth$arity$2(null,i__21216_21429);
var map__21220_21431__$1 = cljs.core.__destructure_map(map__21220_21430);
var task_21432 = map__21220_21431__$1;
var fn_str_21433 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21220_21431__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_21434 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21220_21431__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_21435 = goog.getObjectByName(fn_str_21433,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_21434)].join(''));

(fn_obj_21435.cljs$core$IFn$_invoke$arity$2 ? fn_obj_21435.cljs$core$IFn$_invoke$arity$2(path,new_link_21425) : fn_obj_21435.call(null,path,new_link_21425));


var G__21436 = seq__21212_21426;
var G__21437 = chunk__21214_21427;
var G__21438 = count__21215_21428;
var G__21439 = (i__21216_21429 + (1));
seq__21212_21426 = G__21436;
chunk__21214_21427 = G__21437;
count__21215_21428 = G__21438;
i__21216_21429 = G__21439;
continue;
} else {
var temp__5804__auto___21440 = cljs.core.seq(seq__21212_21426);
if(temp__5804__auto___21440){
var seq__21212_21441__$1 = temp__5804__auto___21440;
if(cljs.core.chunked_seq_QMARK_(seq__21212_21441__$1)){
var c__5568__auto___21442 = cljs.core.chunk_first(seq__21212_21441__$1);
var G__21443 = cljs.core.chunk_rest(seq__21212_21441__$1);
var G__21444 = c__5568__auto___21442;
var G__21445 = cljs.core.count(c__5568__auto___21442);
var G__21446 = (0);
seq__21212_21426 = G__21443;
chunk__21214_21427 = G__21444;
count__21215_21428 = G__21445;
i__21216_21429 = G__21446;
continue;
} else {
var map__21221_21447 = cljs.core.first(seq__21212_21441__$1);
var map__21221_21448__$1 = cljs.core.__destructure_map(map__21221_21447);
var task_21449 = map__21221_21448__$1;
var fn_str_21450 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21221_21448__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_21451 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21221_21448__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_21452 = goog.getObjectByName(fn_str_21450,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_21451)].join(''));

(fn_obj_21452.cljs$core$IFn$_invoke$arity$2 ? fn_obj_21452.cljs$core$IFn$_invoke$arity$2(path,new_link_21425) : fn_obj_21452.call(null,path,new_link_21425));


var G__21453 = cljs.core.next(seq__21212_21441__$1);
var G__21454 = null;
var G__21455 = (0);
var G__21456 = (0);
seq__21212_21426 = G__21453;
chunk__21214_21427 = G__21454;
count__21215_21428 = G__21455;
i__21216_21429 = G__21456;
continue;
}
} else {
}
}
break;
}

return goog.dom.removeNode(node_21423);
});})(seq__21173_21419,chunk__21177_21420,count__21178_21421,i__21179_21422,seq__20989,chunk__20991,count__20992,i__20993,new_link_21425,path_match_21424,node_21423,path,map__20988,map__20988__$1,msg,updates,reload_info))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_21424], 0));

goog.dom.insertSiblingAfter(new_link_21425,node_21423);


var G__21458 = seq__21173_21419;
var G__21459 = chunk__21177_21420;
var G__21460 = count__21178_21421;
var G__21461 = (i__21179_21422 + (1));
seq__21173_21419 = G__21458;
chunk__21177_21420 = G__21459;
count__21178_21421 = G__21460;
i__21179_21422 = G__21461;
continue;
} else {
var G__21462 = seq__21173_21419;
var G__21463 = chunk__21177_21420;
var G__21464 = count__21178_21421;
var G__21465 = (i__21179_21422 + (1));
seq__21173_21419 = G__21462;
chunk__21177_21420 = G__21463;
count__21178_21421 = G__21464;
i__21179_21422 = G__21465;
continue;
}
} else {
var G__21466 = seq__21173_21419;
var G__21467 = chunk__21177_21420;
var G__21468 = count__21178_21421;
var G__21469 = (i__21179_21422 + (1));
seq__21173_21419 = G__21466;
chunk__21177_21420 = G__21467;
count__21178_21421 = G__21468;
i__21179_21422 = G__21469;
continue;
}
} else {
var temp__5804__auto___21470 = cljs.core.seq(seq__21173_21419);
if(temp__5804__auto___21470){
var seq__21173_21471__$1 = temp__5804__auto___21470;
if(cljs.core.chunked_seq_QMARK_(seq__21173_21471__$1)){
var c__5568__auto___21473 = cljs.core.chunk_first(seq__21173_21471__$1);
var G__21475 = cljs.core.chunk_rest(seq__21173_21471__$1);
var G__21476 = c__5568__auto___21473;
var G__21477 = cljs.core.count(c__5568__auto___21473);
var G__21478 = (0);
seq__21173_21419 = G__21475;
chunk__21177_21420 = G__21476;
count__21178_21421 = G__21477;
i__21179_21422 = G__21478;
continue;
} else {
var node_21479 = cljs.core.first(seq__21173_21471__$1);
if(cljs.core.not(node_21479.shadow$old)){
var path_match_21480 = shadow.cljs.devtools.client.browser.match_paths(node_21479.getAttribute("href"),path);
if(cljs.core.truth_(path_match_21480)){
var new_link_21481 = (function (){var G__21226 = node_21479.cloneNode(true);
G__21226.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_21480),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__21226;
})();
(node_21479.shadow$old = true);

(new_link_21481.onload = ((function (seq__21173_21419,chunk__21177_21420,count__21178_21421,i__21179_21422,seq__20989,chunk__20991,count__20992,i__20993,new_link_21481,path_match_21480,node_21479,seq__21173_21471__$1,temp__5804__auto___21470,path,map__20988,map__20988__$1,msg,updates,reload_info){
return (function (e){
var seq__21227_21485 = cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"asset-load","asset-load",-1925902322)], null)));
var chunk__21229_21486 = null;
var count__21230_21487 = (0);
var i__21231_21488 = (0);
while(true){
if((i__21231_21488 < count__21230_21487)){
var map__21237_21489 = chunk__21229_21486.cljs$core$IIndexed$_nth$arity$2(null,i__21231_21488);
var map__21237_21490__$1 = cljs.core.__destructure_map(map__21237_21489);
var task_21491 = map__21237_21490__$1;
var fn_str_21492 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21237_21490__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_21493 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21237_21490__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_21494 = goog.getObjectByName(fn_str_21492,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_21493)].join(''));

(fn_obj_21494.cljs$core$IFn$_invoke$arity$2 ? fn_obj_21494.cljs$core$IFn$_invoke$arity$2(path,new_link_21481) : fn_obj_21494.call(null,path,new_link_21481));


var G__21495 = seq__21227_21485;
var G__21496 = chunk__21229_21486;
var G__21497 = count__21230_21487;
var G__21498 = (i__21231_21488 + (1));
seq__21227_21485 = G__21495;
chunk__21229_21486 = G__21496;
count__21230_21487 = G__21497;
i__21231_21488 = G__21498;
continue;
} else {
var temp__5804__auto___21499__$1 = cljs.core.seq(seq__21227_21485);
if(temp__5804__auto___21499__$1){
var seq__21227_21500__$1 = temp__5804__auto___21499__$1;
if(cljs.core.chunked_seq_QMARK_(seq__21227_21500__$1)){
var c__5568__auto___21501 = cljs.core.chunk_first(seq__21227_21500__$1);
var G__21502 = cljs.core.chunk_rest(seq__21227_21500__$1);
var G__21503 = c__5568__auto___21501;
var G__21504 = cljs.core.count(c__5568__auto___21501);
var G__21505 = (0);
seq__21227_21485 = G__21502;
chunk__21229_21486 = G__21503;
count__21230_21487 = G__21504;
i__21231_21488 = G__21505;
continue;
} else {
var map__21241_21506 = cljs.core.first(seq__21227_21500__$1);
var map__21241_21507__$1 = cljs.core.__destructure_map(map__21241_21506);
var task_21508 = map__21241_21507__$1;
var fn_str_21509 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21241_21507__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_21510 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21241_21507__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_21511 = goog.getObjectByName(fn_str_21509,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_21510)].join(''));

(fn_obj_21511.cljs$core$IFn$_invoke$arity$2 ? fn_obj_21511.cljs$core$IFn$_invoke$arity$2(path,new_link_21481) : fn_obj_21511.call(null,path,new_link_21481));


var G__21512 = cljs.core.next(seq__21227_21500__$1);
var G__21513 = null;
var G__21514 = (0);
var G__21515 = (0);
seq__21227_21485 = G__21512;
chunk__21229_21486 = G__21513;
count__21230_21487 = G__21514;
i__21231_21488 = G__21515;
continue;
}
} else {
}
}
break;
}

return goog.dom.removeNode(node_21479);
});})(seq__21173_21419,chunk__21177_21420,count__21178_21421,i__21179_21422,seq__20989,chunk__20991,count__20992,i__20993,new_link_21481,path_match_21480,node_21479,seq__21173_21471__$1,temp__5804__auto___21470,path,map__20988,map__20988__$1,msg,updates,reload_info))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_21480], 0));

goog.dom.insertSiblingAfter(new_link_21481,node_21479);


var G__21516 = cljs.core.next(seq__21173_21471__$1);
var G__21517 = null;
var G__21518 = (0);
var G__21519 = (0);
seq__21173_21419 = G__21516;
chunk__21177_21420 = G__21517;
count__21178_21421 = G__21518;
i__21179_21422 = G__21519;
continue;
} else {
var G__21520 = cljs.core.next(seq__21173_21471__$1);
var G__21521 = null;
var G__21522 = (0);
var G__21523 = (0);
seq__21173_21419 = G__21520;
chunk__21177_21420 = G__21521;
count__21178_21421 = G__21522;
i__21179_21422 = G__21523;
continue;
}
} else {
var G__21524 = cljs.core.next(seq__21173_21471__$1);
var G__21525 = null;
var G__21526 = (0);
var G__21527 = (0);
seq__21173_21419 = G__21524;
chunk__21177_21420 = G__21525;
count__21178_21421 = G__21526;
i__21179_21422 = G__21527;
continue;
}
}
} else {
}
}
break;
}


var G__21528 = seq__20989;
var G__21529 = chunk__20991;
var G__21530 = count__20992;
var G__21531 = (i__20993 + (1));
seq__20989 = G__21528;
chunk__20991 = G__21529;
count__20992 = G__21530;
i__20993 = G__21531;
continue;
} else {
var G__21532 = seq__20989;
var G__21533 = chunk__20991;
var G__21534 = count__20992;
var G__21535 = (i__20993 + (1));
seq__20989 = G__21532;
chunk__20991 = G__21533;
count__20992 = G__21534;
i__20993 = G__21535;
continue;
}
} else {
var temp__5804__auto__ = cljs.core.seq(seq__20989);
if(temp__5804__auto__){
var seq__20989__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__20989__$1)){
var c__5568__auto__ = cljs.core.chunk_first(seq__20989__$1);
var G__21536 = cljs.core.chunk_rest(seq__20989__$1);
var G__21537 = c__5568__auto__;
var G__21538 = cljs.core.count(c__5568__auto__);
var G__21539 = (0);
seq__20989 = G__21536;
chunk__20991 = G__21537;
count__20992 = G__21538;
i__20993 = G__21539;
continue;
} else {
var path = cljs.core.first(seq__20989__$1);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__21242_21540 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__21246_21541 = null;
var count__21247_21542 = (0);
var i__21248_21543 = (0);
while(true){
if((i__21248_21543 < count__21247_21542)){
var node_21544 = chunk__21246_21541.cljs$core$IIndexed$_nth$arity$2(null,i__21248_21543);
if(cljs.core.not(node_21544.shadow$old)){
var path_match_21545 = shadow.cljs.devtools.client.browser.match_paths(node_21544.getAttribute("href"),path);
if(cljs.core.truth_(path_match_21545)){
var new_link_21546 = (function (){var G__21281 = node_21544.cloneNode(true);
G__21281.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_21545),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__21281;
})();
(node_21544.shadow$old = true);

(new_link_21546.onload = ((function (seq__21242_21540,chunk__21246_21541,count__21247_21542,i__21248_21543,seq__20989,chunk__20991,count__20992,i__20993,new_link_21546,path_match_21545,node_21544,path,seq__20989__$1,temp__5804__auto__,map__20988,map__20988__$1,msg,updates,reload_info){
return (function (e){
var seq__21282_21547 = cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"asset-load","asset-load",-1925902322)], null)));
var chunk__21284_21548 = null;
var count__21285_21549 = (0);
var i__21286_21550 = (0);
while(true){
if((i__21286_21550 < count__21285_21549)){
var map__21292_21551 = chunk__21284_21548.cljs$core$IIndexed$_nth$arity$2(null,i__21286_21550);
var map__21292_21552__$1 = cljs.core.__destructure_map(map__21292_21551);
var task_21553 = map__21292_21552__$1;
var fn_str_21554 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21292_21552__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_21555 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21292_21552__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_21556 = goog.getObjectByName(fn_str_21554,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_21555)].join(''));

(fn_obj_21556.cljs$core$IFn$_invoke$arity$2 ? fn_obj_21556.cljs$core$IFn$_invoke$arity$2(path,new_link_21546) : fn_obj_21556.call(null,path,new_link_21546));


var G__21557 = seq__21282_21547;
var G__21558 = chunk__21284_21548;
var G__21559 = count__21285_21549;
var G__21560 = (i__21286_21550 + (1));
seq__21282_21547 = G__21557;
chunk__21284_21548 = G__21558;
count__21285_21549 = G__21559;
i__21286_21550 = G__21560;
continue;
} else {
var temp__5804__auto___21561__$1 = cljs.core.seq(seq__21282_21547);
if(temp__5804__auto___21561__$1){
var seq__21282_21562__$1 = temp__5804__auto___21561__$1;
if(cljs.core.chunked_seq_QMARK_(seq__21282_21562__$1)){
var c__5568__auto___21563 = cljs.core.chunk_first(seq__21282_21562__$1);
var G__21564 = cljs.core.chunk_rest(seq__21282_21562__$1);
var G__21565 = c__5568__auto___21563;
var G__21566 = cljs.core.count(c__5568__auto___21563);
var G__21567 = (0);
seq__21282_21547 = G__21564;
chunk__21284_21548 = G__21565;
count__21285_21549 = G__21566;
i__21286_21550 = G__21567;
continue;
} else {
var map__21293_21568 = cljs.core.first(seq__21282_21562__$1);
var map__21293_21569__$1 = cljs.core.__destructure_map(map__21293_21568);
var task_21570 = map__21293_21569__$1;
var fn_str_21571 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21293_21569__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_21572 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21293_21569__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_21573 = goog.getObjectByName(fn_str_21571,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_21572)].join(''));

(fn_obj_21573.cljs$core$IFn$_invoke$arity$2 ? fn_obj_21573.cljs$core$IFn$_invoke$arity$2(path,new_link_21546) : fn_obj_21573.call(null,path,new_link_21546));


var G__21574 = cljs.core.next(seq__21282_21562__$1);
var G__21575 = null;
var G__21576 = (0);
var G__21577 = (0);
seq__21282_21547 = G__21574;
chunk__21284_21548 = G__21575;
count__21285_21549 = G__21576;
i__21286_21550 = G__21577;
continue;
}
} else {
}
}
break;
}

return goog.dom.removeNode(node_21544);
});})(seq__21242_21540,chunk__21246_21541,count__21247_21542,i__21248_21543,seq__20989,chunk__20991,count__20992,i__20993,new_link_21546,path_match_21545,node_21544,path,seq__20989__$1,temp__5804__auto__,map__20988,map__20988__$1,msg,updates,reload_info))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_21545], 0));

goog.dom.insertSiblingAfter(new_link_21546,node_21544);


var G__21578 = seq__21242_21540;
var G__21579 = chunk__21246_21541;
var G__21580 = count__21247_21542;
var G__21581 = (i__21248_21543 + (1));
seq__21242_21540 = G__21578;
chunk__21246_21541 = G__21579;
count__21247_21542 = G__21580;
i__21248_21543 = G__21581;
continue;
} else {
var G__21582 = seq__21242_21540;
var G__21583 = chunk__21246_21541;
var G__21584 = count__21247_21542;
var G__21585 = (i__21248_21543 + (1));
seq__21242_21540 = G__21582;
chunk__21246_21541 = G__21583;
count__21247_21542 = G__21584;
i__21248_21543 = G__21585;
continue;
}
} else {
var G__21586 = seq__21242_21540;
var G__21587 = chunk__21246_21541;
var G__21588 = count__21247_21542;
var G__21589 = (i__21248_21543 + (1));
seq__21242_21540 = G__21586;
chunk__21246_21541 = G__21587;
count__21247_21542 = G__21588;
i__21248_21543 = G__21589;
continue;
}
} else {
var temp__5804__auto___21590__$1 = cljs.core.seq(seq__21242_21540);
if(temp__5804__auto___21590__$1){
var seq__21242_21591__$1 = temp__5804__auto___21590__$1;
if(cljs.core.chunked_seq_QMARK_(seq__21242_21591__$1)){
var c__5568__auto___21592 = cljs.core.chunk_first(seq__21242_21591__$1);
var G__21593 = cljs.core.chunk_rest(seq__21242_21591__$1);
var G__21594 = c__5568__auto___21592;
var G__21595 = cljs.core.count(c__5568__auto___21592);
var G__21596 = (0);
seq__21242_21540 = G__21593;
chunk__21246_21541 = G__21594;
count__21247_21542 = G__21595;
i__21248_21543 = G__21596;
continue;
} else {
var node_21597 = cljs.core.first(seq__21242_21591__$1);
if(cljs.core.not(node_21597.shadow$old)){
var path_match_21598 = shadow.cljs.devtools.client.browser.match_paths(node_21597.getAttribute("href"),path);
if(cljs.core.truth_(path_match_21598)){
var new_link_21599 = (function (){var G__21295 = node_21597.cloneNode(true);
G__21295.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_21598),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__21295;
})();
(node_21597.shadow$old = true);

(new_link_21599.onload = ((function (seq__21242_21540,chunk__21246_21541,count__21247_21542,i__21248_21543,seq__20989,chunk__20991,count__20992,i__20993,new_link_21599,path_match_21598,node_21597,seq__21242_21591__$1,temp__5804__auto___21590__$1,path,seq__20989__$1,temp__5804__auto__,map__20988,map__20988__$1,msg,updates,reload_info){
return (function (e){
var seq__21296_21600 = cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"asset-load","asset-load",-1925902322)], null)));
var chunk__21298_21601 = null;
var count__21299_21602 = (0);
var i__21300_21603 = (0);
while(true){
if((i__21300_21603 < count__21299_21602)){
var map__21310_21604 = chunk__21298_21601.cljs$core$IIndexed$_nth$arity$2(null,i__21300_21603);
var map__21310_21605__$1 = cljs.core.__destructure_map(map__21310_21604);
var task_21606 = map__21310_21605__$1;
var fn_str_21607 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21310_21605__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_21608 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21310_21605__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_21609 = goog.getObjectByName(fn_str_21607,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_21608)].join(''));

(fn_obj_21609.cljs$core$IFn$_invoke$arity$2 ? fn_obj_21609.cljs$core$IFn$_invoke$arity$2(path,new_link_21599) : fn_obj_21609.call(null,path,new_link_21599));


var G__21610 = seq__21296_21600;
var G__21611 = chunk__21298_21601;
var G__21612 = count__21299_21602;
var G__21613 = (i__21300_21603 + (1));
seq__21296_21600 = G__21610;
chunk__21298_21601 = G__21611;
count__21299_21602 = G__21612;
i__21300_21603 = G__21613;
continue;
} else {
var temp__5804__auto___21614__$2 = cljs.core.seq(seq__21296_21600);
if(temp__5804__auto___21614__$2){
var seq__21296_21615__$1 = temp__5804__auto___21614__$2;
if(cljs.core.chunked_seq_QMARK_(seq__21296_21615__$1)){
var c__5568__auto___21616 = cljs.core.chunk_first(seq__21296_21615__$1);
var G__21617 = cljs.core.chunk_rest(seq__21296_21615__$1);
var G__21618 = c__5568__auto___21616;
var G__21619 = cljs.core.count(c__5568__auto___21616);
var G__21620 = (0);
seq__21296_21600 = G__21617;
chunk__21298_21601 = G__21618;
count__21299_21602 = G__21619;
i__21300_21603 = G__21620;
continue;
} else {
var map__21311_21621 = cljs.core.first(seq__21296_21615__$1);
var map__21311_21622__$1 = cljs.core.__destructure_map(map__21311_21621);
var task_21623 = map__21311_21622__$1;
var fn_str_21624 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21311_21622__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_21625 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21311_21622__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_21626 = goog.getObjectByName(fn_str_21624,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_21625)].join(''));

(fn_obj_21626.cljs$core$IFn$_invoke$arity$2 ? fn_obj_21626.cljs$core$IFn$_invoke$arity$2(path,new_link_21599) : fn_obj_21626.call(null,path,new_link_21599));


var G__21629 = cljs.core.next(seq__21296_21615__$1);
var G__21630 = null;
var G__21631 = (0);
var G__21632 = (0);
seq__21296_21600 = G__21629;
chunk__21298_21601 = G__21630;
count__21299_21602 = G__21631;
i__21300_21603 = G__21632;
continue;
}
} else {
}
}
break;
}

return goog.dom.removeNode(node_21597);
});})(seq__21242_21540,chunk__21246_21541,count__21247_21542,i__21248_21543,seq__20989,chunk__20991,count__20992,i__20993,new_link_21599,path_match_21598,node_21597,seq__21242_21591__$1,temp__5804__auto___21590__$1,path,seq__20989__$1,temp__5804__auto__,map__20988,map__20988__$1,msg,updates,reload_info))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_21598], 0));

goog.dom.insertSiblingAfter(new_link_21599,node_21597);


var G__21633 = cljs.core.next(seq__21242_21591__$1);
var G__21634 = null;
var G__21635 = (0);
var G__21636 = (0);
seq__21242_21540 = G__21633;
chunk__21246_21541 = G__21634;
count__21247_21542 = G__21635;
i__21248_21543 = G__21636;
continue;
} else {
var G__21637 = cljs.core.next(seq__21242_21591__$1);
var G__21638 = null;
var G__21639 = (0);
var G__21640 = (0);
seq__21242_21540 = G__21637;
chunk__21246_21541 = G__21638;
count__21247_21542 = G__21639;
i__21248_21543 = G__21640;
continue;
}
} else {
var G__21641 = cljs.core.next(seq__21242_21591__$1);
var G__21642 = null;
var G__21643 = (0);
var G__21644 = (0);
seq__21242_21540 = G__21641;
chunk__21246_21541 = G__21642;
count__21247_21542 = G__21643;
i__21248_21543 = G__21644;
continue;
}
}
} else {
}
}
break;
}


var G__21645 = cljs.core.next(seq__20989__$1);
var G__21646 = null;
var G__21647 = (0);
var G__21648 = (0);
seq__20989 = G__21645;
chunk__20991 = G__21646;
count__20992 = G__21647;
i__20993 = G__21648;
continue;
} else {
var G__21649 = cljs.core.next(seq__20989__$1);
var G__21650 = null;
var G__21651 = (0);
var G__21652 = (0);
seq__20989 = G__21649;
chunk__20991 = G__21650;
count__20992 = G__21651;
i__20993 = G__21652;
continue;
}
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.global_eval = (function shadow$cljs$devtools$client$browser$global_eval(js){
if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2("undefined",typeof(module))){
return eval(js);
} else {
return (0,eval)(js);;
}
});
shadow.cljs.devtools.client.browser.repl_init = (function shadow$cljs$devtools$client$browser$repl_init(runtime,p__21313){
var map__21314 = p__21313;
var map__21314__$1 = cljs.core.__destructure_map(map__21314);
var repl_state = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21314__$1,new cljs.core.Keyword(null,"repl-state","repl-state",-1733780387));
return shadow.cljs.devtools.client.shared.load_sources(runtime,cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.env.src_is_loaded_QMARK_,new cljs.core.Keyword(null,"repl-sources","repl-sources",723867535).cljs$core$IFn$_invoke$arity$1(repl_state))),(function (sources){
shadow.cljs.devtools.client.browser.do_js_load(sources);

return shadow.cljs.devtools.client.browser.devtools_msg("ready!");
}));
});
shadow.cljs.devtools.client.browser.runtime_info = (((typeof SHADOW_CONFIG !== 'undefined'))?shadow.json.to_clj.cljs$core$IFn$_invoke$arity$1(SHADOW_CONFIG):null);
shadow.cljs.devtools.client.browser.client_info = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([shadow.cljs.devtools.client.browser.runtime_info,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"host","host",-1558485167),(cljs.core.truth_(goog.global.document)?new cljs.core.Keyword(null,"browser","browser",828191719):new cljs.core.Keyword(null,"browser-worker","browser-worker",1638998282)),new cljs.core.Keyword(null,"user-agent","user-agent",1220426212),[(cljs.core.truth_(goog.userAgent.OPERA)?"Opera":(cljs.core.truth_(goog.userAgent.product.CHROME)?"Chrome":(cljs.core.truth_(goog.userAgent.IE)?"MSIE":(cljs.core.truth_(goog.userAgent.EDGE)?"Edge":(cljs.core.truth_(goog.userAgent.GECKO)?"Firefox":(cljs.core.truth_(goog.userAgent.SAFARI)?"Safari":(cljs.core.truth_(goog.userAgent.WEBKIT)?"Webkit":null)))))))," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.userAgent.VERSION)," [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.userAgent.PLATFORM),"]"].join(''),new cljs.core.Keyword(null,"dom","dom",-1236537922),(!((goog.global.document == null)))], null)], 0));
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.ws_was_welcome_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.ws_was_welcome_ref = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(false);
}
if(((shadow.cljs.devtools.client.env.enabled) && ((shadow.cljs.devtools.client.env.worker_client_id > (0))))){
(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$remote$runtime$api$IEvalJS$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$remote$runtime$api$IEvalJS$_js_eval$arity$2 = (function (this$,code){
var this$__$1 = this;
return shadow.cljs.devtools.client.browser.global_eval(code);
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_invoke$arity$2 = (function (this$,p__21316){
var map__21317 = p__21316;
var map__21317__$1 = cljs.core.__destructure_map(map__21317);
var _ = map__21317__$1;
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21317__$1,new cljs.core.Keyword(null,"js","js",1768080579));
var this$__$1 = this;
return shadow.cljs.devtools.client.browser.global_eval(js);
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_repl_init$arity$4 = (function (runtime,p__21318,done,error){
var map__21319 = p__21318;
var map__21319__$1 = cljs.core.__destructure_map(map__21319);
var repl_sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21319__$1,new cljs.core.Keyword(null,"repl-sources","repl-sources",723867535));
var runtime__$1 = this;
return shadow.cljs.devtools.client.shared.load_sources(runtime__$1,cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.env.src_is_loaded_QMARK_,repl_sources)),(function (sources){
shadow.cljs.devtools.client.browser.do_js_load(sources);

return (done.cljs$core$IFn$_invoke$arity$0 ? done.cljs$core$IFn$_invoke$arity$0() : done.call(null));
}));
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_repl_require$arity$4 = (function (runtime,p__21320,done,error){
var map__21321 = p__21320;
var map__21321__$1 = cljs.core.__destructure_map(map__21321);
var msg = map__21321__$1;
var sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21321__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
var reload_namespaces = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21321__$1,new cljs.core.Keyword(null,"reload-namespaces","reload-namespaces",250210134));
var js_requires = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21321__$1,new cljs.core.Keyword(null,"js-requires","js-requires",-1311472051));
var runtime__$1 = this;
var sources_to_load = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p__21322){
var map__21323 = p__21322;
var map__21323__$1 = cljs.core.__destructure_map(map__21323);
var src = map__21323__$1;
var provides = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21323__$1,new cljs.core.Keyword(null,"provides","provides",-1634397992));
var and__5043__auto__ = shadow.cljs.devtools.client.env.src_is_loaded_QMARK_(src);
if(cljs.core.truth_(and__5043__auto__)){
return cljs.core.not(cljs.core.some(reload_namespaces,provides));
} else {
return and__5043__auto__;
}
}),sources));
if(cljs.core.not(cljs.core.seq(sources_to_load))){
var G__21324 = cljs.core.PersistentVector.EMPTY;
return (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(G__21324) : done.call(null,G__21324));
} else {
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$3(runtime__$1,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"cljs-load-sources","cljs-load-sources",-1458295962),new cljs.core.Keyword(null,"to","to",192099007),shadow.cljs.devtools.client.env.worker_client_id,new cljs.core.Keyword(null,"sources","sources",-321166424),cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582)),sources_to_load)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"cljs-sources","cljs-sources",31121610),(function (p__21325){
var map__21326 = p__21325;
var map__21326__$1 = cljs.core.__destructure_map(map__21326);
var msg__$1 = map__21326__$1;
var sources__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21326__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
try{shadow.cljs.devtools.client.browser.do_js_load(sources__$1);

if(cljs.core.seq(js_requires)){
shadow.cljs.devtools.client.browser.do_js_requires(js_requires);
} else {
}

return (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(sources_to_load) : done.call(null,sources_to_load));
}catch (e21327){var ex = e21327;
return (error.cljs$core$IFn$_invoke$arity$1 ? error.cljs$core$IFn$_invoke$arity$1(ex) : error.call(null,ex));
}})], null));
}
}));

shadow.cljs.devtools.client.shared.add_plugin_BANG_(new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282),cljs.core.PersistentHashSet.EMPTY,(function (p__21328){
var map__21329 = p__21328;
var map__21329__$1 = cljs.core.__destructure_map(map__21329);
var env = map__21329__$1;
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21329__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
var svc = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"runtime","runtime",-1331573996),runtime], null);
shadow.remote.runtime.api.add_extension(runtime,new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"on-welcome","on-welcome",1895317125),(function (){
cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,true);

shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

shadow.cljs.devtools.client.env.patch_goog_BANG_();

return shadow.cljs.devtools.client.browser.devtools_msg(["#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"client-id","client-id",-464622140).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(new cljs.core.Keyword(null,"state-ref","state-ref",2127874952).cljs$core$IFn$_invoke$arity$1(runtime))))," ready!"].join(''));
}),new cljs.core.Keyword(null,"on-disconnect","on-disconnect",-809021814),(function (e){
if(cljs.core.truth_(cljs.core.deref(shadow.cljs.devtools.client.browser.ws_was_welcome_ref))){
shadow.cljs.devtools.client.hud.connection_error("The Websocket connection was closed!");

return cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,false);
} else {
return null;
}
}),new cljs.core.Keyword(null,"on-reconnect","on-reconnect",1239988702),(function (e){
return shadow.cljs.devtools.client.hud.connection_error("Reconnecting ...");
}),new cljs.core.Keyword(null,"ops","ops",1237330063),new cljs.core.PersistentArrayMap(null, 8, [new cljs.core.Keyword(null,"access-denied","access-denied",959449406),(function (msg){
cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,false);

return shadow.cljs.devtools.client.hud.connection_error(["Stale Output! Your loaded JS was not produced by the running shadow-cljs instance."," Is the watch for this build running?"].join(''));
}),new cljs.core.Keyword(null,"cljs-runtime-init","cljs-runtime-init",1305890232),(function (msg){
return shadow.cljs.devtools.client.browser.repl_init(runtime,msg);
}),new cljs.core.Keyword(null,"cljs-asset-update","cljs-asset-update",1224093028),(function (msg){
return shadow.cljs.devtools.client.browser.handle_asset_update(msg);
}),new cljs.core.Keyword(null,"cljs-build-configure","cljs-build-configure",-2089891268),(function (msg){
return null;
}),new cljs.core.Keyword(null,"cljs-build-start","cljs-build-start",-725781241),(function (msg){
shadow.cljs.devtools.client.hud.hud_hide();

shadow.cljs.devtools.client.hud.load_start();

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-start","build-start",-959649480)));
}),new cljs.core.Keyword(null,"cljs-build-complete","cljs-build-complete",273626153),(function (msg){
var msg__$1 = shadow.cljs.devtools.client.env.add_warnings_to_info(msg);
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

shadow.cljs.devtools.client.hud.hud_warnings(msg__$1);

shadow.cljs.devtools.client.browser.handle_build_complete(runtime,msg__$1);

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg__$1,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-complete","build-complete",-501868472)));
}),new cljs.core.Keyword(null,"cljs-build-failure","cljs-build-failure",1718154990),(function (msg){
shadow.cljs.devtools.client.hud.load_end();

shadow.cljs.devtools.client.hud.hud_error(msg);

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-failure","build-failure",-2107487466)));
}),new cljs.core.Keyword("shadow.cljs.devtools.client.env","worker-notify","shadow.cljs.devtools.client.env/worker-notify",-1456820670),(function (p__21332){
var map__21333 = p__21332;
var map__21333__$1 = cljs.core.__destructure_map(map__21333);
var event_op = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21333__$1,new cljs.core.Keyword(null,"event-op","event-op",200358057));
var client_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21333__$1,new cljs.core.Keyword(null,"client-id","client-id",-464622140));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-disconnect","client-disconnect",640227957),event_op)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(client_id,shadow.cljs.devtools.client.env.worker_client_id)))){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

return shadow.cljs.devtools.client.hud.connection_error("The watch for this build was stopped!");
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-connect","client-connect",-1113973888),event_op)){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

return shadow.cljs.devtools.client.hud.connection_error("The watch for this build was restarted. Reload required!");
} else {
return null;
}
}
})], null)], null));

return svc;
}),(function (p__21338){
var map__21339 = p__21338;
var map__21339__$1 = cljs.core.__destructure_map(map__21339);
var svc = map__21339__$1;
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__21339__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
return shadow.remote.runtime.api.del_extension(runtime,new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282));
}));

shadow.cljs.devtools.client.shared.init_runtime_BANG_(shadow.cljs.devtools.client.browser.client_info,shadow.cljs.devtools.client.websocket.start,shadow.cljs.devtools.client.websocket.send,shadow.cljs.devtools.client.websocket.stop);
} else {
}

//# sourceMappingURL=shadow.cljs.devtools.client.browser.js.map
