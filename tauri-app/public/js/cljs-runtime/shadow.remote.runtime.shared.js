goog.provide('shadow.remote.runtime.shared');
shadow.remote.runtime.shared.init_state = (function shadow$remote$runtime$shared$init_state(client_info){
return new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"extensions","extensions",-1103629196),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"ops","ops",1237330063),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"client-info","client-info",1958982504),client_info,new cljs.core.Keyword(null,"call-id-seq","call-id-seq",-1679248218),(0),new cljs.core.Keyword(null,"call-handlers","call-handlers",386605551),cljs.core.PersistentArrayMap.EMPTY], null);
});
shadow.remote.runtime.shared.now = (function shadow$remote$runtime$shared$now(){
return Date.now();
});
shadow.remote.runtime.shared.relay_msg = (function shadow$remote$runtime$shared$relay_msg(runtime,msg){
return shadow.remote.runtime.api.relay_msg(runtime,msg);
});
shadow.remote.runtime.shared.reply = (function shadow$remote$runtime$shared$reply(runtime,p__18939,res){
var map__18942 = p__18939;
var map__18942__$1 = cljs.core.__destructure_map(map__18942);
var call_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__18942__$1,new cljs.core.Keyword(null,"call-id","call-id",1043012968));
var from = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__18942__$1,new cljs.core.Keyword(null,"from","from",1815293044));
var res__$1 = (function (){var G__18944 = res;
var G__18944__$1 = (cljs.core.truth_(call_id)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__18944,new cljs.core.Keyword(null,"call-id","call-id",1043012968),call_id):G__18944);
if(cljs.core.truth_(from)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__18944__$1,new cljs.core.Keyword(null,"to","to",192099007),from);
} else {
return G__18944__$1;
}
})();
return shadow.remote.runtime.api.relay_msg(runtime,res__$1);
});
shadow.remote.runtime.shared.call = (function shadow$remote$runtime$shared$call(var_args){
var G__18946 = arguments.length;
switch (G__18946) {
case 3:
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$3 = (function (runtime,msg,handlers){
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$4(runtime,msg,handlers,(0));
}));

(shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$4 = (function (p__18948,msg,handlers,timeout_after_ms){
var map__18949 = p__18948;
var map__18949__$1 = cljs.core.__destructure_map(map__18949);
var runtime = map__18949__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__18949__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var call_id = new cljs.core.Keyword(null,"call-id-seq","call-id-seq",-1679248218).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(state_ref,cljs.core.update,new cljs.core.Keyword(null,"call-id-seq","call-id-seq",-1679248218),cljs.core.inc);

cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(state_ref,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"call-handlers","call-handlers",386605551),call_id], null),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"handlers","handlers",79528781),handlers,new cljs.core.Keyword(null,"called-at","called-at",607081160),shadow.remote.runtime.shared.now(),new cljs.core.Keyword(null,"msg","msg",-1386103444),msg,new cljs.core.Keyword(null,"timeout","timeout",-318625318),timeout_after_ms], null));

return shadow.remote.runtime.api.relay_msg(runtime,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"call-id","call-id",1043012968),call_id));
}));

(shadow.remote.runtime.shared.call.cljs$lang$maxFixedArity = 4);

shadow.remote.runtime.shared.trigger_BANG_ = (function shadow$remote$runtime$shared$trigger_BANG_(var_args){
var args__5775__auto__ = [];
var len__5769__auto___19050 = arguments.length;
var i__5770__auto___19052 = (0);
while(true){
if((i__5770__auto___19052 < len__5769__auto___19050)){
args__5775__auto__.push((arguments[i__5770__auto___19052]));

var G__19053 = (i__5770__auto___19052 + (1));
i__5770__auto___19052 = G__19053;
continue;
} else {
}
break;
}

var argseq__5776__auto__ = ((((2) < args__5775__auto__.length))?(new cljs.core.IndexedSeq(args__5775__auto__.slice((2)),(0),null)):null);
return shadow.remote.runtime.shared.trigger_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__5776__auto__);
});

(shadow.remote.runtime.shared.trigger_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (p__18967,ev,args){
var map__18968 = p__18967;
var map__18968__$1 = cljs.core.__destructure_map(map__18968);
var runtime = map__18968__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__18968__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var seq__18969 = cljs.core.seq(cljs.core.vals(new cljs.core.Keyword(null,"extensions","extensions",-1103629196).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref))));
var chunk__18972 = null;
var count__18973 = (0);
var i__18974 = (0);
while(true){
if((i__18974 < count__18973)){
var ext = chunk__18972.cljs$core$IIndexed$_nth$arity$2(null,i__18974);
var ev_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(ext,ev);
if(cljs.core.truth_(ev_fn)){
cljs.core.apply.cljs$core$IFn$_invoke$arity$2(ev_fn,args);


var G__19060 = seq__18969;
var G__19061 = chunk__18972;
var G__19062 = count__18973;
var G__19063 = (i__18974 + (1));
seq__18969 = G__19060;
chunk__18972 = G__19061;
count__18973 = G__19062;
i__18974 = G__19063;
continue;
} else {
var G__19064 = seq__18969;
var G__19065 = chunk__18972;
var G__19066 = count__18973;
var G__19067 = (i__18974 + (1));
seq__18969 = G__19064;
chunk__18972 = G__19065;
count__18973 = G__19066;
i__18974 = G__19067;
continue;
}
} else {
var temp__5804__auto__ = cljs.core.seq(seq__18969);
if(temp__5804__auto__){
var seq__18969__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__18969__$1)){
var c__5568__auto__ = cljs.core.chunk_first(seq__18969__$1);
var G__19068 = cljs.core.chunk_rest(seq__18969__$1);
var G__19069 = c__5568__auto__;
var G__19070 = cljs.core.count(c__5568__auto__);
var G__19071 = (0);
seq__18969 = G__19068;
chunk__18972 = G__19069;
count__18973 = G__19070;
i__18974 = G__19071;
continue;
} else {
var ext = cljs.core.first(seq__18969__$1);
var ev_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(ext,ev);
if(cljs.core.truth_(ev_fn)){
cljs.core.apply.cljs$core$IFn$_invoke$arity$2(ev_fn,args);


var G__19072 = cljs.core.next(seq__18969__$1);
var G__19073 = null;
var G__19074 = (0);
var G__19075 = (0);
seq__18969 = G__19072;
chunk__18972 = G__19073;
count__18973 = G__19074;
i__18974 = G__19075;
continue;
} else {
var G__19076 = cljs.core.next(seq__18969__$1);
var G__19077 = null;
var G__19078 = (0);
var G__19079 = (0);
seq__18969 = G__19076;
chunk__18972 = G__19077;
count__18973 = G__19078;
i__18974 = G__19079;
continue;
}
}
} else {
return null;
}
}
break;
}
}));

(shadow.remote.runtime.shared.trigger_BANG_.cljs$lang$maxFixedArity = (2));

/** @this {Function} */
(shadow.remote.runtime.shared.trigger_BANG_.cljs$lang$applyTo = (function (seq18959){
var G__18960 = cljs.core.first(seq18959);
var seq18959__$1 = cljs.core.next(seq18959);
var G__18961 = cljs.core.first(seq18959__$1);
var seq18959__$2 = cljs.core.next(seq18959__$1);
var self__5754__auto__ = this;
return self__5754__auto__.cljs$core$IFn$_invoke$arity$variadic(G__18960,G__18961,seq18959__$2);
}));

shadow.remote.runtime.shared.welcome = (function shadow$remote$runtime$shared$welcome(p__18981,p__18982){
var map__18983 = p__18981;
var map__18983__$1 = cljs.core.__destructure_map(map__18983);
var runtime = map__18983__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__18983__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var map__18984 = p__18982;
var map__18984__$1 = cljs.core.__destructure_map(map__18984);
var msg = map__18984__$1;
var client_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__18984__$1,new cljs.core.Keyword(null,"client-id","client-id",-464622140));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(state_ref,cljs.core.assoc,new cljs.core.Keyword(null,"client-id","client-id",-464622140),client_id);

var map__18985 = cljs.core.deref(state_ref);
var map__18985__$1 = cljs.core.__destructure_map(map__18985);
var client_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__18985__$1,new cljs.core.Keyword(null,"client-info","client-info",1958982504));
var extensions = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__18985__$1,new cljs.core.Keyword(null,"extensions","extensions",-1103629196));
shadow.remote.runtime.shared.relay_msg(runtime,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"hello","hello",-245025397),new cljs.core.Keyword(null,"client-info","client-info",1958982504),client_info], null));

return shadow.remote.runtime.shared.trigger_BANG_(runtime,new cljs.core.Keyword(null,"on-welcome","on-welcome",1895317125));
});
shadow.remote.runtime.shared.ping = (function shadow$remote$runtime$shared$ping(runtime,msg){
return shadow.remote.runtime.shared.reply(runtime,msg,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"pong","pong",-172484958)], null));
});
shadow.remote.runtime.shared.get_client_id = (function shadow$remote$runtime$shared$get_client_id(p__18988){
var map__18989 = p__18988;
var map__18989__$1 = cljs.core.__destructure_map(map__18989);
var runtime = map__18989__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__18989__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var or__5045__auto__ = new cljs.core.Keyword(null,"client-id","client-id",-464622140).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref));
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("runtime has no assigned runtime-id",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"runtime","runtime",-1331573996),runtime], null));
}
});
shadow.remote.runtime.shared.request_supported_ops = (function shadow$remote$runtime$shared$request_supported_ops(p__18990,msg){
var map__18991 = p__18990;
var map__18991__$1 = cljs.core.__destructure_map(map__18991);
var runtime = map__18991__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__18991__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
return shadow.remote.runtime.shared.reply(runtime,msg,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"supported-ops","supported-ops",337914702),new cljs.core.Keyword(null,"ops","ops",1237330063),cljs.core.disj.cljs$core$IFn$_invoke$arity$variadic(cljs.core.set(cljs.core.keys(new cljs.core.Keyword(null,"ops","ops",1237330063).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref)))),new cljs.core.Keyword(null,"welcome","welcome",-578152123),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"unknown-relay-op","unknown-relay-op",170832753),new cljs.core.Keyword(null,"unknown-op","unknown-op",1900385996),new cljs.core.Keyword(null,"request-supported-ops","request-supported-ops",-1034994502),new cljs.core.Keyword(null,"tool-disconnect","tool-disconnect",189103996)], 0))], null));
});
shadow.remote.runtime.shared.unknown_relay_op = (function shadow$remote$runtime$shared$unknown_relay_op(msg){
return console.warn("unknown-relay-op",msg);
});
shadow.remote.runtime.shared.unknown_op = (function shadow$remote$runtime$shared$unknown_op(msg){
return console.warn("unknown-op",msg);
});
shadow.remote.runtime.shared.add_extension_STAR_ = (function shadow$remote$runtime$shared$add_extension_STAR_(p__18997,key,p__18998){
var map__19000 = p__18997;
var map__19000__$1 = cljs.core.__destructure_map(map__19000);
var state = map__19000__$1;
var extensions = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__19000__$1,new cljs.core.Keyword(null,"extensions","extensions",-1103629196));
var map__19001 = p__18998;
var map__19001__$1 = cljs.core.__destructure_map(map__19001);
var spec = map__19001__$1;
var ops = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__19001__$1,new cljs.core.Keyword(null,"ops","ops",1237330063));
if(cljs.core.contains_QMARK_(extensions,key)){
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("extension already registered",new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),key,new cljs.core.Keyword(null,"spec","spec",347520401),spec], null));
} else {
}

return cljs.core.reduce_kv((function (state__$1,op_kw,op_handler){
if(cljs.core.truth_(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(state__$1,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ops","ops",1237330063),op_kw], null)))){
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("op already registered",new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),key,new cljs.core.Keyword(null,"op","op",-1882987955),op_kw], null));
} else {
}

return cljs.core.assoc_in(state__$1,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ops","ops",1237330063),op_kw], null),op_handler);
}),cljs.core.assoc_in(state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"extensions","extensions",-1103629196),key], null),spec),ops);
});
shadow.remote.runtime.shared.add_extension = (function shadow$remote$runtime$shared$add_extension(p__19006,key,spec){
var map__19007 = p__19006;
var map__19007__$1 = cljs.core.__destructure_map(map__19007);
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__19007__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(state_ref,shadow.remote.runtime.shared.add_extension_STAR_,key,spec);
});
shadow.remote.runtime.shared.add_defaults = (function shadow$remote$runtime$shared$add_defaults(runtime){
return shadow.remote.runtime.shared.add_extension(runtime,new cljs.core.Keyword("shadow.remote.runtime.shared","defaults","shadow.remote.runtime.shared/defaults",-1821257543),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"ops","ops",1237330063),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"welcome","welcome",-578152123),(function (p1__19008_SHARP_){
return shadow.remote.runtime.shared.welcome(runtime,p1__19008_SHARP_);
}),new cljs.core.Keyword(null,"unknown-relay-op","unknown-relay-op",170832753),(function (p1__19009_SHARP_){
return shadow.remote.runtime.shared.unknown_relay_op(p1__19009_SHARP_);
}),new cljs.core.Keyword(null,"unknown-op","unknown-op",1900385996),(function (p1__19010_SHARP_){
return shadow.remote.runtime.shared.unknown_op(p1__19010_SHARP_);
}),new cljs.core.Keyword(null,"ping","ping",-1670114784),(function (p1__19011_SHARP_){
return shadow.remote.runtime.shared.ping(runtime,p1__19011_SHARP_);
}),new cljs.core.Keyword(null,"request-supported-ops","request-supported-ops",-1034994502),(function (p1__19012_SHARP_){
return shadow.remote.runtime.shared.request_supported_ops(runtime,p1__19012_SHARP_);
})], null)], null));
});
shadow.remote.runtime.shared.del_extension_STAR_ = (function shadow$remote$runtime$shared$del_extension_STAR_(state,key){
var ext = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"extensions","extensions",-1103629196),key], null));
if(cljs.core.not(ext)){
return state;
} else {
return cljs.core.reduce_kv((function (state__$1,op_kw,op_handler){
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$4(state__$1,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ops","ops",1237330063)], null),cljs.core.dissoc,op_kw);
}),cljs.core.update.cljs$core$IFn$_invoke$arity$4(state,new cljs.core.Keyword(null,"extensions","extensions",-1103629196),cljs.core.dissoc,key),new cljs.core.Keyword(null,"ops","ops",1237330063).cljs$core$IFn$_invoke$arity$1(ext));
}
});
shadow.remote.runtime.shared.del_extension = (function shadow$remote$runtime$shared$del_extension(p__19013,key){
var map__19014 = p__19013;
var map__19014__$1 = cljs.core.__destructure_map(map__19014);
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__19014__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(state_ref,shadow.remote.runtime.shared.del_extension_STAR_,key);
});
shadow.remote.runtime.shared.unhandled_call_result = (function shadow$remote$runtime$shared$unhandled_call_result(call_config,msg){
return console.warn("unhandled call result",msg,call_config);
});
shadow.remote.runtime.shared.unhandled_client_not_found = (function shadow$remote$runtime$shared$unhandled_client_not_found(p__19015,msg){
var map__19016 = p__19015;
var map__19016__$1 = cljs.core.__destructure_map(map__19016);
var runtime = map__19016__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__19016__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
return shadow.remote.runtime.shared.trigger_BANG_.cljs$core$IFn$_invoke$arity$variadic(runtime,new cljs.core.Keyword(null,"on-client-not-found","on-client-not-found",-642452849),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([msg], 0));
});
shadow.remote.runtime.shared.reply_unknown_op = (function shadow$remote$runtime$shared$reply_unknown_op(runtime,msg){
return shadow.remote.runtime.shared.reply(runtime,msg,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"unknown-op","unknown-op",1900385996),new cljs.core.Keyword(null,"msg","msg",-1386103444),msg], null));
});
shadow.remote.runtime.shared.process = (function shadow$remote$runtime$shared$process(p__19017,p__19018){
var map__19019 = p__19017;
var map__19019__$1 = cljs.core.__destructure_map(map__19019);
var runtime = map__19019__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__19019__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var map__19020 = p__19018;
var map__19020__$1 = cljs.core.__destructure_map(map__19020);
var msg = map__19020__$1;
var op = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__19020__$1,new cljs.core.Keyword(null,"op","op",-1882987955));
var call_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__19020__$1,new cljs.core.Keyword(null,"call-id","call-id",1043012968));
var state = cljs.core.deref(state_ref);
var op_handler = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ops","ops",1237330063),op], null));
if(cljs.core.truth_(call_id)){
var cfg = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"call-handlers","call-handlers",386605551),call_id], null));
var call_handler = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(cfg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"handlers","handlers",79528781),op], null));
if(cljs.core.truth_(call_handler)){
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(state_ref,cljs.core.update,new cljs.core.Keyword(null,"call-handlers","call-handlers",386605551),cljs.core.dissoc,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([call_id], 0));

return (call_handler.cljs$core$IFn$_invoke$arity$1 ? call_handler.cljs$core$IFn$_invoke$arity$1(msg) : call_handler.call(null,msg));
} else {
if(cljs.core.truth_(op_handler)){
return (op_handler.cljs$core$IFn$_invoke$arity$1 ? op_handler.cljs$core$IFn$_invoke$arity$1(msg) : op_handler.call(null,msg));
} else {
return shadow.remote.runtime.shared.unhandled_call_result(cfg,msg);

}
}
} else {
if(cljs.core.truth_(op_handler)){
return (op_handler.cljs$core$IFn$_invoke$arity$1 ? op_handler.cljs$core$IFn$_invoke$arity$1(msg) : op_handler.call(null,msg));
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-not-found","client-not-found",-1754042614),op)){
return shadow.remote.runtime.shared.unhandled_client_not_found(runtime,msg);
} else {
return shadow.remote.runtime.shared.reply_unknown_op(runtime,msg);

}
}
}
});
shadow.remote.runtime.shared.run_on_idle = (function shadow$remote$runtime$shared$run_on_idle(state_ref){
var seq__19025 = cljs.core.seq(cljs.core.vals(new cljs.core.Keyword(null,"extensions","extensions",-1103629196).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref))));
var chunk__19027 = null;
var count__19028 = (0);
var i__19029 = (0);
while(true){
if((i__19029 < count__19028)){
var map__19033 = chunk__19027.cljs$core$IIndexed$_nth$arity$2(null,i__19029);
var map__19033__$1 = cljs.core.__destructure_map(map__19033);
var on_idle = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__19033__$1,new cljs.core.Keyword(null,"on-idle","on-idle",2044706602));
if(cljs.core.truth_(on_idle)){
(on_idle.cljs$core$IFn$_invoke$arity$0 ? on_idle.cljs$core$IFn$_invoke$arity$0() : on_idle.call(null));


var G__19089 = seq__19025;
var G__19090 = chunk__19027;
var G__19091 = count__19028;
var G__19092 = (i__19029 + (1));
seq__19025 = G__19089;
chunk__19027 = G__19090;
count__19028 = G__19091;
i__19029 = G__19092;
continue;
} else {
var G__19093 = seq__19025;
var G__19094 = chunk__19027;
var G__19095 = count__19028;
var G__19096 = (i__19029 + (1));
seq__19025 = G__19093;
chunk__19027 = G__19094;
count__19028 = G__19095;
i__19029 = G__19096;
continue;
}
} else {
var temp__5804__auto__ = cljs.core.seq(seq__19025);
if(temp__5804__auto__){
var seq__19025__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__19025__$1)){
var c__5568__auto__ = cljs.core.chunk_first(seq__19025__$1);
var G__19101 = cljs.core.chunk_rest(seq__19025__$1);
var G__19102 = c__5568__auto__;
var G__19103 = cljs.core.count(c__5568__auto__);
var G__19104 = (0);
seq__19025 = G__19101;
chunk__19027 = G__19102;
count__19028 = G__19103;
i__19029 = G__19104;
continue;
} else {
var map__19034 = cljs.core.first(seq__19025__$1);
var map__19034__$1 = cljs.core.__destructure_map(map__19034);
var on_idle = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__19034__$1,new cljs.core.Keyword(null,"on-idle","on-idle",2044706602));
if(cljs.core.truth_(on_idle)){
(on_idle.cljs$core$IFn$_invoke$arity$0 ? on_idle.cljs$core$IFn$_invoke$arity$0() : on_idle.call(null));


var G__19109 = cljs.core.next(seq__19025__$1);
var G__19110 = null;
var G__19111 = (0);
var G__19112 = (0);
seq__19025 = G__19109;
chunk__19027 = G__19110;
count__19028 = G__19111;
i__19029 = G__19112;
continue;
} else {
var G__19113 = cljs.core.next(seq__19025__$1);
var G__19114 = null;
var G__19115 = (0);
var G__19116 = (0);
seq__19025 = G__19113;
chunk__19027 = G__19114;
count__19028 = G__19115;
i__19029 = G__19116;
continue;
}
}
} else {
return null;
}
}
break;
}
});

//# sourceMappingURL=shadow.remote.runtime.shared.js.map
