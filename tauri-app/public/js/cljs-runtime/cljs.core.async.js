goog.provide('cljs.core.async');
goog.scope(function(){
  cljs.core.async.goog$module$goog$array = goog.module.get('goog.array');
});
cljs.core.async.fn_handler = (function cljs$core$async$fn_handler(var_args){
var G__15723 = arguments.length;
switch (G__15723) {
case 1:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1 = (function (f){
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(f,true);
}));

(cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2 = (function (f,blockable){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async15732 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async15732 = (function (f,blockable,meta15733){
this.f = f;
this.blockable = blockable;
this.meta15733 = meta15733;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async15732.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_15734,meta15733__$1){
var self__ = this;
var _15734__$1 = this;
return (new cljs.core.async.t_cljs$core$async15732(self__.f,self__.blockable,meta15733__$1));
}));

(cljs.core.async.t_cljs$core$async15732.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_15734){
var self__ = this;
var _15734__$1 = this;
return self__.meta15733;
}));

(cljs.core.async.t_cljs$core$async15732.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async15732.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async15732.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.blockable;
}));

(cljs.core.async.t_cljs$core$async15732.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
}));

(cljs.core.async.t_cljs$core$async15732.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"blockable","blockable",-28395259,null),new cljs.core.Symbol(null,"meta15733","meta15733",-2062529307,null)], null);
}));

(cljs.core.async.t_cljs$core$async15732.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async15732.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async15732");

(cljs.core.async.t_cljs$core$async15732.cljs$lang$ctorPrWriter = (function (this__5330__auto__,writer__5331__auto__,opt__5332__auto__){
return cljs.core._write(writer__5331__auto__,"cljs.core.async/t_cljs$core$async15732");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async15732.
 */
cljs.core.async.__GT_t_cljs$core$async15732 = (function cljs$core$async$__GT_t_cljs$core$async15732(f__$1,blockable__$1,meta15733){
return (new cljs.core.async.t_cljs$core$async15732(f__$1,blockable__$1,meta15733));
});

}

return (new cljs.core.async.t_cljs$core$async15732(f,blockable,cljs.core.PersistentArrayMap.EMPTY));
}));

(cljs.core.async.fn_handler.cljs$lang$maxFixedArity = 2);

/**
 * Returns a fixed buffer of size n. When full, puts will block/park.
 */
cljs.core.async.buffer = (function cljs$core$async$buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete but
 *   val will be dropped (no transfer).
 */
cljs.core.async.dropping_buffer = (function cljs$core$async$dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete, and be
 *   buffered, but oldest elements in buffer will be dropped (not
 *   transferred).
 */
cljs.core.async.sliding_buffer = (function cljs$core$async$sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer(n);
});
/**
 * Returns true if a channel created with buff will never block. That is to say,
 * puts into this buffer will never cause the buffer to be full. 
 */
cljs.core.async.unblocking_buffer_QMARK_ = (function cljs$core$async$unblocking_buffer_QMARK_(buff){
if((!((buff == null)))){
if(((false) || ((cljs.core.PROTOCOL_SENTINEL === buff.cljs$core$async$impl$protocols$UnblockingBuffer$)))){
return true;
} else {
if((!buff.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
}
});
/**
 * Creates a channel with an optional buffer, an optional transducer (like (map f),
 *   (filter p) etc or a composition thereof), and an optional exception handler.
 *   If buf-or-n is a number, will create and use a fixed buffer of that size. If a
 *   transducer is supplied a buffer must be specified. ex-handler must be a
 *   fn of one argument - if an exception occurs during transformation it will be called
 *   with the thrown value as an argument, and any non-nil return value will be placed
 *   in the channel.
 */
cljs.core.async.chan = (function cljs$core$async$chan(var_args){
var G__15762 = arguments.length;
switch (G__15762) {
case 0:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1 = (function (buf_or_n){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,null,null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2 = (function (buf_or_n,xform){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,xform,null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error(["Assert failed: ","buffer must be supplied when transducer is","\n","buf-or-n"].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.cljs$core$IFn$_invoke$arity$3(((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer(buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
}));

(cljs.core.async.chan.cljs$lang$maxFixedArity = 3);

/**
 * Creates a promise channel with an optional transducer, and an optional
 *   exception-handler. A promise channel can take exactly one value that consumers
 *   will receive. Once full, puts complete but val is dropped (no transfer).
 *   Consumers will block until either a value is placed in the channel or the
 *   channel is closed. See chan for the semantics of xform and ex-handler.
 */
cljs.core.async.promise_chan = (function cljs$core$async$promise_chan(var_args){
var G__15771 = arguments.length;
switch (G__15771) {
case 0:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1(null);
}));

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1 = (function (xform){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2(xform,null);
}));

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2 = (function (xform,ex_handler){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(cljs.core.async.impl.buffers.promise_buffer(),xform,ex_handler);
}));

(cljs.core.async.promise_chan.cljs$lang$maxFixedArity = 2);

/**
 * Returns a channel that will close after msecs
 */
cljs.core.async.timeout = (function cljs$core$async$timeout(msecs){
return cljs.core.async.impl.timers.timeout(msecs);
});
/**
 * takes a val from port. Must be called inside a (go ...) block. Will
 *   return nil if closed. Will park if nothing is available.
 *   Returns true unless port is already closed
 */
cljs.core.async._LT__BANG_ = (function cljs$core$async$_LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
 * Asynchronously takes a val from port, passing to fn1. Will pass nil
 * if closed. If on-caller? (default true) is true, and value is
 * immediately available, will call fn1 on calling thread.
 * Returns nil.
 */
cljs.core.async.take_BANG_ = (function cljs$core$async$take_BANG_(var_args){
var G__15773 = arguments.length;
switch (G__15773) {
case 2:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,fn1){
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3(port,fn1,true);
}));

(cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(ret)){
var val_17489 = cljs.core.deref(ret);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_17489) : fn1.call(null,val_17489));
} else {
cljs.core.async.impl.dispatch.run((function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_17489) : fn1.call(null,val_17489));
}));
}
} else {
}

return null;
}));

(cljs.core.async.take_BANG_.cljs$lang$maxFixedArity = 3);

cljs.core.async.nop = (function cljs$core$async$nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(cljs.core.async.nop);
/**
 * puts a val into port. nil values are not allowed. Must be called
 *   inside a (go ...) block. Will park if no buffer space is available.
 *   Returns true unless port is already closed.
 */
cljs.core.async._GT__BANG_ = (function cljs$core$async$_GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
 * Asynchronously puts a val into port, calling fn1 (if supplied) when
 * complete. nil values are not allowed. Will throw if closed. If
 * on-caller? (default true) is true, and the put is immediately
 * accepted, will call fn1 on calling thread.  Returns nil.
 */
cljs.core.async.put_BANG_ = (function cljs$core$async$put_BANG_(var_args){
var G__15775 = arguments.length;
switch (G__15775) {
case 2:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,val){
var temp__5802__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__5802__auto__)){
var ret = temp__5802__auto__;
return cljs.core.deref(ret);
} else {
return true;
}
}));

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,val,fn1){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4(port,val,fn1,true);
}));

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__5802__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(temp__5802__auto__)){
var retb = temp__5802__auto__;
var ret = cljs.core.deref(retb);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
} else {
cljs.core.async.impl.dispatch.run((function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
}));
}

return ret;
} else {
return true;
}
}));

(cljs.core.async.put_BANG_.cljs$lang$maxFixedArity = 4);

cljs.core.async.close_BANG_ = (function cljs$core$async$close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_(port);
});
cljs.core.async.random_array = (function cljs$core$async$random_array(n){
var a = (new Array(n));
var n__5636__auto___17497 = n;
var x_17498 = (0);
while(true){
if((x_17498 < n__5636__auto___17497)){
(a[x_17498] = x_17498);

var G__17499 = (x_17498 + (1));
x_17498 = G__17499;
continue;
} else {
}
break;
}

cljs.core.async.goog$module$goog$array.shuffle(a);

return a;
});
cljs.core.async.alt_flag = (function cljs$core$async$alt_flag(){
var flag = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(true);
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async15776 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async15776 = (function (flag,meta15777){
this.flag = flag;
this.meta15777 = meta15777;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async15776.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_15778,meta15777__$1){
var self__ = this;
var _15778__$1 = this;
return (new cljs.core.async.t_cljs$core$async15776(self__.flag,meta15777__$1));
}));

(cljs.core.async.t_cljs$core$async15776.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_15778){
var self__ = this;
var _15778__$1 = this;
return self__.meta15777;
}));

(cljs.core.async.t_cljs$core$async15776.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async15776.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref(self__.flag);
}));

(cljs.core.async.t_cljs$core$async15776.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async15776.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.flag,null);

return true;
}));

(cljs.core.async.t_cljs$core$async15776.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"meta15777","meta15777",996557410,null)], null);
}));

(cljs.core.async.t_cljs$core$async15776.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async15776.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async15776");

(cljs.core.async.t_cljs$core$async15776.cljs$lang$ctorPrWriter = (function (this__5330__auto__,writer__5331__auto__,opt__5332__auto__){
return cljs.core._write(writer__5331__auto__,"cljs.core.async/t_cljs$core$async15776");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async15776.
 */
cljs.core.async.__GT_t_cljs$core$async15776 = (function cljs$core$async$alt_flag_$___GT_t_cljs$core$async15776(flag__$1,meta15777){
return (new cljs.core.async.t_cljs$core$async15776(flag__$1,meta15777));
});

}

return (new cljs.core.async.t_cljs$core$async15776(flag,cljs.core.PersistentArrayMap.EMPTY));
});
cljs.core.async.alt_handler = (function cljs$core$async$alt_handler(flag,cb){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async15779 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async15779 = (function (flag,cb,meta15780){
this.flag = flag;
this.cb = cb;
this.meta15780 = meta15780;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async15779.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_15781,meta15780__$1){
var self__ = this;
var _15781__$1 = this;
return (new cljs.core.async.t_cljs$core$async15779(self__.flag,self__.cb,meta15780__$1));
}));

(cljs.core.async.t_cljs$core$async15779.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_15781){
var self__ = this;
var _15781__$1 = this;
return self__.meta15780;
}));

(cljs.core.async.t_cljs$core$async15779.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async15779.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.flag);
}));

(cljs.core.async.t_cljs$core$async15779.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async15779.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit(self__.flag);

return self__.cb;
}));

(cljs.core.async.t_cljs$core$async15779.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null),new cljs.core.Symbol(null,"meta15780","meta15780",-707285594,null)], null);
}));

(cljs.core.async.t_cljs$core$async15779.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async15779.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async15779");

(cljs.core.async.t_cljs$core$async15779.cljs$lang$ctorPrWriter = (function (this__5330__auto__,writer__5331__auto__,opt__5332__auto__){
return cljs.core._write(writer__5331__auto__,"cljs.core.async/t_cljs$core$async15779");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async15779.
 */
cljs.core.async.__GT_t_cljs$core$async15779 = (function cljs$core$async$alt_handler_$___GT_t_cljs$core$async15779(flag__$1,cb__$1,meta15780){
return (new cljs.core.async.t_cljs$core$async15779(flag__$1,cb__$1,meta15780));
});

}

return (new cljs.core.async.t_cljs$core$async15779(flag,cb,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * returns derefable [val port] if immediate, nil if enqueued
 */
cljs.core.async.do_alts = (function cljs$core$async$do_alts(fret,ports,opts){
if((cljs.core.count(ports) > (0))){
} else {
throw (new Error(["Assert failed: ","alts must have at least one channel operation","\n","(pos? (count ports))"].join('')));
}

var flag = cljs.core.async.alt_flag();
var n = cljs.core.count(ports);
var idxs = cljs.core.async.random_array(n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.cljs$core$IFn$_invoke$arity$2(ports,idx);
var wport = ((cljs.core.vector_QMARK_(port))?(port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((0)) : port.call(null,(0))):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = (port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((1)) : port.call(null,(1)));
return cljs.core.async.impl.protocols.put_BANG_(wport,val,cljs.core.async.alt_handler(flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__15789_SHARP_){
var G__15794 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__15789_SHARP_,wport], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__15794) : fret.call(null,G__15794));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.alt_handler(flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__15790_SHARP_){
var G__15795 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__15790_SHARP_,port], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__15795) : fret.call(null,G__15795));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref(vbox),(function (){var or__5045__auto__ = wport;
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return port;
}
})()], null));
} else {
var G__17500 = (i + (1));
i = G__17500;
continue;
}
} else {
return null;
}
break;
}
})();
var or__5045__auto__ = ret;
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
if(cljs.core.contains_QMARK_(opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__5804__auto__ = (function (){var and__5043__auto__ = flag.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1(null);
if(cljs.core.truth_(and__5043__auto__)){
return flag.cljs$core$async$impl$protocols$Handler$commit$arity$1(null);
} else {
return and__5043__auto__;
}
})();
if(cljs.core.truth_(temp__5804__auto__)){
var got = temp__5804__auto__;
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Completes at most one of several channel operations. Must be called
 * inside a (go ...) block. ports is a vector of channel endpoints,
 * which can be either a channel to take from or a vector of
 *   [channel-to-put-to val-to-put], in any combination. Takes will be
 *   made as if by <!, and puts will be made as if by >!. Unless
 *   the :priority option is true, if more than one port operation is
 *   ready a non-deterministic choice will be made. If no operation is
 *   ready and a :default value is supplied, [default-val :default] will
 *   be returned, otherwise alts! will park until the first operation to
 *   become ready completes. Returns [val port] of the completed
 *   operation, where val is the value taken for takes, and a
 *   boolean (true unless already closed, as per put!) for puts.
 * 
 *   opts are passed as :key val ... Supported options:
 * 
 *   :default val - the value to use if none of the operations are immediately ready
 *   :priority true - (default nil) when true, the operations will be tried in order.
 * 
 *   Note: there is no guarantee that the port exps or val exprs will be
 *   used, nor in what order should they be, so they should not be
 *   depended upon for side effects.
 */
cljs.core.async.alts_BANG_ = (function cljs$core$async$alts_BANG_(var_args){
var args__5775__auto__ = [];
var len__5769__auto___17501 = arguments.length;
var i__5770__auto___17502 = (0);
while(true){
if((i__5770__auto___17502 < len__5769__auto___17501)){
args__5775__auto__.push((arguments[i__5770__auto___17502]));

var G__17503 = (i__5770__auto___17502 + (1));
i__5770__auto___17502 = G__17503;
continue;
} else {
}
break;
}

var argseq__5776__auto__ = ((((1) < args__5775__auto__.length))?(new cljs.core.IndexedSeq(args__5775__auto__.slice((1)),(0),null)):null);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__5776__auto__);
});

(cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ports,p__15813){
var map__15814 = p__15813;
var map__15814__$1 = cljs.core.__destructure_map(map__15814);
var opts = map__15814__$1;
throw (new Error("alts! used not in (go ...) block"));
}));

(cljs.core.async.alts_BANG_.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(cljs.core.async.alts_BANG_.cljs$lang$applyTo = (function (seq15801){
var G__15802 = cljs.core.first(seq15801);
var seq15801__$1 = cljs.core.next(seq15801);
var self__5754__auto__ = this;
return self__5754__auto__.cljs$core$IFn$_invoke$arity$variadic(G__15802,seq15801__$1);
}));

/**
 * Puts a val into port if it's possible to do so immediately.
 *   nil values are not allowed. Never blocks. Returns true if offer succeeds.
 */
cljs.core.async.offer_BANG_ = (function cljs$core$async$offer_BANG_(port,val){
var ret = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes a val from port if it's possible to do so immediately.
 *   Never blocks. Returns value if successful, nil otherwise.
 */
cljs.core.async.poll_BANG_ = (function cljs$core$async$poll_BANG_(port){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes
 */
cljs.core.async.pipe = (function cljs$core$async$pipe(var_args){
var G__15840 = arguments.length;
switch (G__15840) {
case 2:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2 = (function (from,to){
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3(from,to,true);
}));

(cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3 = (function (from,to,close_QMARK_){
var c__15644__auto___17507 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__15645__auto__ = (function (){var switch__15400__auto__ = (function (state_15898){
var state_val_15899 = (state_15898[(1)]);
if((state_val_15899 === (7))){
var inst_15892 = (state_15898[(2)]);
var state_15898__$1 = state_15898;
var statearr_15925_17508 = state_15898__$1;
(statearr_15925_17508[(2)] = inst_15892);

(statearr_15925_17508[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_15899 === (1))){
var state_15898__$1 = state_15898;
var statearr_15928_17509 = state_15898__$1;
(statearr_15928_17509[(2)] = null);

(statearr_15928_17509[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_15899 === (4))){
var inst_15872 = (state_15898[(7)]);
var inst_15872__$1 = (state_15898[(2)]);
var inst_15873 = (inst_15872__$1 == null);
var state_15898__$1 = (function (){var statearr_15933 = state_15898;
(statearr_15933[(7)] = inst_15872__$1);

return statearr_15933;
})();
if(cljs.core.truth_(inst_15873)){
var statearr_15934_17510 = state_15898__$1;
(statearr_15934_17510[(1)] = (5));

} else {
var statearr_15935_17511 = state_15898__$1;
(statearr_15935_17511[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_15899 === (13))){
var state_15898__$1 = state_15898;
var statearr_15943_17512 = state_15898__$1;
(statearr_15943_17512[(2)] = null);

(statearr_15943_17512[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_15899 === (6))){
var inst_15872 = (state_15898[(7)]);
var state_15898__$1 = state_15898;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_15898__$1,(11),to,inst_15872);
} else {
if((state_val_15899 === (3))){
var inst_15896 = (state_15898[(2)]);
var state_15898__$1 = state_15898;
return cljs.core.async.impl.ioc_helpers.return_chan(state_15898__$1,inst_15896);
} else {
if((state_val_15899 === (12))){
var state_15898__$1 = state_15898;
var statearr_15963_17513 = state_15898__$1;
(statearr_15963_17513[(2)] = null);

(statearr_15963_17513[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_15899 === (2))){
var state_15898__$1 = state_15898;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_15898__$1,(4),from);
} else {
if((state_val_15899 === (11))){
var inst_15884 = (state_15898[(2)]);
var state_15898__$1 = state_15898;
if(cljs.core.truth_(inst_15884)){
var statearr_15965_17514 = state_15898__$1;
(statearr_15965_17514[(1)] = (12));

} else {
var statearr_15968_17515 = state_15898__$1;
(statearr_15968_17515[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_15899 === (9))){
var state_15898__$1 = state_15898;
var statearr_15973_17516 = state_15898__$1;
(statearr_15973_17516[(2)] = null);

(statearr_15973_17516[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_15899 === (5))){
var state_15898__$1 = state_15898;
if(cljs.core.truth_(close_QMARK_)){
var statearr_15974_17517 = state_15898__$1;
(statearr_15974_17517[(1)] = (8));

} else {
var statearr_15977_17518 = state_15898__$1;
(statearr_15977_17518[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_15899 === (14))){
var inst_15890 = (state_15898[(2)]);
var state_15898__$1 = state_15898;
var statearr_15982_17519 = state_15898__$1;
(statearr_15982_17519[(2)] = inst_15890);

(statearr_15982_17519[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_15899 === (10))){
var inst_15881 = (state_15898[(2)]);
var state_15898__$1 = state_15898;
var statearr_15983_17520 = state_15898__$1;
(statearr_15983_17520[(2)] = inst_15881);

(statearr_15983_17520[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_15899 === (8))){
var inst_15877 = cljs.core.async.close_BANG_(to);
var state_15898__$1 = state_15898;
var statearr_15984_17521 = state_15898__$1;
(statearr_15984_17521[(2)] = inst_15877);

(statearr_15984_17521[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__15401__auto__ = null;
var cljs$core$async$state_machine__15401__auto____0 = (function (){
var statearr_15991 = [null,null,null,null,null,null,null,null];
(statearr_15991[(0)] = cljs$core$async$state_machine__15401__auto__);

(statearr_15991[(1)] = (1));

return statearr_15991;
});
var cljs$core$async$state_machine__15401__auto____1 = (function (state_15898){
while(true){
var ret_value__15402__auto__ = (function (){try{while(true){
var result__15403__auto__ = switch__15400__auto__(state_15898);
if(cljs.core.keyword_identical_QMARK_(result__15403__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__15403__auto__;
}
break;
}
}catch (e15993){var ex__15404__auto__ = e15993;
var statearr_16000_17522 = state_15898;
(statearr_16000_17522[(2)] = ex__15404__auto__);


if(cljs.core.seq((state_15898[(4)]))){
var statearr_16005_17523 = state_15898;
(statearr_16005_17523[(1)] = cljs.core.first((state_15898[(4)])));

} else {
throw ex__15404__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__15402__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__17524 = state_15898;
state_15898 = G__17524;
continue;
} else {
return ret_value__15402__auto__;
}
break;
}
});
cljs$core$async$state_machine__15401__auto__ = function(state_15898){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__15401__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__15401__auto____1.call(this,state_15898);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__15401__auto____0;
cljs$core$async$state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__15401__auto____1;
return cljs$core$async$state_machine__15401__auto__;
})()
})();
var state__15646__auto__ = (function (){var statearr_16009 = f__15645__auto__();
(statearr_16009[(6)] = c__15644__auto___17507);

return statearr_16009;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__15646__auto__);
}));


return to;
}));

(cljs.core.async.pipe.cljs$lang$maxFixedArity = 3);

cljs.core.async.pipeline_STAR_ = (function cljs$core$async$pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error("Assert failed: (pos? n)"));
}

var jobs = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var results = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var process__$1 = (function (p__16022){
var vec__16024 = p__16022;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__16024,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__16024,(1),null);
var job = vec__16024;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((1),xf,ex_handler);
var c__15644__auto___17526 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__15645__auto__ = (function (){var switch__15400__auto__ = (function (state_16031){
var state_val_16032 = (state_16031[(1)]);
if((state_val_16032 === (1))){
var state_16031__$1 = state_16031;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_16031__$1,(2),res,v);
} else {
if((state_val_16032 === (2))){
var inst_16028 = (state_16031[(2)]);
var inst_16029 = cljs.core.async.close_BANG_(res);
var state_16031__$1 = (function (){var statearr_16034 = state_16031;
(statearr_16034[(7)] = inst_16028);

return statearr_16034;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_16031__$1,inst_16029);
} else {
return null;
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____0 = (function (){
var statearr_16035 = [null,null,null,null,null,null,null,null];
(statearr_16035[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__);

(statearr_16035[(1)] = (1));

return statearr_16035;
});
var cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____1 = (function (state_16031){
while(true){
var ret_value__15402__auto__ = (function (){try{while(true){
var result__15403__auto__ = switch__15400__auto__(state_16031);
if(cljs.core.keyword_identical_QMARK_(result__15403__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__15403__auto__;
}
break;
}
}catch (e16036){var ex__15404__auto__ = e16036;
var statearr_16037_17529 = state_16031;
(statearr_16037_17529[(2)] = ex__15404__auto__);


if(cljs.core.seq((state_16031[(4)]))){
var statearr_16038_17530 = state_16031;
(statearr_16038_17530[(1)] = cljs.core.first((state_16031[(4)])));

} else {
throw ex__15404__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__15402__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__17531 = state_16031;
state_16031 = G__17531;
continue;
} else {
return ret_value__15402__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__ = function(state_16031){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____1.call(this,state_16031);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__;
})()
})();
var state__15646__auto__ = (function (){var statearr_16039 = f__15645__auto__();
(statearr_16039[(6)] = c__15644__auto___17526);

return statearr_16039;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__15646__auto__);
}));


cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});
var async = (function (p__16041){
var vec__16042 = p__16041;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__16042,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__16042,(1),null);
var job = vec__16042;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
(xf.cljs$core$IFn$_invoke$arity$2 ? xf.cljs$core$IFn$_invoke$arity$2(v,res) : xf.call(null,v,res));

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});
var n__5636__auto___17534 = n;
var __17535 = (0);
while(true){
if((__17535 < n__5636__auto___17534)){
var G__16045_17536 = type;
var G__16045_17537__$1 = (((G__16045_17536 instanceof cljs.core.Keyword))?G__16045_17536.fqn:null);
switch (G__16045_17537__$1) {
case "compute":
var c__15644__auto___17539 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__17535,c__15644__auto___17539,G__16045_17536,G__16045_17537__$1,n__5636__auto___17534,jobs,results,process__$1,async){
return (function (){
var f__15645__auto__ = (function (){var switch__15400__auto__ = ((function (__17535,c__15644__auto___17539,G__16045_17536,G__16045_17537__$1,n__5636__auto___17534,jobs,results,process__$1,async){
return (function (state_16059){
var state_val_16060 = (state_16059[(1)]);
if((state_val_16060 === (1))){
var state_16059__$1 = state_16059;
var statearr_16065_17540 = state_16059__$1;
(statearr_16065_17540[(2)] = null);

(statearr_16065_17540[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16060 === (2))){
var state_16059__$1 = state_16059;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_16059__$1,(4),jobs);
} else {
if((state_val_16060 === (3))){
var inst_16057 = (state_16059[(2)]);
var state_16059__$1 = state_16059;
return cljs.core.async.impl.ioc_helpers.return_chan(state_16059__$1,inst_16057);
} else {
if((state_val_16060 === (4))){
var inst_16049 = (state_16059[(2)]);
var inst_16050 = process__$1(inst_16049);
var state_16059__$1 = state_16059;
if(cljs.core.truth_(inst_16050)){
var statearr_16074_17541 = state_16059__$1;
(statearr_16074_17541[(1)] = (5));

} else {
var statearr_16075_17542 = state_16059__$1;
(statearr_16075_17542[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16060 === (5))){
var state_16059__$1 = state_16059;
var statearr_16076_17544 = state_16059__$1;
(statearr_16076_17544[(2)] = null);

(statearr_16076_17544[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16060 === (6))){
var state_16059__$1 = state_16059;
var statearr_16077_17545 = state_16059__$1;
(statearr_16077_17545[(2)] = null);

(statearr_16077_17545[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16060 === (7))){
var inst_16055 = (state_16059[(2)]);
var state_16059__$1 = state_16059;
var statearr_16078_17547 = state_16059__$1;
(statearr_16078_17547[(2)] = inst_16055);

(statearr_16078_17547[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__17535,c__15644__auto___17539,G__16045_17536,G__16045_17537__$1,n__5636__auto___17534,jobs,results,process__$1,async))
;
return ((function (__17535,switch__15400__auto__,c__15644__auto___17539,G__16045_17536,G__16045_17537__$1,n__5636__auto___17534,jobs,results,process__$1,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____0 = (function (){
var statearr_16079 = [null,null,null,null,null,null,null];
(statearr_16079[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__);

(statearr_16079[(1)] = (1));

return statearr_16079;
});
var cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____1 = (function (state_16059){
while(true){
var ret_value__15402__auto__ = (function (){try{while(true){
var result__15403__auto__ = switch__15400__auto__(state_16059);
if(cljs.core.keyword_identical_QMARK_(result__15403__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__15403__auto__;
}
break;
}
}catch (e16080){var ex__15404__auto__ = e16080;
var statearr_16081_17548 = state_16059;
(statearr_16081_17548[(2)] = ex__15404__auto__);


if(cljs.core.seq((state_16059[(4)]))){
var statearr_16082_17549 = state_16059;
(statearr_16082_17549[(1)] = cljs.core.first((state_16059[(4)])));

} else {
throw ex__15404__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__15402__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__17550 = state_16059;
state_16059 = G__17550;
continue;
} else {
return ret_value__15402__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__ = function(state_16059){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____1.call(this,state_16059);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__;
})()
;})(__17535,switch__15400__auto__,c__15644__auto___17539,G__16045_17536,G__16045_17537__$1,n__5636__auto___17534,jobs,results,process__$1,async))
})();
var state__15646__auto__ = (function (){var statearr_16083 = f__15645__auto__();
(statearr_16083[(6)] = c__15644__auto___17539);

return statearr_16083;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__15646__auto__);
});})(__17535,c__15644__auto___17539,G__16045_17536,G__16045_17537__$1,n__5636__auto___17534,jobs,results,process__$1,async))
);


break;
case "async":
var c__15644__auto___17551 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__17535,c__15644__auto___17551,G__16045_17536,G__16045_17537__$1,n__5636__auto___17534,jobs,results,process__$1,async){
return (function (){
var f__15645__auto__ = (function (){var switch__15400__auto__ = ((function (__17535,c__15644__auto___17551,G__16045_17536,G__16045_17537__$1,n__5636__auto___17534,jobs,results,process__$1,async){
return (function (state_16096){
var state_val_16097 = (state_16096[(1)]);
if((state_val_16097 === (1))){
var state_16096__$1 = state_16096;
var statearr_16098_17553 = state_16096__$1;
(statearr_16098_17553[(2)] = null);

(statearr_16098_17553[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16097 === (2))){
var state_16096__$1 = state_16096;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_16096__$1,(4),jobs);
} else {
if((state_val_16097 === (3))){
var inst_16094 = (state_16096[(2)]);
var state_16096__$1 = state_16096;
return cljs.core.async.impl.ioc_helpers.return_chan(state_16096__$1,inst_16094);
} else {
if((state_val_16097 === (4))){
var inst_16086 = (state_16096[(2)]);
var inst_16087 = async(inst_16086);
var state_16096__$1 = state_16096;
if(cljs.core.truth_(inst_16087)){
var statearr_16099_17555 = state_16096__$1;
(statearr_16099_17555[(1)] = (5));

} else {
var statearr_16100_17556 = state_16096__$1;
(statearr_16100_17556[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16097 === (5))){
var state_16096__$1 = state_16096;
var statearr_16101_17557 = state_16096__$1;
(statearr_16101_17557[(2)] = null);

(statearr_16101_17557[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16097 === (6))){
var state_16096__$1 = state_16096;
var statearr_16102_17558 = state_16096__$1;
(statearr_16102_17558[(2)] = null);

(statearr_16102_17558[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16097 === (7))){
var inst_16092 = (state_16096[(2)]);
var state_16096__$1 = state_16096;
var statearr_16103_17559 = state_16096__$1;
(statearr_16103_17559[(2)] = inst_16092);

(statearr_16103_17559[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__17535,c__15644__auto___17551,G__16045_17536,G__16045_17537__$1,n__5636__auto___17534,jobs,results,process__$1,async))
;
return ((function (__17535,switch__15400__auto__,c__15644__auto___17551,G__16045_17536,G__16045_17537__$1,n__5636__auto___17534,jobs,results,process__$1,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____0 = (function (){
var statearr_16104 = [null,null,null,null,null,null,null];
(statearr_16104[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__);

(statearr_16104[(1)] = (1));

return statearr_16104;
});
var cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____1 = (function (state_16096){
while(true){
var ret_value__15402__auto__ = (function (){try{while(true){
var result__15403__auto__ = switch__15400__auto__(state_16096);
if(cljs.core.keyword_identical_QMARK_(result__15403__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__15403__auto__;
}
break;
}
}catch (e16105){var ex__15404__auto__ = e16105;
var statearr_16106_17560 = state_16096;
(statearr_16106_17560[(2)] = ex__15404__auto__);


if(cljs.core.seq((state_16096[(4)]))){
var statearr_16107_17561 = state_16096;
(statearr_16107_17561[(1)] = cljs.core.first((state_16096[(4)])));

} else {
throw ex__15404__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__15402__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__17562 = state_16096;
state_16096 = G__17562;
continue;
} else {
return ret_value__15402__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__ = function(state_16096){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____1.call(this,state_16096);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__;
})()
;})(__17535,switch__15400__auto__,c__15644__auto___17551,G__16045_17536,G__16045_17537__$1,n__5636__auto___17534,jobs,results,process__$1,async))
})();
var state__15646__auto__ = (function (){var statearr_16108 = f__15645__auto__();
(statearr_16108[(6)] = c__15644__auto___17551);

return statearr_16108;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__15646__auto__);
});})(__17535,c__15644__auto___17551,G__16045_17536,G__16045_17537__$1,n__5636__auto___17534,jobs,results,process__$1,async))
);


break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__16045_17537__$1)].join('')));

}

var G__17563 = (__17535 + (1));
__17535 = G__17563;
continue;
} else {
}
break;
}

var c__15644__auto___17564 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__15645__auto__ = (function (){var switch__15400__auto__ = (function (state_16130){
var state_val_16131 = (state_16130[(1)]);
if((state_val_16131 === (7))){
var inst_16126 = (state_16130[(2)]);
var state_16130__$1 = state_16130;
var statearr_16132_17565 = state_16130__$1;
(statearr_16132_17565[(2)] = inst_16126);

(statearr_16132_17565[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16131 === (1))){
var state_16130__$1 = state_16130;
var statearr_16133_17567 = state_16130__$1;
(statearr_16133_17567[(2)] = null);

(statearr_16133_17567[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16131 === (4))){
var inst_16111 = (state_16130[(7)]);
var inst_16111__$1 = (state_16130[(2)]);
var inst_16112 = (inst_16111__$1 == null);
var state_16130__$1 = (function (){var statearr_16134 = state_16130;
(statearr_16134[(7)] = inst_16111__$1);

return statearr_16134;
})();
if(cljs.core.truth_(inst_16112)){
var statearr_16135_17569 = state_16130__$1;
(statearr_16135_17569[(1)] = (5));

} else {
var statearr_16136_17570 = state_16130__$1;
(statearr_16136_17570[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16131 === (6))){
var inst_16111 = (state_16130[(7)]);
var inst_16116 = (state_16130[(8)]);
var inst_16116__$1 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var inst_16117 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_16118 = [inst_16111,inst_16116__$1];
var inst_16119 = (new cljs.core.PersistentVector(null,2,(5),inst_16117,inst_16118,null));
var state_16130__$1 = (function (){var statearr_16137 = state_16130;
(statearr_16137[(8)] = inst_16116__$1);

return statearr_16137;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_16130__$1,(8),jobs,inst_16119);
} else {
if((state_val_16131 === (3))){
var inst_16128 = (state_16130[(2)]);
var state_16130__$1 = state_16130;
return cljs.core.async.impl.ioc_helpers.return_chan(state_16130__$1,inst_16128);
} else {
if((state_val_16131 === (2))){
var state_16130__$1 = state_16130;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_16130__$1,(4),from);
} else {
if((state_val_16131 === (9))){
var inst_16123 = (state_16130[(2)]);
var state_16130__$1 = (function (){var statearr_16138 = state_16130;
(statearr_16138[(9)] = inst_16123);

return statearr_16138;
})();
var statearr_16139_17571 = state_16130__$1;
(statearr_16139_17571[(2)] = null);

(statearr_16139_17571[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16131 === (5))){
var inst_16114 = cljs.core.async.close_BANG_(jobs);
var state_16130__$1 = state_16130;
var statearr_16140_17572 = state_16130__$1;
(statearr_16140_17572[(2)] = inst_16114);

(statearr_16140_17572[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16131 === (8))){
var inst_16116 = (state_16130[(8)]);
var inst_16121 = (state_16130[(2)]);
var state_16130__$1 = (function (){var statearr_16141 = state_16130;
(statearr_16141[(10)] = inst_16121);

return statearr_16141;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_16130__$1,(9),results,inst_16116);
} else {
return null;
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____0 = (function (){
var statearr_16142 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_16142[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__);

(statearr_16142[(1)] = (1));

return statearr_16142;
});
var cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____1 = (function (state_16130){
while(true){
var ret_value__15402__auto__ = (function (){try{while(true){
var result__15403__auto__ = switch__15400__auto__(state_16130);
if(cljs.core.keyword_identical_QMARK_(result__15403__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__15403__auto__;
}
break;
}
}catch (e16143){var ex__15404__auto__ = e16143;
var statearr_16144_17574 = state_16130;
(statearr_16144_17574[(2)] = ex__15404__auto__);


if(cljs.core.seq((state_16130[(4)]))){
var statearr_16145_17576 = state_16130;
(statearr_16145_17576[(1)] = cljs.core.first((state_16130[(4)])));

} else {
throw ex__15404__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__15402__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__17577 = state_16130;
state_16130 = G__17577;
continue;
} else {
return ret_value__15402__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__ = function(state_16130){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____1.call(this,state_16130);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__;
})()
})();
var state__15646__auto__ = (function (){var statearr_16146 = f__15645__auto__();
(statearr_16146[(6)] = c__15644__auto___17564);

return statearr_16146;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__15646__auto__);
}));


var c__15644__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__15645__auto__ = (function (){var switch__15400__auto__ = (function (state_16184){
var state_val_16185 = (state_16184[(1)]);
if((state_val_16185 === (7))){
var inst_16180 = (state_16184[(2)]);
var state_16184__$1 = state_16184;
var statearr_16186_17578 = state_16184__$1;
(statearr_16186_17578[(2)] = inst_16180);

(statearr_16186_17578[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16185 === (20))){
var state_16184__$1 = state_16184;
var statearr_16187_17579 = state_16184__$1;
(statearr_16187_17579[(2)] = null);

(statearr_16187_17579[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16185 === (1))){
var state_16184__$1 = state_16184;
var statearr_16188_17580 = state_16184__$1;
(statearr_16188_17580[(2)] = null);

(statearr_16188_17580[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16185 === (4))){
var inst_16149 = (state_16184[(7)]);
var inst_16149__$1 = (state_16184[(2)]);
var inst_16150 = (inst_16149__$1 == null);
var state_16184__$1 = (function (){var statearr_16189 = state_16184;
(statearr_16189[(7)] = inst_16149__$1);

return statearr_16189;
})();
if(cljs.core.truth_(inst_16150)){
var statearr_16190_17581 = state_16184__$1;
(statearr_16190_17581[(1)] = (5));

} else {
var statearr_16191_17582 = state_16184__$1;
(statearr_16191_17582[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16185 === (15))){
var inst_16162 = (state_16184[(8)]);
var state_16184__$1 = state_16184;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_16184__$1,(18),to,inst_16162);
} else {
if((state_val_16185 === (21))){
var inst_16175 = (state_16184[(2)]);
var state_16184__$1 = state_16184;
var statearr_16192_17583 = state_16184__$1;
(statearr_16192_17583[(2)] = inst_16175);

(statearr_16192_17583[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16185 === (13))){
var inst_16177 = (state_16184[(2)]);
var state_16184__$1 = (function (){var statearr_16193 = state_16184;
(statearr_16193[(9)] = inst_16177);

return statearr_16193;
})();
var statearr_16194_17585 = state_16184__$1;
(statearr_16194_17585[(2)] = null);

(statearr_16194_17585[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16185 === (6))){
var inst_16149 = (state_16184[(7)]);
var state_16184__$1 = state_16184;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_16184__$1,(11),inst_16149);
} else {
if((state_val_16185 === (17))){
var inst_16170 = (state_16184[(2)]);
var state_16184__$1 = state_16184;
if(cljs.core.truth_(inst_16170)){
var statearr_16195_17586 = state_16184__$1;
(statearr_16195_17586[(1)] = (19));

} else {
var statearr_16196_17587 = state_16184__$1;
(statearr_16196_17587[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16185 === (3))){
var inst_16182 = (state_16184[(2)]);
var state_16184__$1 = state_16184;
return cljs.core.async.impl.ioc_helpers.return_chan(state_16184__$1,inst_16182);
} else {
if((state_val_16185 === (12))){
var inst_16159 = (state_16184[(10)]);
var state_16184__$1 = state_16184;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_16184__$1,(14),inst_16159);
} else {
if((state_val_16185 === (2))){
var state_16184__$1 = state_16184;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_16184__$1,(4),results);
} else {
if((state_val_16185 === (19))){
var state_16184__$1 = state_16184;
var statearr_16197_17588 = state_16184__$1;
(statearr_16197_17588[(2)] = null);

(statearr_16197_17588[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16185 === (11))){
var inst_16159 = (state_16184[(2)]);
var state_16184__$1 = (function (){var statearr_16198 = state_16184;
(statearr_16198[(10)] = inst_16159);

return statearr_16198;
})();
var statearr_16199_17589 = state_16184__$1;
(statearr_16199_17589[(2)] = null);

(statearr_16199_17589[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16185 === (9))){
var state_16184__$1 = state_16184;
var statearr_16200_17590 = state_16184__$1;
(statearr_16200_17590[(2)] = null);

(statearr_16200_17590[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16185 === (5))){
var state_16184__$1 = state_16184;
if(cljs.core.truth_(close_QMARK_)){
var statearr_16201_17591 = state_16184__$1;
(statearr_16201_17591[(1)] = (8));

} else {
var statearr_16202_17592 = state_16184__$1;
(statearr_16202_17592[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16185 === (14))){
var inst_16162 = (state_16184[(8)]);
var inst_16164 = (state_16184[(11)]);
var inst_16162__$1 = (state_16184[(2)]);
var inst_16163 = (inst_16162__$1 == null);
var inst_16164__$1 = cljs.core.not(inst_16163);
var state_16184__$1 = (function (){var statearr_16203 = state_16184;
(statearr_16203[(8)] = inst_16162__$1);

(statearr_16203[(11)] = inst_16164__$1);

return statearr_16203;
})();
if(inst_16164__$1){
var statearr_16204_17597 = state_16184__$1;
(statearr_16204_17597[(1)] = (15));

} else {
var statearr_16205_17598 = state_16184__$1;
(statearr_16205_17598[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16185 === (16))){
var inst_16164 = (state_16184[(11)]);
var state_16184__$1 = state_16184;
var statearr_16206_17599 = state_16184__$1;
(statearr_16206_17599[(2)] = inst_16164);

(statearr_16206_17599[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16185 === (10))){
var inst_16156 = (state_16184[(2)]);
var state_16184__$1 = state_16184;
var statearr_16207_17600 = state_16184__$1;
(statearr_16207_17600[(2)] = inst_16156);

(statearr_16207_17600[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16185 === (18))){
var inst_16167 = (state_16184[(2)]);
var state_16184__$1 = state_16184;
var statearr_16208_17601 = state_16184__$1;
(statearr_16208_17601[(2)] = inst_16167);

(statearr_16208_17601[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16185 === (8))){
var inst_16153 = cljs.core.async.close_BANG_(to);
var state_16184__$1 = state_16184;
var statearr_16209_17602 = state_16184__$1;
(statearr_16209_17602[(2)] = inst_16153);

(statearr_16209_17602[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____0 = (function (){
var statearr_16210 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_16210[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__);

(statearr_16210[(1)] = (1));

return statearr_16210;
});
var cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____1 = (function (state_16184){
while(true){
var ret_value__15402__auto__ = (function (){try{while(true){
var result__15403__auto__ = switch__15400__auto__(state_16184);
if(cljs.core.keyword_identical_QMARK_(result__15403__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__15403__auto__;
}
break;
}
}catch (e16211){var ex__15404__auto__ = e16211;
var statearr_16212_17603 = state_16184;
(statearr_16212_17603[(2)] = ex__15404__auto__);


if(cljs.core.seq((state_16184[(4)]))){
var statearr_16213_17604 = state_16184;
(statearr_16213_17604[(1)] = cljs.core.first((state_16184[(4)])));

} else {
throw ex__15404__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__15402__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__17605 = state_16184;
state_16184 = G__17605;
continue;
} else {
return ret_value__15402__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__ = function(state_16184){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____1.call(this,state_16184);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__15401__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__15401__auto__;
})()
})();
var state__15646__auto__ = (function (){var statearr_16214 = f__15645__auto__();
(statearr_16214[(6)] = c__15644__auto__);

return statearr_16214;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__15646__auto__);
}));

return c__15644__auto__;
});
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the async function af, with parallelism n. af
 *   must be a function of two arguments, the first an input value and
 *   the second a channel on which to place the result(s). The
 *   presumption is that af will return immediately, having launched some
 *   asynchronous operation whose completion/callback will put results on
 *   the channel, then close! it. Outputs will be returned in order
 *   relative to the inputs. By default, the to channel will be closed
 *   when the from channel closes, but can be determined by the close?
 *   parameter. Will stop consuming the from channel if the to channel
 *   closes. See also pipeline, pipeline-blocking.
 */
cljs.core.async.pipeline_async = (function cljs$core$async$pipeline_async(var_args){
var G__16216 = arguments.length;
switch (G__16216) {
case 4:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4 = (function (n,to,af,from){
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5(n,to,af,from,true);
}));

(cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_(n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
}));

(cljs.core.async.pipeline_async.cljs$lang$maxFixedArity = 5);

/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the transducer xf, with parallelism n. Because
 *   it is parallel, the transducer will be applied independently to each
 *   element, not across elements, and may produce zero or more outputs
 *   per input.  Outputs will be returned in order relative to the
 *   inputs. By default, the to channel will be closed when the from
 *   channel closes, but can be determined by the close?  parameter. Will
 *   stop consuming the from channel if the to channel closes.
 * 
 *   Note this is supplied for API compatibility with the Clojure version.
 *   Values of N > 1 will not result in actual concurrency in a
 *   single-threaded runtime.
 */
cljs.core.async.pipeline = (function cljs$core$async$pipeline(var_args){
var G__16218 = arguments.length;
switch (G__16218) {
case 4:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
case 6:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4 = (function (n,to,xf,from){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5(n,to,xf,from,true);
}));

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5 = (function (n,to,xf,from,close_QMARK_){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6(n,to,xf,from,close_QMARK_,null);
}));

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
}));

(cljs.core.async.pipeline.cljs$lang$maxFixedArity = 6);

/**
 * Takes a predicate and a source channel and returns a vector of two
 *   channels, the first of which will contain the values for which the
 *   predicate returned true, the second those for which it returned
 *   false.
 * 
 *   The out channels will be unbuffered by default, or two buf-or-ns can
 *   be supplied. The channels will close after the source channel has
 *   closed.
 */
cljs.core.async.split = (function cljs$core$async$split(var_args){
var G__16220 = arguments.length;
switch (G__16220) {
case 2:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.split.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4(p,ch,null,null);
}));

(cljs.core.async.split.cljs$core$IFn$_invoke$arity$4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(t_buf_or_n);
var fc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(f_buf_or_n);
var c__15644__auto___17609 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__15645__auto__ = (function (){var switch__15400__auto__ = (function (state_16246){
var state_val_16247 = (state_16246[(1)]);
if((state_val_16247 === (7))){
var inst_16242 = (state_16246[(2)]);
var state_16246__$1 = state_16246;
var statearr_16248_17611 = state_16246__$1;
(statearr_16248_17611[(2)] = inst_16242);

(statearr_16248_17611[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16247 === (1))){
var state_16246__$1 = state_16246;
var statearr_16249_17613 = state_16246__$1;
(statearr_16249_17613[(2)] = null);

(statearr_16249_17613[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16247 === (4))){
var inst_16223 = (state_16246[(7)]);
var inst_16223__$1 = (state_16246[(2)]);
var inst_16224 = (inst_16223__$1 == null);
var state_16246__$1 = (function (){var statearr_16250 = state_16246;
(statearr_16250[(7)] = inst_16223__$1);

return statearr_16250;
})();
if(cljs.core.truth_(inst_16224)){
var statearr_16251_17614 = state_16246__$1;
(statearr_16251_17614[(1)] = (5));

} else {
var statearr_16252_17615 = state_16246__$1;
(statearr_16252_17615[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16247 === (13))){
var state_16246__$1 = state_16246;
var statearr_16253_17616 = state_16246__$1;
(statearr_16253_17616[(2)] = null);

(statearr_16253_17616[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16247 === (6))){
var inst_16223 = (state_16246[(7)]);
var inst_16229 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_16223) : p.call(null,inst_16223));
var state_16246__$1 = state_16246;
if(cljs.core.truth_(inst_16229)){
var statearr_16254_17617 = state_16246__$1;
(statearr_16254_17617[(1)] = (9));

} else {
var statearr_16255_17618 = state_16246__$1;
(statearr_16255_17618[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16247 === (3))){
var inst_16244 = (state_16246[(2)]);
var state_16246__$1 = state_16246;
return cljs.core.async.impl.ioc_helpers.return_chan(state_16246__$1,inst_16244);
} else {
if((state_val_16247 === (12))){
var state_16246__$1 = state_16246;
var statearr_16256_17619 = state_16246__$1;
(statearr_16256_17619[(2)] = null);

(statearr_16256_17619[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16247 === (2))){
var state_16246__$1 = state_16246;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_16246__$1,(4),ch);
} else {
if((state_val_16247 === (11))){
var inst_16223 = (state_16246[(7)]);
var inst_16233 = (state_16246[(2)]);
var state_16246__$1 = state_16246;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_16246__$1,(8),inst_16233,inst_16223);
} else {
if((state_val_16247 === (9))){
var state_16246__$1 = state_16246;
var statearr_16257_17620 = state_16246__$1;
(statearr_16257_17620[(2)] = tc);

(statearr_16257_17620[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16247 === (5))){
var inst_16226 = cljs.core.async.close_BANG_(tc);
var inst_16227 = cljs.core.async.close_BANG_(fc);
var state_16246__$1 = (function (){var statearr_16258 = state_16246;
(statearr_16258[(8)] = inst_16226);

return statearr_16258;
})();
var statearr_16259_17621 = state_16246__$1;
(statearr_16259_17621[(2)] = inst_16227);

(statearr_16259_17621[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16247 === (14))){
var inst_16240 = (state_16246[(2)]);
var state_16246__$1 = state_16246;
var statearr_16260_17622 = state_16246__$1;
(statearr_16260_17622[(2)] = inst_16240);

(statearr_16260_17622[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16247 === (10))){
var state_16246__$1 = state_16246;
var statearr_16261_17623 = state_16246__$1;
(statearr_16261_17623[(2)] = fc);

(statearr_16261_17623[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16247 === (8))){
var inst_16235 = (state_16246[(2)]);
var state_16246__$1 = state_16246;
if(cljs.core.truth_(inst_16235)){
var statearr_16262_17624 = state_16246__$1;
(statearr_16262_17624[(1)] = (12));

} else {
var statearr_16263_17625 = state_16246__$1;
(statearr_16263_17625[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__15401__auto__ = null;
var cljs$core$async$state_machine__15401__auto____0 = (function (){
var statearr_16264 = [null,null,null,null,null,null,null,null,null];
(statearr_16264[(0)] = cljs$core$async$state_machine__15401__auto__);

(statearr_16264[(1)] = (1));

return statearr_16264;
});
var cljs$core$async$state_machine__15401__auto____1 = (function (state_16246){
while(true){
var ret_value__15402__auto__ = (function (){try{while(true){
var result__15403__auto__ = switch__15400__auto__(state_16246);
if(cljs.core.keyword_identical_QMARK_(result__15403__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__15403__auto__;
}
break;
}
}catch (e16265){var ex__15404__auto__ = e16265;
var statearr_16266_17630 = state_16246;
(statearr_16266_17630[(2)] = ex__15404__auto__);


if(cljs.core.seq((state_16246[(4)]))){
var statearr_16267_17631 = state_16246;
(statearr_16267_17631[(1)] = cljs.core.first((state_16246[(4)])));

} else {
throw ex__15404__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__15402__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__17635 = state_16246;
state_16246 = G__17635;
continue;
} else {
return ret_value__15402__auto__;
}
break;
}
});
cljs$core$async$state_machine__15401__auto__ = function(state_16246){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__15401__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__15401__auto____1.call(this,state_16246);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__15401__auto____0;
cljs$core$async$state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__15401__auto____1;
return cljs$core$async$state_machine__15401__auto__;
})()
})();
var state__15646__auto__ = (function (){var statearr_16268 = f__15645__auto__();
(statearr_16268[(6)] = c__15644__auto___17609);

return statearr_16268;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__15646__auto__);
}));


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
}));

(cljs.core.async.split.cljs$lang$maxFixedArity = 4);

/**
 * f should be a function of 2 arguments. Returns a channel containing
 *   the single result of applying f to init and the first item from the
 *   channel, then applying f to that result and the 2nd item, etc. If
 *   the channel closes without yielding items, returns init and f is not
 *   called. ch must close before reduce produces a result.
 */
cljs.core.async.reduce = (function cljs$core$async$reduce(f,init,ch){
var c__15644__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__15645__auto__ = (function (){var switch__15400__auto__ = (function (state_16290){
var state_val_16291 = (state_16290[(1)]);
if((state_val_16291 === (7))){
var inst_16286 = (state_16290[(2)]);
var state_16290__$1 = state_16290;
var statearr_16292_17639 = state_16290__$1;
(statearr_16292_17639[(2)] = inst_16286);

(statearr_16292_17639[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16291 === (1))){
var inst_16269 = init;
var inst_16270 = inst_16269;
var state_16290__$1 = (function (){var statearr_16293 = state_16290;
(statearr_16293[(7)] = inst_16270);

return statearr_16293;
})();
var statearr_16294_17640 = state_16290__$1;
(statearr_16294_17640[(2)] = null);

(statearr_16294_17640[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16291 === (4))){
var inst_16273 = (state_16290[(8)]);
var inst_16273__$1 = (state_16290[(2)]);
var inst_16274 = (inst_16273__$1 == null);
var state_16290__$1 = (function (){var statearr_16295 = state_16290;
(statearr_16295[(8)] = inst_16273__$1);

return statearr_16295;
})();
if(cljs.core.truth_(inst_16274)){
var statearr_16296_17644 = state_16290__$1;
(statearr_16296_17644[(1)] = (5));

} else {
var statearr_16297_17645 = state_16290__$1;
(statearr_16297_17645[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16291 === (6))){
var inst_16273 = (state_16290[(8)]);
var inst_16270 = (state_16290[(7)]);
var inst_16277 = (state_16290[(9)]);
var inst_16277__$1 = (f.cljs$core$IFn$_invoke$arity$2 ? f.cljs$core$IFn$_invoke$arity$2(inst_16270,inst_16273) : f.call(null,inst_16270,inst_16273));
var inst_16278 = cljs.core.reduced_QMARK_(inst_16277__$1);
var state_16290__$1 = (function (){var statearr_16298 = state_16290;
(statearr_16298[(9)] = inst_16277__$1);

return statearr_16298;
})();
if(inst_16278){
var statearr_16299_17646 = state_16290__$1;
(statearr_16299_17646[(1)] = (8));

} else {
var statearr_16300_17647 = state_16290__$1;
(statearr_16300_17647[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16291 === (3))){
var inst_16288 = (state_16290[(2)]);
var state_16290__$1 = state_16290;
return cljs.core.async.impl.ioc_helpers.return_chan(state_16290__$1,inst_16288);
} else {
if((state_val_16291 === (2))){
var state_16290__$1 = state_16290;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_16290__$1,(4),ch);
} else {
if((state_val_16291 === (9))){
var inst_16277 = (state_16290[(9)]);
var inst_16270 = inst_16277;
var state_16290__$1 = (function (){var statearr_16301 = state_16290;
(statearr_16301[(7)] = inst_16270);

return statearr_16301;
})();
var statearr_16302_17651 = state_16290__$1;
(statearr_16302_17651[(2)] = null);

(statearr_16302_17651[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16291 === (5))){
var inst_16270 = (state_16290[(7)]);
var state_16290__$1 = state_16290;
var statearr_16303_17652 = state_16290__$1;
(statearr_16303_17652[(2)] = inst_16270);

(statearr_16303_17652[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16291 === (10))){
var inst_16284 = (state_16290[(2)]);
var state_16290__$1 = state_16290;
var statearr_16304_17653 = state_16290__$1;
(statearr_16304_17653[(2)] = inst_16284);

(statearr_16304_17653[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16291 === (8))){
var inst_16277 = (state_16290[(9)]);
var inst_16280 = cljs.core.deref(inst_16277);
var state_16290__$1 = state_16290;
var statearr_16305_17654 = state_16290__$1;
(statearr_16305_17654[(2)] = inst_16280);

(statearr_16305_17654[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$reduce_$_state_machine__15401__auto__ = null;
var cljs$core$async$reduce_$_state_machine__15401__auto____0 = (function (){
var statearr_16306 = [null,null,null,null,null,null,null,null,null,null];
(statearr_16306[(0)] = cljs$core$async$reduce_$_state_machine__15401__auto__);

(statearr_16306[(1)] = (1));

return statearr_16306;
});
var cljs$core$async$reduce_$_state_machine__15401__auto____1 = (function (state_16290){
while(true){
var ret_value__15402__auto__ = (function (){try{while(true){
var result__15403__auto__ = switch__15400__auto__(state_16290);
if(cljs.core.keyword_identical_QMARK_(result__15403__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__15403__auto__;
}
break;
}
}catch (e16307){var ex__15404__auto__ = e16307;
var statearr_16308_17662 = state_16290;
(statearr_16308_17662[(2)] = ex__15404__auto__);


if(cljs.core.seq((state_16290[(4)]))){
var statearr_16309_17663 = state_16290;
(statearr_16309_17663[(1)] = cljs.core.first((state_16290[(4)])));

} else {
throw ex__15404__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__15402__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__17664 = state_16290;
state_16290 = G__17664;
continue;
} else {
return ret_value__15402__auto__;
}
break;
}
});
cljs$core$async$reduce_$_state_machine__15401__auto__ = function(state_16290){
switch(arguments.length){
case 0:
return cljs$core$async$reduce_$_state_machine__15401__auto____0.call(this);
case 1:
return cljs$core$async$reduce_$_state_machine__15401__auto____1.call(this,state_16290);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$reduce_$_state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$reduce_$_state_machine__15401__auto____0;
cljs$core$async$reduce_$_state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$reduce_$_state_machine__15401__auto____1;
return cljs$core$async$reduce_$_state_machine__15401__auto__;
})()
})();
var state__15646__auto__ = (function (){var statearr_16310 = f__15645__auto__();
(statearr_16310[(6)] = c__15644__auto__);

return statearr_16310;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__15646__auto__);
}));

return c__15644__auto__;
});
/**
 * async/reduces a channel with a transformation (xform f).
 *   Returns a channel containing the result.  ch must close before
 *   transduce produces a result.
 */
cljs.core.async.transduce = (function cljs$core$async$transduce(xform,f,init,ch){
var f__$1 = (xform.cljs$core$IFn$_invoke$arity$1 ? xform.cljs$core$IFn$_invoke$arity$1(f) : xform.call(null,f));
var c__15644__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__15645__auto__ = (function (){var switch__15400__auto__ = (function (state_16316){
var state_val_16317 = (state_16316[(1)]);
if((state_val_16317 === (1))){
var inst_16311 = cljs.core.async.reduce(f__$1,init,ch);
var state_16316__$1 = state_16316;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_16316__$1,(2),inst_16311);
} else {
if((state_val_16317 === (2))){
var inst_16313 = (state_16316[(2)]);
var inst_16314 = (f__$1.cljs$core$IFn$_invoke$arity$1 ? f__$1.cljs$core$IFn$_invoke$arity$1(inst_16313) : f__$1.call(null,inst_16313));
var state_16316__$1 = state_16316;
return cljs.core.async.impl.ioc_helpers.return_chan(state_16316__$1,inst_16314);
} else {
return null;
}
}
});
return (function() {
var cljs$core$async$transduce_$_state_machine__15401__auto__ = null;
var cljs$core$async$transduce_$_state_machine__15401__auto____0 = (function (){
var statearr_16318 = [null,null,null,null,null,null,null];
(statearr_16318[(0)] = cljs$core$async$transduce_$_state_machine__15401__auto__);

(statearr_16318[(1)] = (1));

return statearr_16318;
});
var cljs$core$async$transduce_$_state_machine__15401__auto____1 = (function (state_16316){
while(true){
var ret_value__15402__auto__ = (function (){try{while(true){
var result__15403__auto__ = switch__15400__auto__(state_16316);
if(cljs.core.keyword_identical_QMARK_(result__15403__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__15403__auto__;
}
break;
}
}catch (e16319){var ex__15404__auto__ = e16319;
var statearr_16320_17668 = state_16316;
(statearr_16320_17668[(2)] = ex__15404__auto__);


if(cljs.core.seq((state_16316[(4)]))){
var statearr_16321_17672 = state_16316;
(statearr_16321_17672[(1)] = cljs.core.first((state_16316[(4)])));

} else {
throw ex__15404__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__15402__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__17673 = state_16316;
state_16316 = G__17673;
continue;
} else {
return ret_value__15402__auto__;
}
break;
}
});
cljs$core$async$transduce_$_state_machine__15401__auto__ = function(state_16316){
switch(arguments.length){
case 0:
return cljs$core$async$transduce_$_state_machine__15401__auto____0.call(this);
case 1:
return cljs$core$async$transduce_$_state_machine__15401__auto____1.call(this,state_16316);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$transduce_$_state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$transduce_$_state_machine__15401__auto____0;
cljs$core$async$transduce_$_state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$transduce_$_state_machine__15401__auto____1;
return cljs$core$async$transduce_$_state_machine__15401__auto__;
})()
})();
var state__15646__auto__ = (function (){var statearr_16322 = f__15645__auto__();
(statearr_16322[(6)] = c__15644__auto__);

return statearr_16322;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__15646__auto__);
}));

return c__15644__auto__;
});
/**
 * Puts the contents of coll into the supplied channel.
 * 
 *   By default the channel will be closed after the items are copied,
 *   but can be determined by the close? parameter.
 * 
 *   Returns a channel which will close after the items are copied.
 */
cljs.core.async.onto_chan_BANG_ = (function cljs$core$async$onto_chan_BANG_(var_args){
var G__16324 = arguments.length;
switch (G__16324) {
case 2:
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
}));

(cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
var c__15644__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__15645__auto__ = (function (){var switch__15400__auto__ = (function (state_16349){
var state_val_16350 = (state_16349[(1)]);
if((state_val_16350 === (7))){
var inst_16331 = (state_16349[(2)]);
var state_16349__$1 = state_16349;
var statearr_16351_17678 = state_16349__$1;
(statearr_16351_17678[(2)] = inst_16331);

(statearr_16351_17678[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16350 === (1))){
var inst_16325 = cljs.core.seq(coll);
var inst_16326 = inst_16325;
var state_16349__$1 = (function (){var statearr_16352 = state_16349;
(statearr_16352[(7)] = inst_16326);

return statearr_16352;
})();
var statearr_16353_17679 = state_16349__$1;
(statearr_16353_17679[(2)] = null);

(statearr_16353_17679[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16350 === (4))){
var inst_16326 = (state_16349[(7)]);
var inst_16329 = cljs.core.first(inst_16326);
var state_16349__$1 = state_16349;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_16349__$1,(7),ch,inst_16329);
} else {
if((state_val_16350 === (13))){
var inst_16343 = (state_16349[(2)]);
var state_16349__$1 = state_16349;
var statearr_16354_17680 = state_16349__$1;
(statearr_16354_17680[(2)] = inst_16343);

(statearr_16354_17680[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16350 === (6))){
var inst_16334 = (state_16349[(2)]);
var state_16349__$1 = state_16349;
if(cljs.core.truth_(inst_16334)){
var statearr_16355_17681 = state_16349__$1;
(statearr_16355_17681[(1)] = (8));

} else {
var statearr_16356_17683 = state_16349__$1;
(statearr_16356_17683[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16350 === (3))){
var inst_16347 = (state_16349[(2)]);
var state_16349__$1 = state_16349;
return cljs.core.async.impl.ioc_helpers.return_chan(state_16349__$1,inst_16347);
} else {
if((state_val_16350 === (12))){
var state_16349__$1 = state_16349;
var statearr_16357_17684 = state_16349__$1;
(statearr_16357_17684[(2)] = null);

(statearr_16357_17684[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16350 === (2))){
var inst_16326 = (state_16349[(7)]);
var state_16349__$1 = state_16349;
if(cljs.core.truth_(inst_16326)){
var statearr_16358_17685 = state_16349__$1;
(statearr_16358_17685[(1)] = (4));

} else {
var statearr_16359_17686 = state_16349__$1;
(statearr_16359_17686[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16350 === (11))){
var inst_16340 = cljs.core.async.close_BANG_(ch);
var state_16349__$1 = state_16349;
var statearr_16360_17687 = state_16349__$1;
(statearr_16360_17687[(2)] = inst_16340);

(statearr_16360_17687[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16350 === (9))){
var state_16349__$1 = state_16349;
if(cljs.core.truth_(close_QMARK_)){
var statearr_16361_17688 = state_16349__$1;
(statearr_16361_17688[(1)] = (11));

} else {
var statearr_16362_17689 = state_16349__$1;
(statearr_16362_17689[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16350 === (5))){
var inst_16326 = (state_16349[(7)]);
var state_16349__$1 = state_16349;
var statearr_16363_17690 = state_16349__$1;
(statearr_16363_17690[(2)] = inst_16326);

(statearr_16363_17690[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16350 === (10))){
var inst_16345 = (state_16349[(2)]);
var state_16349__$1 = state_16349;
var statearr_16364_17691 = state_16349__$1;
(statearr_16364_17691[(2)] = inst_16345);

(statearr_16364_17691[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16350 === (8))){
var inst_16326 = (state_16349[(7)]);
var inst_16336 = cljs.core.next(inst_16326);
var inst_16326__$1 = inst_16336;
var state_16349__$1 = (function (){var statearr_16365 = state_16349;
(statearr_16365[(7)] = inst_16326__$1);

return statearr_16365;
})();
var statearr_16366_17692 = state_16349__$1;
(statearr_16366_17692[(2)] = null);

(statearr_16366_17692[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__15401__auto__ = null;
var cljs$core$async$state_machine__15401__auto____0 = (function (){
var statearr_16367 = [null,null,null,null,null,null,null,null];
(statearr_16367[(0)] = cljs$core$async$state_machine__15401__auto__);

(statearr_16367[(1)] = (1));

return statearr_16367;
});
var cljs$core$async$state_machine__15401__auto____1 = (function (state_16349){
while(true){
var ret_value__15402__auto__ = (function (){try{while(true){
var result__15403__auto__ = switch__15400__auto__(state_16349);
if(cljs.core.keyword_identical_QMARK_(result__15403__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__15403__auto__;
}
break;
}
}catch (e16368){var ex__15404__auto__ = e16368;
var statearr_16369_17693 = state_16349;
(statearr_16369_17693[(2)] = ex__15404__auto__);


if(cljs.core.seq((state_16349[(4)]))){
var statearr_16370_17694 = state_16349;
(statearr_16370_17694[(1)] = cljs.core.first((state_16349[(4)])));

} else {
throw ex__15404__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__15402__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__17695 = state_16349;
state_16349 = G__17695;
continue;
} else {
return ret_value__15402__auto__;
}
break;
}
});
cljs$core$async$state_machine__15401__auto__ = function(state_16349){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__15401__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__15401__auto____1.call(this,state_16349);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__15401__auto____0;
cljs$core$async$state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__15401__auto____1;
return cljs$core$async$state_machine__15401__auto__;
})()
})();
var state__15646__auto__ = (function (){var statearr_16371 = f__15645__auto__();
(statearr_16371[(6)] = c__15644__auto__);

return statearr_16371;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__15646__auto__);
}));

return c__15644__auto__;
}));

(cljs.core.async.onto_chan_BANG_.cljs$lang$maxFixedArity = 3);

/**
 * Creates and returns a channel which contains the contents of coll,
 *   closing when exhausted.
 */
cljs.core.async.to_chan_BANG_ = (function cljs$core$async$to_chan_BANG_(coll){
var ch = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.bounded_count((100),coll));
cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2(ch,coll);

return ch;
});
/**
 * Deprecated - use onto-chan!
 */
cljs.core.async.onto_chan = (function cljs$core$async$onto_chan(var_args){
var G__16373 = arguments.length;
switch (G__16373) {
case 2:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
}));

(cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,close_QMARK_);
}));

(cljs.core.async.onto_chan.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - use to-chan!
 */
cljs.core.async.to_chan = (function cljs$core$async$to_chan(coll){
return cljs.core.async.to_chan_BANG_(coll);
});

/**
 * @interface
 */
cljs.core.async.Mux = function(){};

var cljs$core$async$Mux$muxch_STAR_$dyn_17697 = (function (_){
var x__5393__auto__ = (((_ == null))?null:_);
var m__5394__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__5393__auto__)]);
if((!((m__5394__auto__ == null)))){
return (m__5394__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5394__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__5394__auto__.call(null,_));
} else {
var m__5392__auto__ = (cljs.core.async.muxch_STAR_["_"]);
if((!((m__5392__auto__ == null)))){
return (m__5392__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5392__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__5392__auto__.call(null,_));
} else {
throw cljs.core.missing_protocol("Mux.muxch*",_);
}
}
});
cljs.core.async.muxch_STAR_ = (function cljs$core$async$muxch_STAR_(_){
if((((!((_ == null)))) && ((!((_.cljs$core$async$Mux$muxch_STAR_$arity$1 == null)))))){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
return cljs$core$async$Mux$muxch_STAR_$dyn_17697(_);
}
});


/**
 * @interface
 */
cljs.core.async.Mult = function(){};

var cljs$core$async$Mult$tap_STAR_$dyn_17698 = (function (m,ch,close_QMARK_){
var x__5393__auto__ = (((m == null))?null:m);
var m__5394__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__5393__auto__)]);
if((!((m__5394__auto__ == null)))){
return (m__5394__auto__.cljs$core$IFn$_invoke$arity$3 ? m__5394__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__5394__auto__.call(null,m,ch,close_QMARK_));
} else {
var m__5392__auto__ = (cljs.core.async.tap_STAR_["_"]);
if((!((m__5392__auto__ == null)))){
return (m__5392__auto__.cljs$core$IFn$_invoke$arity$3 ? m__5392__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__5392__auto__.call(null,m,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Mult.tap*",m);
}
}
});
cljs.core.async.tap_STAR_ = (function cljs$core$async$tap_STAR_(m,ch,close_QMARK_){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$tap_STAR_$arity$3 == null)))))){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
return cljs$core$async$Mult$tap_STAR_$dyn_17698(m,ch,close_QMARK_);
}
});

var cljs$core$async$Mult$untap_STAR_$dyn_17699 = (function (m,ch){
var x__5393__auto__ = (((m == null))?null:m);
var m__5394__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__5393__auto__)]);
if((!((m__5394__auto__ == null)))){
return (m__5394__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5394__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__5394__auto__.call(null,m,ch));
} else {
var m__5392__auto__ = (cljs.core.async.untap_STAR_["_"]);
if((!((m__5392__auto__ == null)))){
return (m__5392__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5392__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__5392__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mult.untap*",m);
}
}
});
cljs.core.async.untap_STAR_ = (function cljs$core$async$untap_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mult$untap_STAR_$dyn_17699(m,ch);
}
});

var cljs$core$async$Mult$untap_all_STAR_$dyn_17700 = (function (m){
var x__5393__auto__ = (((m == null))?null:m);
var m__5394__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__5393__auto__)]);
if((!((m__5394__auto__ == null)))){
return (m__5394__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5394__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__5394__auto__.call(null,m));
} else {
var m__5392__auto__ = (cljs.core.async.untap_all_STAR_["_"]);
if((!((m__5392__auto__ == null)))){
return (m__5392__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5392__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__5392__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mult.untap-all*",m);
}
}
});
cljs.core.async.untap_all_STAR_ = (function cljs$core$async$untap_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
return cljs$core$async$Mult$untap_all_STAR_$dyn_17700(m);
}
});

/**
 * Creates and returns a mult(iple) of the supplied channel. Channels
 *   containing copies of the channel can be created with 'tap', and
 *   detached with 'untap'.
 * 
 *   Each item is distributed to all taps in parallel and synchronously,
 *   i.e. each tap must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow taps from holding up the mult.
 * 
 *   Items received when there are no taps get dropped.
 * 
 *   If a tap puts to a closed channel, it will be removed from the mult.
 */
cljs.core.async.mult = (function cljs$core$async$mult(ch){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async16374 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Mult}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async16374 = (function (ch,cs,meta16375){
this.ch = ch;
this.cs = cs;
this.meta16375 = meta16375;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async16374.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_16376,meta16375__$1){
var self__ = this;
var _16376__$1 = this;
return (new cljs.core.async.t_cljs$core$async16374(self__.ch,self__.cs,meta16375__$1));
}));

(cljs.core.async.t_cljs$core$async16374.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_16376){
var self__ = this;
var _16376__$1 = this;
return self__.meta16375;
}));

(cljs.core.async.t_cljs$core$async16374.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async16374.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
}));

(cljs.core.async.t_cljs$core$async16374.prototype.cljs$core$async$Mult$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async16374.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
}));

(cljs.core.async.t_cljs$core$async16374.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch__$1);

return null;
}));

(cljs.core.async.t_cljs$core$async16374.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
}));

(cljs.core.async.t_cljs$core$async16374.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"meta16375","meta16375",-302232739,null)], null);
}));

(cljs.core.async.t_cljs$core$async16374.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async16374.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async16374");

(cljs.core.async.t_cljs$core$async16374.cljs$lang$ctorPrWriter = (function (this__5330__auto__,writer__5331__auto__,opt__5332__auto__){
return cljs.core._write(writer__5331__auto__,"cljs.core.async/t_cljs$core$async16374");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async16374.
 */
cljs.core.async.__GT_t_cljs$core$async16374 = (function cljs$core$async$mult_$___GT_t_cljs$core$async16374(ch__$1,cs__$1,meta16375){
return (new cljs.core.async.t_cljs$core$async16374(ch__$1,cs__$1,meta16375));
});

}

return (new cljs.core.async.t_cljs$core$async16374(ch,cs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = (function (_){
if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,true);
} else {
return null;
}
});
var c__15644__auto___17707 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__15645__auto__ = (function (){var switch__15400__auto__ = (function (state_16509){
var state_val_16510 = (state_16509[(1)]);
if((state_val_16510 === (7))){
var inst_16505 = (state_16509[(2)]);
var state_16509__$1 = state_16509;
var statearr_16511_17708 = state_16509__$1;
(statearr_16511_17708[(2)] = inst_16505);

(statearr_16511_17708[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (20))){
var inst_16410 = (state_16509[(7)]);
var inst_16422 = cljs.core.first(inst_16410);
var inst_16423 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_16422,(0),null);
var inst_16424 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_16422,(1),null);
var state_16509__$1 = (function (){var statearr_16512 = state_16509;
(statearr_16512[(8)] = inst_16423);

return statearr_16512;
})();
if(cljs.core.truth_(inst_16424)){
var statearr_16513_17710 = state_16509__$1;
(statearr_16513_17710[(1)] = (22));

} else {
var statearr_16514_17711 = state_16509__$1;
(statearr_16514_17711[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (27))){
var inst_16452 = (state_16509[(9)]);
var inst_16454 = (state_16509[(10)]);
var inst_16459 = (state_16509[(11)]);
var inst_16379 = (state_16509[(12)]);
var inst_16459__$1 = cljs.core._nth(inst_16452,inst_16454);
var inst_16460 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_16459__$1,inst_16379,done);
var state_16509__$1 = (function (){var statearr_16515 = state_16509;
(statearr_16515[(11)] = inst_16459__$1);

return statearr_16515;
})();
if(cljs.core.truth_(inst_16460)){
var statearr_16516_17713 = state_16509__$1;
(statearr_16516_17713[(1)] = (30));

} else {
var statearr_16517_17714 = state_16509__$1;
(statearr_16517_17714[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (1))){
var state_16509__$1 = state_16509;
var statearr_16518_17715 = state_16509__$1;
(statearr_16518_17715[(2)] = null);

(statearr_16518_17715[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (24))){
var inst_16410 = (state_16509[(7)]);
var inst_16429 = (state_16509[(2)]);
var inst_16430 = cljs.core.next(inst_16410);
var inst_16388 = inst_16430;
var inst_16389 = null;
var inst_16390 = (0);
var inst_16391 = (0);
var state_16509__$1 = (function (){var statearr_16519 = state_16509;
(statearr_16519[(13)] = inst_16389);

(statearr_16519[(14)] = inst_16391);

(statearr_16519[(15)] = inst_16390);

(statearr_16519[(16)] = inst_16429);

(statearr_16519[(17)] = inst_16388);

return statearr_16519;
})();
var statearr_16520_17716 = state_16509__$1;
(statearr_16520_17716[(2)] = null);

(statearr_16520_17716[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (39))){
var state_16509__$1 = state_16509;
var statearr_16524_17717 = state_16509__$1;
(statearr_16524_17717[(2)] = null);

(statearr_16524_17717[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (4))){
var inst_16379 = (state_16509[(12)]);
var inst_16379__$1 = (state_16509[(2)]);
var inst_16380 = (inst_16379__$1 == null);
var state_16509__$1 = (function (){var statearr_16525 = state_16509;
(statearr_16525[(12)] = inst_16379__$1);

return statearr_16525;
})();
if(cljs.core.truth_(inst_16380)){
var statearr_16526_17718 = state_16509__$1;
(statearr_16526_17718[(1)] = (5));

} else {
var statearr_16527_17719 = state_16509__$1;
(statearr_16527_17719[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (15))){
var inst_16389 = (state_16509[(13)]);
var inst_16391 = (state_16509[(14)]);
var inst_16390 = (state_16509[(15)]);
var inst_16388 = (state_16509[(17)]);
var inst_16406 = (state_16509[(2)]);
var inst_16407 = (inst_16391 + (1));
var tmp16521 = inst_16389;
var tmp16522 = inst_16390;
var tmp16523 = inst_16388;
var inst_16388__$1 = tmp16523;
var inst_16389__$1 = tmp16521;
var inst_16390__$1 = tmp16522;
var inst_16391__$1 = inst_16407;
var state_16509__$1 = (function (){var statearr_16528 = state_16509;
(statearr_16528[(13)] = inst_16389__$1);

(statearr_16528[(18)] = inst_16406);

(statearr_16528[(14)] = inst_16391__$1);

(statearr_16528[(15)] = inst_16390__$1);

(statearr_16528[(17)] = inst_16388__$1);

return statearr_16528;
})();
var statearr_16529_17720 = state_16509__$1;
(statearr_16529_17720[(2)] = null);

(statearr_16529_17720[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (21))){
var inst_16433 = (state_16509[(2)]);
var state_16509__$1 = state_16509;
var statearr_16533_17722 = state_16509__$1;
(statearr_16533_17722[(2)] = inst_16433);

(statearr_16533_17722[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (31))){
var inst_16459 = (state_16509[(11)]);
var inst_16463 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_16459);
var state_16509__$1 = state_16509;
var statearr_16534_17726 = state_16509__$1;
(statearr_16534_17726[(2)] = inst_16463);

(statearr_16534_17726[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (32))){
var inst_16452 = (state_16509[(9)]);
var inst_16453 = (state_16509[(19)]);
var inst_16451 = (state_16509[(20)]);
var inst_16454 = (state_16509[(10)]);
var inst_16465 = (state_16509[(2)]);
var inst_16466 = (inst_16454 + (1));
var tmp16530 = inst_16452;
var tmp16531 = inst_16453;
var tmp16532 = inst_16451;
var inst_16451__$1 = tmp16532;
var inst_16452__$1 = tmp16530;
var inst_16453__$1 = tmp16531;
var inst_16454__$1 = inst_16466;
var state_16509__$1 = (function (){var statearr_16535 = state_16509;
(statearr_16535[(9)] = inst_16452__$1);

(statearr_16535[(19)] = inst_16453__$1);

(statearr_16535[(20)] = inst_16451__$1);

(statearr_16535[(10)] = inst_16454__$1);

(statearr_16535[(21)] = inst_16465);

return statearr_16535;
})();
var statearr_16536_17729 = state_16509__$1;
(statearr_16536_17729[(2)] = null);

(statearr_16536_17729[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (40))){
var inst_16478 = (state_16509[(22)]);
var inst_16482 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_16478);
var state_16509__$1 = state_16509;
var statearr_16537_17732 = state_16509__$1;
(statearr_16537_17732[(2)] = inst_16482);

(statearr_16537_17732[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (33))){
var inst_16469 = (state_16509[(23)]);
var inst_16471 = cljs.core.chunked_seq_QMARK_(inst_16469);
var state_16509__$1 = state_16509;
if(inst_16471){
var statearr_16538_17733 = state_16509__$1;
(statearr_16538_17733[(1)] = (36));

} else {
var statearr_16539_17734 = state_16509__$1;
(statearr_16539_17734[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (13))){
var inst_16400 = (state_16509[(24)]);
var inst_16403 = cljs.core.async.close_BANG_(inst_16400);
var state_16509__$1 = state_16509;
var statearr_16540_17735 = state_16509__$1;
(statearr_16540_17735[(2)] = inst_16403);

(statearr_16540_17735[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (22))){
var inst_16423 = (state_16509[(8)]);
var inst_16426 = cljs.core.async.close_BANG_(inst_16423);
var state_16509__$1 = state_16509;
var statearr_16541_17743 = state_16509__$1;
(statearr_16541_17743[(2)] = inst_16426);

(statearr_16541_17743[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (36))){
var inst_16469 = (state_16509[(23)]);
var inst_16473 = cljs.core.chunk_first(inst_16469);
var inst_16474 = cljs.core.chunk_rest(inst_16469);
var inst_16475 = cljs.core.count(inst_16473);
var inst_16451 = inst_16474;
var inst_16452 = inst_16473;
var inst_16453 = inst_16475;
var inst_16454 = (0);
var state_16509__$1 = (function (){var statearr_16542 = state_16509;
(statearr_16542[(9)] = inst_16452);

(statearr_16542[(19)] = inst_16453);

(statearr_16542[(20)] = inst_16451);

(statearr_16542[(10)] = inst_16454);

return statearr_16542;
})();
var statearr_16543_17750 = state_16509__$1;
(statearr_16543_17750[(2)] = null);

(statearr_16543_17750[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (41))){
var inst_16469 = (state_16509[(23)]);
var inst_16484 = (state_16509[(2)]);
var inst_16485 = cljs.core.next(inst_16469);
var inst_16451 = inst_16485;
var inst_16452 = null;
var inst_16453 = (0);
var inst_16454 = (0);
var state_16509__$1 = (function (){var statearr_16544 = state_16509;
(statearr_16544[(9)] = inst_16452);

(statearr_16544[(19)] = inst_16453);

(statearr_16544[(20)] = inst_16451);

(statearr_16544[(10)] = inst_16454);

(statearr_16544[(25)] = inst_16484);

return statearr_16544;
})();
var statearr_16545_17751 = state_16509__$1;
(statearr_16545_17751[(2)] = null);

(statearr_16545_17751[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (43))){
var state_16509__$1 = state_16509;
var statearr_16546_17752 = state_16509__$1;
(statearr_16546_17752[(2)] = null);

(statearr_16546_17752[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (29))){
var inst_16493 = (state_16509[(2)]);
var state_16509__$1 = state_16509;
var statearr_16547_17753 = state_16509__$1;
(statearr_16547_17753[(2)] = inst_16493);

(statearr_16547_17753[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (44))){
var inst_16502 = (state_16509[(2)]);
var state_16509__$1 = (function (){var statearr_16548 = state_16509;
(statearr_16548[(26)] = inst_16502);

return statearr_16548;
})();
var statearr_16549_17754 = state_16509__$1;
(statearr_16549_17754[(2)] = null);

(statearr_16549_17754[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (6))){
var inst_16443 = (state_16509[(27)]);
var inst_16442 = cljs.core.deref(cs);
var inst_16443__$1 = cljs.core.keys(inst_16442);
var inst_16444 = cljs.core.count(inst_16443__$1);
var inst_16445 = cljs.core.reset_BANG_(dctr,inst_16444);
var inst_16450 = cljs.core.seq(inst_16443__$1);
var inst_16451 = inst_16450;
var inst_16452 = null;
var inst_16453 = (0);
var inst_16454 = (0);
var state_16509__$1 = (function (){var statearr_16550 = state_16509;
(statearr_16550[(9)] = inst_16452);

(statearr_16550[(27)] = inst_16443__$1);

(statearr_16550[(19)] = inst_16453);

(statearr_16550[(20)] = inst_16451);

(statearr_16550[(10)] = inst_16454);

(statearr_16550[(28)] = inst_16445);

return statearr_16550;
})();
var statearr_16551_17755 = state_16509__$1;
(statearr_16551_17755[(2)] = null);

(statearr_16551_17755[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (28))){
var inst_16469 = (state_16509[(23)]);
var inst_16451 = (state_16509[(20)]);
var inst_16469__$1 = cljs.core.seq(inst_16451);
var state_16509__$1 = (function (){var statearr_16552 = state_16509;
(statearr_16552[(23)] = inst_16469__$1);

return statearr_16552;
})();
if(inst_16469__$1){
var statearr_16553_17756 = state_16509__$1;
(statearr_16553_17756[(1)] = (33));

} else {
var statearr_16554_17757 = state_16509__$1;
(statearr_16554_17757[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (25))){
var inst_16453 = (state_16509[(19)]);
var inst_16454 = (state_16509[(10)]);
var inst_16456 = (inst_16454 < inst_16453);
var inst_16457 = inst_16456;
var state_16509__$1 = state_16509;
if(cljs.core.truth_(inst_16457)){
var statearr_16555_17758 = state_16509__$1;
(statearr_16555_17758[(1)] = (27));

} else {
var statearr_16556_17759 = state_16509__$1;
(statearr_16556_17759[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (34))){
var state_16509__$1 = state_16509;
var statearr_16557_17760 = state_16509__$1;
(statearr_16557_17760[(2)] = null);

(statearr_16557_17760[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (17))){
var state_16509__$1 = state_16509;
var statearr_16558_17761 = state_16509__$1;
(statearr_16558_17761[(2)] = null);

(statearr_16558_17761[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (3))){
var inst_16507 = (state_16509[(2)]);
var state_16509__$1 = state_16509;
return cljs.core.async.impl.ioc_helpers.return_chan(state_16509__$1,inst_16507);
} else {
if((state_val_16510 === (12))){
var inst_16438 = (state_16509[(2)]);
var state_16509__$1 = state_16509;
var statearr_16559_17762 = state_16509__$1;
(statearr_16559_17762[(2)] = inst_16438);

(statearr_16559_17762[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (2))){
var state_16509__$1 = state_16509;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_16509__$1,(4),ch);
} else {
if((state_val_16510 === (23))){
var state_16509__$1 = state_16509;
var statearr_16560_17763 = state_16509__$1;
(statearr_16560_17763[(2)] = null);

(statearr_16560_17763[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (35))){
var inst_16491 = (state_16509[(2)]);
var state_16509__$1 = state_16509;
var statearr_16561_17764 = state_16509__$1;
(statearr_16561_17764[(2)] = inst_16491);

(statearr_16561_17764[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (19))){
var inst_16410 = (state_16509[(7)]);
var inst_16414 = cljs.core.chunk_first(inst_16410);
var inst_16415 = cljs.core.chunk_rest(inst_16410);
var inst_16416 = cljs.core.count(inst_16414);
var inst_16388 = inst_16415;
var inst_16389 = inst_16414;
var inst_16390 = inst_16416;
var inst_16391 = (0);
var state_16509__$1 = (function (){var statearr_16562 = state_16509;
(statearr_16562[(13)] = inst_16389);

(statearr_16562[(14)] = inst_16391);

(statearr_16562[(15)] = inst_16390);

(statearr_16562[(17)] = inst_16388);

return statearr_16562;
})();
var statearr_16563_17765 = state_16509__$1;
(statearr_16563_17765[(2)] = null);

(statearr_16563_17765[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (11))){
var inst_16410 = (state_16509[(7)]);
var inst_16388 = (state_16509[(17)]);
var inst_16410__$1 = cljs.core.seq(inst_16388);
var state_16509__$1 = (function (){var statearr_16564 = state_16509;
(statearr_16564[(7)] = inst_16410__$1);

return statearr_16564;
})();
if(inst_16410__$1){
var statearr_16565_17766 = state_16509__$1;
(statearr_16565_17766[(1)] = (16));

} else {
var statearr_16566_17767 = state_16509__$1;
(statearr_16566_17767[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (9))){
var inst_16440 = (state_16509[(2)]);
var state_16509__$1 = state_16509;
var statearr_16567_17768 = state_16509__$1;
(statearr_16567_17768[(2)] = inst_16440);

(statearr_16567_17768[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (5))){
var inst_16386 = cljs.core.deref(cs);
var inst_16387 = cljs.core.seq(inst_16386);
var inst_16388 = inst_16387;
var inst_16389 = null;
var inst_16390 = (0);
var inst_16391 = (0);
var state_16509__$1 = (function (){var statearr_16568 = state_16509;
(statearr_16568[(13)] = inst_16389);

(statearr_16568[(14)] = inst_16391);

(statearr_16568[(15)] = inst_16390);

(statearr_16568[(17)] = inst_16388);

return statearr_16568;
})();
var statearr_16569_17769 = state_16509__$1;
(statearr_16569_17769[(2)] = null);

(statearr_16569_17769[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (14))){
var state_16509__$1 = state_16509;
var statearr_16570_17770 = state_16509__$1;
(statearr_16570_17770[(2)] = null);

(statearr_16570_17770[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (45))){
var inst_16499 = (state_16509[(2)]);
var state_16509__$1 = state_16509;
var statearr_16571_17771 = state_16509__$1;
(statearr_16571_17771[(2)] = inst_16499);

(statearr_16571_17771[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (26))){
var inst_16443 = (state_16509[(27)]);
var inst_16495 = (state_16509[(2)]);
var inst_16496 = cljs.core.seq(inst_16443);
var state_16509__$1 = (function (){var statearr_16572 = state_16509;
(statearr_16572[(29)] = inst_16495);

return statearr_16572;
})();
if(inst_16496){
var statearr_16573_17778 = state_16509__$1;
(statearr_16573_17778[(1)] = (42));

} else {
var statearr_16574_17779 = state_16509__$1;
(statearr_16574_17779[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (16))){
var inst_16410 = (state_16509[(7)]);
var inst_16412 = cljs.core.chunked_seq_QMARK_(inst_16410);
var state_16509__$1 = state_16509;
if(inst_16412){
var statearr_16575_17780 = state_16509__$1;
(statearr_16575_17780[(1)] = (19));

} else {
var statearr_16576_17781 = state_16509__$1;
(statearr_16576_17781[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (38))){
var inst_16488 = (state_16509[(2)]);
var state_16509__$1 = state_16509;
var statearr_16577_17782 = state_16509__$1;
(statearr_16577_17782[(2)] = inst_16488);

(statearr_16577_17782[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (30))){
var state_16509__$1 = state_16509;
var statearr_16578_17783 = state_16509__$1;
(statearr_16578_17783[(2)] = null);

(statearr_16578_17783[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (10))){
var inst_16389 = (state_16509[(13)]);
var inst_16391 = (state_16509[(14)]);
var inst_16399 = cljs.core._nth(inst_16389,inst_16391);
var inst_16400 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_16399,(0),null);
var inst_16401 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_16399,(1),null);
var state_16509__$1 = (function (){var statearr_16579 = state_16509;
(statearr_16579[(24)] = inst_16400);

return statearr_16579;
})();
if(cljs.core.truth_(inst_16401)){
var statearr_16580_17784 = state_16509__$1;
(statearr_16580_17784[(1)] = (13));

} else {
var statearr_16581_17785 = state_16509__$1;
(statearr_16581_17785[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (18))){
var inst_16436 = (state_16509[(2)]);
var state_16509__$1 = state_16509;
var statearr_16582_17786 = state_16509__$1;
(statearr_16582_17786[(2)] = inst_16436);

(statearr_16582_17786[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (42))){
var state_16509__$1 = state_16509;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_16509__$1,(45),dchan);
} else {
if((state_val_16510 === (37))){
var inst_16469 = (state_16509[(23)]);
var inst_16478 = (state_16509[(22)]);
var inst_16379 = (state_16509[(12)]);
var inst_16478__$1 = cljs.core.first(inst_16469);
var inst_16479 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_16478__$1,inst_16379,done);
var state_16509__$1 = (function (){var statearr_16583 = state_16509;
(statearr_16583[(22)] = inst_16478__$1);

return statearr_16583;
})();
if(cljs.core.truth_(inst_16479)){
var statearr_16584_17787 = state_16509__$1;
(statearr_16584_17787[(1)] = (39));

} else {
var statearr_16585_17788 = state_16509__$1;
(statearr_16585_17788[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16510 === (8))){
var inst_16391 = (state_16509[(14)]);
var inst_16390 = (state_16509[(15)]);
var inst_16393 = (inst_16391 < inst_16390);
var inst_16394 = inst_16393;
var state_16509__$1 = state_16509;
if(cljs.core.truth_(inst_16394)){
var statearr_16586_17789 = state_16509__$1;
(statearr_16586_17789[(1)] = (10));

} else {
var statearr_16587_17790 = state_16509__$1;
(statearr_16587_17790[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mult_$_state_machine__15401__auto__ = null;
var cljs$core$async$mult_$_state_machine__15401__auto____0 = (function (){
var statearr_16588 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_16588[(0)] = cljs$core$async$mult_$_state_machine__15401__auto__);

(statearr_16588[(1)] = (1));

return statearr_16588;
});
var cljs$core$async$mult_$_state_machine__15401__auto____1 = (function (state_16509){
while(true){
var ret_value__15402__auto__ = (function (){try{while(true){
var result__15403__auto__ = switch__15400__auto__(state_16509);
if(cljs.core.keyword_identical_QMARK_(result__15403__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__15403__auto__;
}
break;
}
}catch (e16589){var ex__15404__auto__ = e16589;
var statearr_16590_17796 = state_16509;
(statearr_16590_17796[(2)] = ex__15404__auto__);


if(cljs.core.seq((state_16509[(4)]))){
var statearr_16591_17798 = state_16509;
(statearr_16591_17798[(1)] = cljs.core.first((state_16509[(4)])));

} else {
throw ex__15404__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__15402__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__17799 = state_16509;
state_16509 = G__17799;
continue;
} else {
return ret_value__15402__auto__;
}
break;
}
});
cljs$core$async$mult_$_state_machine__15401__auto__ = function(state_16509){
switch(arguments.length){
case 0:
return cljs$core$async$mult_$_state_machine__15401__auto____0.call(this);
case 1:
return cljs$core$async$mult_$_state_machine__15401__auto____1.call(this,state_16509);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mult_$_state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mult_$_state_machine__15401__auto____0;
cljs$core$async$mult_$_state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mult_$_state_machine__15401__auto____1;
return cljs$core$async$mult_$_state_machine__15401__auto__;
})()
})();
var state__15646__auto__ = (function (){var statearr_16592 = f__15645__auto__();
(statearr_16592[(6)] = c__15644__auto___17707);

return statearr_16592;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__15646__auto__);
}));


return m;
});
/**
 * Copies the mult source onto the supplied channel.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.tap = (function cljs$core$async$tap(var_args){
var G__16594 = arguments.length;
switch (G__16594) {
case 2:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2 = (function (mult,ch){
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(mult,ch,true);
}));

(cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_(mult,ch,close_QMARK_);

return ch;
}));

(cljs.core.async.tap.cljs$lang$maxFixedArity = 3);

/**
 * Disconnects a target channel from a mult
 */
cljs.core.async.untap = (function cljs$core$async$untap(mult,ch){
return cljs.core.async.untap_STAR_(mult,ch);
});
/**
 * Disconnects all target channels from a mult
 */
cljs.core.async.untap_all = (function cljs$core$async$untap_all(mult){
return cljs.core.async.untap_all_STAR_(mult);
});

/**
 * @interface
 */
cljs.core.async.Mix = function(){};

var cljs$core$async$Mix$admix_STAR_$dyn_17801 = (function (m,ch){
var x__5393__auto__ = (((m == null))?null:m);
var m__5394__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__5393__auto__)]);
if((!((m__5394__auto__ == null)))){
return (m__5394__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5394__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__5394__auto__.call(null,m,ch));
} else {
var m__5392__auto__ = (cljs.core.async.admix_STAR_["_"]);
if((!((m__5392__auto__ == null)))){
return (m__5392__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5392__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__5392__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.admix*",m);
}
}
});
cljs.core.async.admix_STAR_ = (function cljs$core$async$admix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$admix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mix$admix_STAR_$dyn_17801(m,ch);
}
});

var cljs$core$async$Mix$unmix_STAR_$dyn_17802 = (function (m,ch){
var x__5393__auto__ = (((m == null))?null:m);
var m__5394__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__5393__auto__)]);
if((!((m__5394__auto__ == null)))){
return (m__5394__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5394__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__5394__auto__.call(null,m,ch));
} else {
var m__5392__auto__ = (cljs.core.async.unmix_STAR_["_"]);
if((!((m__5392__auto__ == null)))){
return (m__5392__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5392__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__5392__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.unmix*",m);
}
}
});
cljs.core.async.unmix_STAR_ = (function cljs$core$async$unmix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mix$unmix_STAR_$dyn_17802(m,ch);
}
});

var cljs$core$async$Mix$unmix_all_STAR_$dyn_17803 = (function (m){
var x__5393__auto__ = (((m == null))?null:m);
var m__5394__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__5393__auto__)]);
if((!((m__5394__auto__ == null)))){
return (m__5394__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5394__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__5394__auto__.call(null,m));
} else {
var m__5392__auto__ = (cljs.core.async.unmix_all_STAR_["_"]);
if((!((m__5392__auto__ == null)))){
return (m__5392__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5392__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__5392__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mix.unmix-all*",m);
}
}
});
cljs.core.async.unmix_all_STAR_ = (function cljs$core$async$unmix_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
return cljs$core$async$Mix$unmix_all_STAR_$dyn_17803(m);
}
});

var cljs$core$async$Mix$toggle_STAR_$dyn_17810 = (function (m,state_map){
var x__5393__auto__ = (((m == null))?null:m);
var m__5394__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__5393__auto__)]);
if((!((m__5394__auto__ == null)))){
return (m__5394__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5394__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__5394__auto__.call(null,m,state_map));
} else {
var m__5392__auto__ = (cljs.core.async.toggle_STAR_["_"]);
if((!((m__5392__auto__ == null)))){
return (m__5392__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5392__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__5392__auto__.call(null,m,state_map));
} else {
throw cljs.core.missing_protocol("Mix.toggle*",m);
}
}
});
cljs.core.async.toggle_STAR_ = (function cljs$core$async$toggle_STAR_(m,state_map){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$toggle_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
return cljs$core$async$Mix$toggle_STAR_$dyn_17810(m,state_map);
}
});

var cljs$core$async$Mix$solo_mode_STAR_$dyn_17811 = (function (m,mode){
var x__5393__auto__ = (((m == null))?null:m);
var m__5394__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__5393__auto__)]);
if((!((m__5394__auto__ == null)))){
return (m__5394__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5394__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__5394__auto__.call(null,m,mode));
} else {
var m__5392__auto__ = (cljs.core.async.solo_mode_STAR_["_"]);
if((!((m__5392__auto__ == null)))){
return (m__5392__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5392__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__5392__auto__.call(null,m,mode));
} else {
throw cljs.core.missing_protocol("Mix.solo-mode*",m);
}
}
});
cljs.core.async.solo_mode_STAR_ = (function cljs$core$async$solo_mode_STAR_(m,mode){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$solo_mode_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
return cljs$core$async$Mix$solo_mode_STAR_$dyn_17811(m,mode);
}
});

cljs.core.async.ioc_alts_BANG_ = (function cljs$core$async$ioc_alts_BANG_(var_args){
var args__5775__auto__ = [];
var len__5769__auto___17812 = arguments.length;
var i__5770__auto___17813 = (0);
while(true){
if((i__5770__auto___17813 < len__5769__auto___17812)){
args__5775__auto__.push((arguments[i__5770__auto___17813]));

var G__17814 = (i__5770__auto___17813 + (1));
i__5770__auto___17813 = G__17814;
continue;
} else {
}
break;
}

var argseq__5776__auto__ = ((((3) < args__5775__auto__.length))?(new cljs.core.IndexedSeq(args__5775__auto__.slice((3)),(0),null)):null);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__5776__auto__);
});

(cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (state,cont_block,ports,p__16599){
var map__16600 = p__16599;
var map__16600__$1 = cljs.core.__destructure_map(map__16600);
var opts = map__16600__$1;
var statearr_16601_17815 = state;
(statearr_16601_17815[(1)] = cont_block);


var temp__5804__auto__ = cljs.core.async.do_alts((function (val){
var statearr_16602_17816 = state;
(statearr_16602_17816[(2)] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state);
}),ports,opts);
if(cljs.core.truth_(temp__5804__auto__)){
var cb = temp__5804__auto__;
var statearr_16603_17817 = state;
(statearr_16603_17817[(2)] = cljs.core.deref(cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}));

(cljs.core.async.ioc_alts_BANG_.cljs$lang$maxFixedArity = (3));

/** @this {Function} */
(cljs.core.async.ioc_alts_BANG_.cljs$lang$applyTo = (function (seq16595){
var G__16596 = cljs.core.first(seq16595);
var seq16595__$1 = cljs.core.next(seq16595);
var G__16597 = cljs.core.first(seq16595__$1);
var seq16595__$2 = cljs.core.next(seq16595__$1);
var G__16598 = cljs.core.first(seq16595__$2);
var seq16595__$3 = cljs.core.next(seq16595__$2);
var self__5754__auto__ = this;
return self__5754__auto__.cljs$core$IFn$_invoke$arity$variadic(G__16596,G__16597,G__16598,seq16595__$3);
}));

/**
 * Creates and returns a mix of one or more input channels which will
 *   be put on the supplied out channel. Input sources can be added to
 *   the mix with 'admix', and removed with 'unmix'. A mix supports
 *   soloing, muting and pausing multiple inputs atomically using
 *   'toggle', and can solo using either muting or pausing as determined
 *   by 'solo-mode'.
 * 
 *   Each channel can have zero or more boolean modes set via 'toggle':
 * 
 *   :solo - when true, only this (ond other soloed) channel(s) will appear
 *        in the mix output channel. :mute and :pause states of soloed
 *        channels are ignored. If solo-mode is :mute, non-soloed
 *        channels are muted, if :pause, non-soloed channels are
 *        paused.
 * 
 *   :mute - muted channels will have their contents consumed but not included in the mix
 *   :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
 */
cljs.core.async.mix = (function cljs$core$async$mix(out){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.async.sliding_buffer((1)));
var changed = (function (){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(change,true);
});
var pick = (function (attr,chs){
return cljs.core.reduce_kv((function (ret,c,v){
if(cljs.core.truth_((attr.cljs$core$IFn$_invoke$arity$1 ? attr.cljs$core$IFn$_invoke$arity$1(v) : attr.call(null,v)))){
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,c);
} else {
return ret;
}
}),cljs.core.PersistentHashSet.EMPTY,chs);
});
var calc_state = (function (){
var chs = cljs.core.deref(cs);
var mode = cljs.core.deref(solo_mode);
var solos = pick(new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick(new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick(new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(((((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && ((!(cljs.core.empty_QMARK_(solos))))))?cljs.core.vec(solos):cljs.core.vec(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(pauses,cljs.core.keys(chs)))),change)], null);
});
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async16604 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mix}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async16604 = (function (change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta16605){
this.change = change;
this.solo_mode = solo_mode;
this.pick = pick;
this.cs = cs;
this.calc_state = calc_state;
this.out = out;
this.changed = changed;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.meta16605 = meta16605;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async16604.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_16606,meta16605__$1){
var self__ = this;
var _16606__$1 = this;
return (new cljs.core.async.t_cljs$core$async16604(self__.change,self__.solo_mode,self__.pick,self__.cs,self__.calc_state,self__.out,self__.changed,self__.solo_modes,self__.attrs,meta16605__$1));
}));

(cljs.core.async.t_cljs$core$async16604.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_16606){
var self__ = this;
var _16606__$1 = this;
return self__.meta16605;
}));

(cljs.core.async.t_cljs$core$async16604.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async16604.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
}));

(cljs.core.async.t_cljs$core$async16604.prototype.cljs$core$async$Mix$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async16604.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async16604.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async16604.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async16604.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.partial.cljs$core$IFn$_invoke$arity$2(cljs.core.merge_with,cljs.core.merge),state_map);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async16604.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.solo_modes.cljs$core$IFn$_invoke$arity$1 ? self__.solo_modes.cljs$core$IFn$_invoke$arity$1(mode) : self__.solo_modes.call(null,mode)))){
} else {
throw (new Error(["Assert failed: ",["mode must be one of: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(self__.solo_modes)].join(''),"\n","(solo-modes mode)"].join('')));
}

cljs.core.reset_BANG_(self__.solo_mode,mode);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async16604.getBasis = (function (){
return new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"change","change",477485025,null),new cljs.core.Symbol(null,"solo-mode","solo-mode",2031788074,null),new cljs.core.Symbol(null,"pick","pick",1300068175,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"calc-state","calc-state",-349968968,null),new cljs.core.Symbol(null,"out","out",729986010,null),new cljs.core.Symbol(null,"changed","changed",-2083710852,null),new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"attrs","attrs",-450137186,null),new cljs.core.Symbol(null,"meta16605","meta16605",1199269167,null)], null);
}));

(cljs.core.async.t_cljs$core$async16604.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async16604.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async16604");

(cljs.core.async.t_cljs$core$async16604.cljs$lang$ctorPrWriter = (function (this__5330__auto__,writer__5331__auto__,opt__5332__auto__){
return cljs.core._write(writer__5331__auto__,"cljs.core.async/t_cljs$core$async16604");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async16604.
 */
cljs.core.async.__GT_t_cljs$core$async16604 = (function cljs$core$async$mix_$___GT_t_cljs$core$async16604(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta16605){
return (new cljs.core.async.t_cljs$core$async16604(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta16605));
});

}

return (new cljs.core.async.t_cljs$core$async16604(change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__15644__auto___17823 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__15645__auto__ = (function (){var switch__15400__auto__ = (function (state_16674){
var state_val_16675 = (state_16674[(1)]);
if((state_val_16675 === (7))){
var inst_16634 = (state_16674[(2)]);
var state_16674__$1 = state_16674;
if(cljs.core.truth_(inst_16634)){
var statearr_16676_17824 = state_16674__$1;
(statearr_16676_17824[(1)] = (8));

} else {
var statearr_16677_17825 = state_16674__$1;
(statearr_16677_17825[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16675 === (20))){
var inst_16627 = (state_16674[(7)]);
var state_16674__$1 = state_16674;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_16674__$1,(23),out,inst_16627);
} else {
if((state_val_16675 === (1))){
var inst_16610 = calc_state();
var inst_16611 = cljs.core.__destructure_map(inst_16610);
var inst_16612 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_16611,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_16613 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_16611,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_16614 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_16611,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_16615 = inst_16610;
var state_16674__$1 = (function (){var statearr_16678 = state_16674;
(statearr_16678[(8)] = inst_16613);

(statearr_16678[(9)] = inst_16614);

(statearr_16678[(10)] = inst_16612);

(statearr_16678[(11)] = inst_16615);

return statearr_16678;
})();
var statearr_16679_17826 = state_16674__$1;
(statearr_16679_17826[(2)] = null);

(statearr_16679_17826[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16675 === (24))){
var inst_16618 = (state_16674[(12)]);
var inst_16615 = inst_16618;
var state_16674__$1 = (function (){var statearr_16680 = state_16674;
(statearr_16680[(11)] = inst_16615);

return statearr_16680;
})();
var statearr_16681_17827 = state_16674__$1;
(statearr_16681_17827[(2)] = null);

(statearr_16681_17827[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16675 === (4))){
var inst_16629 = (state_16674[(13)]);
var inst_16627 = (state_16674[(7)]);
var inst_16626 = (state_16674[(2)]);
var inst_16627__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_16626,(0),null);
var inst_16628 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_16626,(1),null);
var inst_16629__$1 = (inst_16627__$1 == null);
var state_16674__$1 = (function (){var statearr_16682 = state_16674;
(statearr_16682[(14)] = inst_16628);

(statearr_16682[(13)] = inst_16629__$1);

(statearr_16682[(7)] = inst_16627__$1);

return statearr_16682;
})();
if(cljs.core.truth_(inst_16629__$1)){
var statearr_16683_17830 = state_16674__$1;
(statearr_16683_17830[(1)] = (5));

} else {
var statearr_16684_17831 = state_16674__$1;
(statearr_16684_17831[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16675 === (15))){
var inst_16619 = (state_16674[(15)]);
var inst_16648 = (state_16674[(16)]);
var inst_16648__$1 = cljs.core.empty_QMARK_(inst_16619);
var state_16674__$1 = (function (){var statearr_16685 = state_16674;
(statearr_16685[(16)] = inst_16648__$1);

return statearr_16685;
})();
if(inst_16648__$1){
var statearr_16686_17832 = state_16674__$1;
(statearr_16686_17832[(1)] = (17));

} else {
var statearr_16687_17833 = state_16674__$1;
(statearr_16687_17833[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16675 === (21))){
var inst_16618 = (state_16674[(12)]);
var inst_16615 = inst_16618;
var state_16674__$1 = (function (){var statearr_16688 = state_16674;
(statearr_16688[(11)] = inst_16615);

return statearr_16688;
})();
var statearr_16689_17834 = state_16674__$1;
(statearr_16689_17834[(2)] = null);

(statearr_16689_17834[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16675 === (13))){
var inst_16641 = (state_16674[(2)]);
var inst_16642 = calc_state();
var inst_16615 = inst_16642;
var state_16674__$1 = (function (){var statearr_16690 = state_16674;
(statearr_16690[(17)] = inst_16641);

(statearr_16690[(11)] = inst_16615);

return statearr_16690;
})();
var statearr_16691_17835 = state_16674__$1;
(statearr_16691_17835[(2)] = null);

(statearr_16691_17835[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16675 === (22))){
var inst_16668 = (state_16674[(2)]);
var state_16674__$1 = state_16674;
var statearr_16692_17837 = state_16674__$1;
(statearr_16692_17837[(2)] = inst_16668);

(statearr_16692_17837[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16675 === (6))){
var inst_16628 = (state_16674[(14)]);
var inst_16632 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_16628,change);
var state_16674__$1 = state_16674;
var statearr_16693_17839 = state_16674__$1;
(statearr_16693_17839[(2)] = inst_16632);

(statearr_16693_17839[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16675 === (25))){
var state_16674__$1 = state_16674;
var statearr_16694_17840 = state_16674__$1;
(statearr_16694_17840[(2)] = null);

(statearr_16694_17840[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16675 === (17))){
var inst_16628 = (state_16674[(14)]);
var inst_16620 = (state_16674[(18)]);
var inst_16650 = (inst_16620.cljs$core$IFn$_invoke$arity$1 ? inst_16620.cljs$core$IFn$_invoke$arity$1(inst_16628) : inst_16620.call(null,inst_16628));
var inst_16651 = cljs.core.not(inst_16650);
var state_16674__$1 = state_16674;
var statearr_16695_17841 = state_16674__$1;
(statearr_16695_17841[(2)] = inst_16651);

(statearr_16695_17841[(1)] = (19));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16675 === (3))){
var inst_16672 = (state_16674[(2)]);
var state_16674__$1 = state_16674;
return cljs.core.async.impl.ioc_helpers.return_chan(state_16674__$1,inst_16672);
} else {
if((state_val_16675 === (12))){
var state_16674__$1 = state_16674;
var statearr_16696_17842 = state_16674__$1;
(statearr_16696_17842[(2)] = null);

(statearr_16696_17842[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16675 === (2))){
var inst_16618 = (state_16674[(12)]);
var inst_16615 = (state_16674[(11)]);
var inst_16618__$1 = cljs.core.__destructure_map(inst_16615);
var inst_16619 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_16618__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_16620 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_16618__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_16621 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_16618__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var state_16674__$1 = (function (){var statearr_16697 = state_16674;
(statearr_16697[(15)] = inst_16619);

(statearr_16697[(18)] = inst_16620);

(statearr_16697[(12)] = inst_16618__$1);

return statearr_16697;
})();
return cljs.core.async.ioc_alts_BANG_(state_16674__$1,(4),inst_16621);
} else {
if((state_val_16675 === (23))){
var inst_16659 = (state_16674[(2)]);
var state_16674__$1 = state_16674;
if(cljs.core.truth_(inst_16659)){
var statearr_16698_17843 = state_16674__$1;
(statearr_16698_17843[(1)] = (24));

} else {
var statearr_16699_17844 = state_16674__$1;
(statearr_16699_17844[(1)] = (25));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16675 === (19))){
var inst_16654 = (state_16674[(2)]);
var state_16674__$1 = state_16674;
var statearr_16700_17845 = state_16674__$1;
(statearr_16700_17845[(2)] = inst_16654);

(statearr_16700_17845[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16675 === (11))){
var inst_16628 = (state_16674[(14)]);
var inst_16638 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(cs,cljs.core.dissoc,inst_16628);
var state_16674__$1 = state_16674;
var statearr_16701_17846 = state_16674__$1;
(statearr_16701_17846[(2)] = inst_16638);

(statearr_16701_17846[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16675 === (9))){
var inst_16619 = (state_16674[(15)]);
var inst_16628 = (state_16674[(14)]);
var inst_16645 = (state_16674[(19)]);
var inst_16645__$1 = (inst_16619.cljs$core$IFn$_invoke$arity$1 ? inst_16619.cljs$core$IFn$_invoke$arity$1(inst_16628) : inst_16619.call(null,inst_16628));
var state_16674__$1 = (function (){var statearr_16702 = state_16674;
(statearr_16702[(19)] = inst_16645__$1);

return statearr_16702;
})();
if(cljs.core.truth_(inst_16645__$1)){
var statearr_16703_17847 = state_16674__$1;
(statearr_16703_17847[(1)] = (14));

} else {
var statearr_16704_17848 = state_16674__$1;
(statearr_16704_17848[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16675 === (5))){
var inst_16629 = (state_16674[(13)]);
var state_16674__$1 = state_16674;
var statearr_16705_17849 = state_16674__$1;
(statearr_16705_17849[(2)] = inst_16629);

(statearr_16705_17849[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16675 === (14))){
var inst_16645 = (state_16674[(19)]);
var state_16674__$1 = state_16674;
var statearr_16706_17850 = state_16674__$1;
(statearr_16706_17850[(2)] = inst_16645);

(statearr_16706_17850[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16675 === (26))){
var inst_16664 = (state_16674[(2)]);
var state_16674__$1 = state_16674;
var statearr_16707_17852 = state_16674__$1;
(statearr_16707_17852[(2)] = inst_16664);

(statearr_16707_17852[(1)] = (22));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16675 === (16))){
var inst_16656 = (state_16674[(2)]);
var state_16674__$1 = state_16674;
if(cljs.core.truth_(inst_16656)){
var statearr_16708_17854 = state_16674__$1;
(statearr_16708_17854[(1)] = (20));

} else {
var statearr_16709_17855 = state_16674__$1;
(statearr_16709_17855[(1)] = (21));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16675 === (10))){
var inst_16670 = (state_16674[(2)]);
var state_16674__$1 = state_16674;
var statearr_16710_17856 = state_16674__$1;
(statearr_16710_17856[(2)] = inst_16670);

(statearr_16710_17856[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16675 === (18))){
var inst_16648 = (state_16674[(16)]);
var state_16674__$1 = state_16674;
var statearr_16711_17857 = state_16674__$1;
(statearr_16711_17857[(2)] = inst_16648);

(statearr_16711_17857[(1)] = (19));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16675 === (8))){
var inst_16627 = (state_16674[(7)]);
var inst_16636 = (inst_16627 == null);
var state_16674__$1 = state_16674;
if(cljs.core.truth_(inst_16636)){
var statearr_16712_17858 = state_16674__$1;
(statearr_16712_17858[(1)] = (11));

} else {
var statearr_16713_17859 = state_16674__$1;
(statearr_16713_17859[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mix_$_state_machine__15401__auto__ = null;
var cljs$core$async$mix_$_state_machine__15401__auto____0 = (function (){
var statearr_16714 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_16714[(0)] = cljs$core$async$mix_$_state_machine__15401__auto__);

(statearr_16714[(1)] = (1));

return statearr_16714;
});
var cljs$core$async$mix_$_state_machine__15401__auto____1 = (function (state_16674){
while(true){
var ret_value__15402__auto__ = (function (){try{while(true){
var result__15403__auto__ = switch__15400__auto__(state_16674);
if(cljs.core.keyword_identical_QMARK_(result__15403__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__15403__auto__;
}
break;
}
}catch (e16715){var ex__15404__auto__ = e16715;
var statearr_16716_17860 = state_16674;
(statearr_16716_17860[(2)] = ex__15404__auto__);


if(cljs.core.seq((state_16674[(4)]))){
var statearr_16717_17861 = state_16674;
(statearr_16717_17861[(1)] = cljs.core.first((state_16674[(4)])));

} else {
throw ex__15404__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__15402__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__17862 = state_16674;
state_16674 = G__17862;
continue;
} else {
return ret_value__15402__auto__;
}
break;
}
});
cljs$core$async$mix_$_state_machine__15401__auto__ = function(state_16674){
switch(arguments.length){
case 0:
return cljs$core$async$mix_$_state_machine__15401__auto____0.call(this);
case 1:
return cljs$core$async$mix_$_state_machine__15401__auto____1.call(this,state_16674);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mix_$_state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mix_$_state_machine__15401__auto____0;
cljs$core$async$mix_$_state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mix_$_state_machine__15401__auto____1;
return cljs$core$async$mix_$_state_machine__15401__auto__;
})()
})();
var state__15646__auto__ = (function (){var statearr_16718 = f__15645__auto__();
(statearr_16718[(6)] = c__15644__auto___17823);

return statearr_16718;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__15646__auto__);
}));


return m;
});
/**
 * Adds ch as an input to the mix
 */
cljs.core.async.admix = (function cljs$core$async$admix(mix,ch){
return cljs.core.async.admix_STAR_(mix,ch);
});
/**
 * Removes ch as an input to the mix
 */
cljs.core.async.unmix = (function cljs$core$async$unmix(mix,ch){
return cljs.core.async.unmix_STAR_(mix,ch);
});
/**
 * removes all inputs from the mix
 */
cljs.core.async.unmix_all = (function cljs$core$async$unmix_all(mix){
return cljs.core.async.unmix_all_STAR_(mix);
});
/**
 * Atomically sets the state(s) of one or more channels in a mix. The
 *   state map is a map of channels -> channel-state-map. A
 *   channel-state-map is a map of attrs -> boolean, where attr is one or
 *   more of :mute, :pause or :solo. Any states supplied are merged with
 *   the current state.
 * 
 *   Note that channels can be added to a mix via toggle, which can be
 *   used to add channels in a particular (e.g. paused) state.
 */
cljs.core.async.toggle = (function cljs$core$async$toggle(mix,state_map){
return cljs.core.async.toggle_STAR_(mix,state_map);
});
/**
 * Sets the solo mode of the mix. mode must be one of :mute or :pause
 */
cljs.core.async.solo_mode = (function cljs$core$async$solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_(mix,mode);
});

/**
 * @interface
 */
cljs.core.async.Pub = function(){};

var cljs$core$async$Pub$sub_STAR_$dyn_17863 = (function (p,v,ch,close_QMARK_){
var x__5393__auto__ = (((p == null))?null:p);
var m__5394__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__5393__auto__)]);
if((!((m__5394__auto__ == null)))){
return (m__5394__auto__.cljs$core$IFn$_invoke$arity$4 ? m__5394__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__5394__auto__.call(null,p,v,ch,close_QMARK_));
} else {
var m__5392__auto__ = (cljs.core.async.sub_STAR_["_"]);
if((!((m__5392__auto__ == null)))){
return (m__5392__auto__.cljs$core$IFn$_invoke$arity$4 ? m__5392__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__5392__auto__.call(null,p,v,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Pub.sub*",p);
}
}
});
cljs.core.async.sub_STAR_ = (function cljs$core$async$sub_STAR_(p,v,ch,close_QMARK_){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$sub_STAR_$arity$4 == null)))))){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
return cljs$core$async$Pub$sub_STAR_$dyn_17863(p,v,ch,close_QMARK_);
}
});

var cljs$core$async$Pub$unsub_STAR_$dyn_17864 = (function (p,v,ch){
var x__5393__auto__ = (((p == null))?null:p);
var m__5394__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__5393__auto__)]);
if((!((m__5394__auto__ == null)))){
return (m__5394__auto__.cljs$core$IFn$_invoke$arity$3 ? m__5394__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__5394__auto__.call(null,p,v,ch));
} else {
var m__5392__auto__ = (cljs.core.async.unsub_STAR_["_"]);
if((!((m__5392__auto__ == null)))){
return (m__5392__auto__.cljs$core$IFn$_invoke$arity$3 ? m__5392__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__5392__auto__.call(null,p,v,ch));
} else {
throw cljs.core.missing_protocol("Pub.unsub*",p);
}
}
});
cljs.core.async.unsub_STAR_ = (function cljs$core$async$unsub_STAR_(p,v,ch){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_STAR_$arity$3 == null)))))){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
return cljs$core$async$Pub$unsub_STAR_$dyn_17864(p,v,ch);
}
});

var cljs$core$async$Pub$unsub_all_STAR_$dyn_17866 = (function() {
var G__17867 = null;
var G__17867__1 = (function (p){
var x__5393__auto__ = (((p == null))?null:p);
var m__5394__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__5393__auto__)]);
if((!((m__5394__auto__ == null)))){
return (m__5394__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5394__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__5394__auto__.call(null,p));
} else {
var m__5392__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__5392__auto__ == null)))){
return (m__5392__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5392__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__5392__auto__.call(null,p));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
});
var G__17867__2 = (function (p,v){
var x__5393__auto__ = (((p == null))?null:p);
var m__5394__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__5393__auto__)]);
if((!((m__5394__auto__ == null)))){
return (m__5394__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5394__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__5394__auto__.call(null,p,v));
} else {
var m__5392__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__5392__auto__ == null)))){
return (m__5392__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5392__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__5392__auto__.call(null,p,v));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
});
G__17867 = function(p,v){
switch(arguments.length){
case 1:
return G__17867__1.call(this,p);
case 2:
return G__17867__2.call(this,p,v);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
G__17867.cljs$core$IFn$_invoke$arity$1 = G__17867__1;
G__17867.cljs$core$IFn$_invoke$arity$2 = G__17867__2;
return G__17867;
})()
;
cljs.core.async.unsub_all_STAR_ = (function cljs$core$async$unsub_all_STAR_(var_args){
var G__16720 = arguments.length;
switch (G__16720) {
case 1:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (p){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$1 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
return cljs$core$async$Pub$unsub_all_STAR_$dyn_17866(p);
}
}));

(cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (p,v){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$2 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
return cljs$core$async$Pub$unsub_all_STAR_$dyn_17866(p,v);
}
}));

(cljs.core.async.unsub_all_STAR_.cljs$lang$maxFixedArity = 2);


/**
 * Creates and returns a pub(lication) of the supplied channel,
 *   partitioned into topics by the topic-fn. topic-fn will be applied to
 *   each value on the channel and the result will determine the 'topic'
 *   on which that value will be put. Channels can be subscribed to
 *   receive copies of topics using 'sub', and unsubscribed using
 *   'unsub'. Each topic will be handled by an internal mult on a
 *   dedicated channel. By default these internal channels are
 *   unbuffered, but a buf-fn can be supplied which, given a topic,
 *   creates a buffer with desired properties.
 * 
 *   Each item is distributed to all subs in parallel and synchronously,
 *   i.e. each sub must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow subs from holding up the pub.
 * 
 *   Items received when there are no matching subs get dropped.
 * 
 *   Note that if buf-fns are used then each topic is handled
 *   asynchronously, i.e. if a channel is subscribed to more than one
 *   topic it should not expect them to be interleaved identically with
 *   the source.
 */
cljs.core.async.pub = (function cljs$core$async$pub(var_args){
var G__16723 = arguments.length;
switch (G__16723) {
case 2:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2 = (function (ch,topic_fn){
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3(ch,topic_fn,cljs.core.constantly(null));
}));

(cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = (function (topic){
var or__5045__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(mults),topic);
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(mults,(function (p1__16721_SHARP_){
if(cljs.core.truth_((p1__16721_SHARP_.cljs$core$IFn$_invoke$arity$1 ? p1__16721_SHARP_.cljs$core$IFn$_invoke$arity$1(topic) : p1__16721_SHARP_.call(null,topic)))){
return p1__16721_SHARP_;
} else {
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(p1__16721_SHARP_,topic,cljs.core.async.mult(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((buf_fn.cljs$core$IFn$_invoke$arity$1 ? buf_fn.cljs$core$IFn$_invoke$arity$1(topic) : buf_fn.call(null,topic)))));
}
})),topic);
}
});
var p = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async16724 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Pub}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async16724 = (function (ch,topic_fn,buf_fn,mults,ensure_mult,meta16725){
this.ch = ch;
this.topic_fn = topic_fn;
this.buf_fn = buf_fn;
this.mults = mults;
this.ensure_mult = ensure_mult;
this.meta16725 = meta16725;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async16724.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_16726,meta16725__$1){
var self__ = this;
var _16726__$1 = this;
return (new cljs.core.async.t_cljs$core$async16724(self__.ch,self__.topic_fn,self__.buf_fn,self__.mults,self__.ensure_mult,meta16725__$1));
}));

(cljs.core.async.t_cljs$core$async16724.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_16726){
var self__ = this;
var _16726__$1 = this;
return self__.meta16725;
}));

(cljs.core.async.t_cljs$core$async16724.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async16724.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
}));

(cljs.core.async.t_cljs$core$async16724.prototype.cljs$core$async$Pub$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async16724.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = (self__.ensure_mult.cljs$core$IFn$_invoke$arity$1 ? self__.ensure_mult.cljs$core$IFn$_invoke$arity$1(topic) : self__.ensure_mult.call(null,topic));
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(m,ch__$1,close_QMARK_);
}));

(cljs.core.async.t_cljs$core$async16724.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__5804__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(self__.mults),topic);
if(cljs.core.truth_(temp__5804__auto__)){
var m = temp__5804__auto__;
return cljs.core.async.untap(m,ch__$1);
} else {
return null;
}
}));

(cljs.core.async.t_cljs$core$async16724.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_(self__.mults,cljs.core.PersistentArrayMap.EMPTY);
}));

(cljs.core.async.t_cljs$core$async16724.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.mults,cljs.core.dissoc,topic);
}));

(cljs.core.async.t_cljs$core$async16724.getBasis = (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"topic-fn","topic-fn",-862449736,null),new cljs.core.Symbol(null,"buf-fn","buf-fn",-1200281591,null),new cljs.core.Symbol(null,"mults","mults",-461114485,null),new cljs.core.Symbol(null,"ensure-mult","ensure-mult",1796584816,null),new cljs.core.Symbol(null,"meta16725","meta16725",1682247048,null)], null);
}));

(cljs.core.async.t_cljs$core$async16724.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async16724.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async16724");

(cljs.core.async.t_cljs$core$async16724.cljs$lang$ctorPrWriter = (function (this__5330__auto__,writer__5331__auto__,opt__5332__auto__){
return cljs.core._write(writer__5331__auto__,"cljs.core.async/t_cljs$core$async16724");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async16724.
 */
cljs.core.async.__GT_t_cljs$core$async16724 = (function cljs$core$async$__GT_t_cljs$core$async16724(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta16725){
return (new cljs.core.async.t_cljs$core$async16724(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta16725));
});

}

return (new cljs.core.async.t_cljs$core$async16724(ch,topic_fn,buf_fn,mults,ensure_mult,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__15644__auto___17874 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__15645__auto__ = (function (){var switch__15400__auto__ = (function (state_16798){
var state_val_16799 = (state_16798[(1)]);
if((state_val_16799 === (7))){
var inst_16794 = (state_16798[(2)]);
var state_16798__$1 = state_16798;
var statearr_16800_17875 = state_16798__$1;
(statearr_16800_17875[(2)] = inst_16794);

(statearr_16800_17875[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16799 === (20))){
var state_16798__$1 = state_16798;
var statearr_16801_17876 = state_16798__$1;
(statearr_16801_17876[(2)] = null);

(statearr_16801_17876[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16799 === (1))){
var state_16798__$1 = state_16798;
var statearr_16802_17877 = state_16798__$1;
(statearr_16802_17877[(2)] = null);

(statearr_16802_17877[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16799 === (24))){
var inst_16777 = (state_16798[(7)]);
var inst_16786 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(mults,cljs.core.dissoc,inst_16777);
var state_16798__$1 = state_16798;
var statearr_16803_17878 = state_16798__$1;
(statearr_16803_17878[(2)] = inst_16786);

(statearr_16803_17878[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16799 === (4))){
var inst_16729 = (state_16798[(8)]);
var inst_16729__$1 = (state_16798[(2)]);
var inst_16730 = (inst_16729__$1 == null);
var state_16798__$1 = (function (){var statearr_16804 = state_16798;
(statearr_16804[(8)] = inst_16729__$1);

return statearr_16804;
})();
if(cljs.core.truth_(inst_16730)){
var statearr_16805_17879 = state_16798__$1;
(statearr_16805_17879[(1)] = (5));

} else {
var statearr_16806_17880 = state_16798__$1;
(statearr_16806_17880[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16799 === (15))){
var inst_16771 = (state_16798[(2)]);
var state_16798__$1 = state_16798;
var statearr_16807_17881 = state_16798__$1;
(statearr_16807_17881[(2)] = inst_16771);

(statearr_16807_17881[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16799 === (21))){
var inst_16791 = (state_16798[(2)]);
var state_16798__$1 = (function (){var statearr_16808 = state_16798;
(statearr_16808[(9)] = inst_16791);

return statearr_16808;
})();
var statearr_16809_17882 = state_16798__$1;
(statearr_16809_17882[(2)] = null);

(statearr_16809_17882[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16799 === (13))){
var inst_16753 = (state_16798[(10)]);
var inst_16755 = cljs.core.chunked_seq_QMARK_(inst_16753);
var state_16798__$1 = state_16798;
if(inst_16755){
var statearr_16810_17887 = state_16798__$1;
(statearr_16810_17887[(1)] = (16));

} else {
var statearr_16811_17888 = state_16798__$1;
(statearr_16811_17888[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16799 === (22))){
var inst_16783 = (state_16798[(2)]);
var state_16798__$1 = state_16798;
if(cljs.core.truth_(inst_16783)){
var statearr_16812_17892 = state_16798__$1;
(statearr_16812_17892[(1)] = (23));

} else {
var statearr_16813_17893 = state_16798__$1;
(statearr_16813_17893[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16799 === (6))){
var inst_16729 = (state_16798[(8)]);
var inst_16777 = (state_16798[(7)]);
var inst_16779 = (state_16798[(11)]);
var inst_16777__$1 = (topic_fn.cljs$core$IFn$_invoke$arity$1 ? topic_fn.cljs$core$IFn$_invoke$arity$1(inst_16729) : topic_fn.call(null,inst_16729));
var inst_16778 = cljs.core.deref(mults);
var inst_16779__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_16778,inst_16777__$1);
var state_16798__$1 = (function (){var statearr_16814 = state_16798;
(statearr_16814[(7)] = inst_16777__$1);

(statearr_16814[(11)] = inst_16779__$1);

return statearr_16814;
})();
if(cljs.core.truth_(inst_16779__$1)){
var statearr_16815_17894 = state_16798__$1;
(statearr_16815_17894[(1)] = (19));

} else {
var statearr_16816_17895 = state_16798__$1;
(statearr_16816_17895[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16799 === (25))){
var inst_16788 = (state_16798[(2)]);
var state_16798__$1 = state_16798;
var statearr_16817_17899 = state_16798__$1;
(statearr_16817_17899[(2)] = inst_16788);

(statearr_16817_17899[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16799 === (17))){
var inst_16753 = (state_16798[(10)]);
var inst_16762 = cljs.core.first(inst_16753);
var inst_16763 = cljs.core.async.muxch_STAR_(inst_16762);
var inst_16764 = cljs.core.async.close_BANG_(inst_16763);
var inst_16765 = cljs.core.next(inst_16753);
var inst_16739 = inst_16765;
var inst_16740 = null;
var inst_16741 = (0);
var inst_16742 = (0);
var state_16798__$1 = (function (){var statearr_16818 = state_16798;
(statearr_16818[(12)] = inst_16741);

(statearr_16818[(13)] = inst_16740);

(statearr_16818[(14)] = inst_16764);

(statearr_16818[(15)] = inst_16739);

(statearr_16818[(16)] = inst_16742);

return statearr_16818;
})();
var statearr_16819_17903 = state_16798__$1;
(statearr_16819_17903[(2)] = null);

(statearr_16819_17903[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16799 === (3))){
var inst_16796 = (state_16798[(2)]);
var state_16798__$1 = state_16798;
return cljs.core.async.impl.ioc_helpers.return_chan(state_16798__$1,inst_16796);
} else {
if((state_val_16799 === (12))){
var inst_16773 = (state_16798[(2)]);
var state_16798__$1 = state_16798;
var statearr_16820_17904 = state_16798__$1;
(statearr_16820_17904[(2)] = inst_16773);

(statearr_16820_17904[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16799 === (2))){
var state_16798__$1 = state_16798;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_16798__$1,(4),ch);
} else {
if((state_val_16799 === (23))){
var state_16798__$1 = state_16798;
var statearr_16821_17908 = state_16798__$1;
(statearr_16821_17908[(2)] = null);

(statearr_16821_17908[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16799 === (19))){
var inst_16729 = (state_16798[(8)]);
var inst_16779 = (state_16798[(11)]);
var inst_16781 = cljs.core.async.muxch_STAR_(inst_16779);
var state_16798__$1 = state_16798;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_16798__$1,(22),inst_16781,inst_16729);
} else {
if((state_val_16799 === (11))){
var inst_16753 = (state_16798[(10)]);
var inst_16739 = (state_16798[(15)]);
var inst_16753__$1 = cljs.core.seq(inst_16739);
var state_16798__$1 = (function (){var statearr_16822 = state_16798;
(statearr_16822[(10)] = inst_16753__$1);

return statearr_16822;
})();
if(inst_16753__$1){
var statearr_16823_17913 = state_16798__$1;
(statearr_16823_17913[(1)] = (13));

} else {
var statearr_16824_17914 = state_16798__$1;
(statearr_16824_17914[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16799 === (9))){
var inst_16775 = (state_16798[(2)]);
var state_16798__$1 = state_16798;
var statearr_16825_17915 = state_16798__$1;
(statearr_16825_17915[(2)] = inst_16775);

(statearr_16825_17915[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16799 === (5))){
var inst_16736 = cljs.core.deref(mults);
var inst_16737 = cljs.core.vals(inst_16736);
var inst_16738 = cljs.core.seq(inst_16737);
var inst_16739 = inst_16738;
var inst_16740 = null;
var inst_16741 = (0);
var inst_16742 = (0);
var state_16798__$1 = (function (){var statearr_16826 = state_16798;
(statearr_16826[(12)] = inst_16741);

(statearr_16826[(13)] = inst_16740);

(statearr_16826[(15)] = inst_16739);

(statearr_16826[(16)] = inst_16742);

return statearr_16826;
})();
var statearr_16827_17916 = state_16798__$1;
(statearr_16827_17916[(2)] = null);

(statearr_16827_17916[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16799 === (14))){
var state_16798__$1 = state_16798;
var statearr_16831_17917 = state_16798__$1;
(statearr_16831_17917[(2)] = null);

(statearr_16831_17917[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16799 === (16))){
var inst_16753 = (state_16798[(10)]);
var inst_16757 = cljs.core.chunk_first(inst_16753);
var inst_16758 = cljs.core.chunk_rest(inst_16753);
var inst_16759 = cljs.core.count(inst_16757);
var inst_16739 = inst_16758;
var inst_16740 = inst_16757;
var inst_16741 = inst_16759;
var inst_16742 = (0);
var state_16798__$1 = (function (){var statearr_16832 = state_16798;
(statearr_16832[(12)] = inst_16741);

(statearr_16832[(13)] = inst_16740);

(statearr_16832[(15)] = inst_16739);

(statearr_16832[(16)] = inst_16742);

return statearr_16832;
})();
var statearr_16833_17918 = state_16798__$1;
(statearr_16833_17918[(2)] = null);

(statearr_16833_17918[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16799 === (10))){
var inst_16741 = (state_16798[(12)]);
var inst_16740 = (state_16798[(13)]);
var inst_16739 = (state_16798[(15)]);
var inst_16742 = (state_16798[(16)]);
var inst_16747 = cljs.core._nth(inst_16740,inst_16742);
var inst_16748 = cljs.core.async.muxch_STAR_(inst_16747);
var inst_16749 = cljs.core.async.close_BANG_(inst_16748);
var inst_16750 = (inst_16742 + (1));
var tmp16828 = inst_16741;
var tmp16829 = inst_16740;
var tmp16830 = inst_16739;
var inst_16739__$1 = tmp16830;
var inst_16740__$1 = tmp16829;
var inst_16741__$1 = tmp16828;
var inst_16742__$1 = inst_16750;
var state_16798__$1 = (function (){var statearr_16834 = state_16798;
(statearr_16834[(12)] = inst_16741__$1);

(statearr_16834[(13)] = inst_16740__$1);

(statearr_16834[(17)] = inst_16749);

(statearr_16834[(15)] = inst_16739__$1);

(statearr_16834[(16)] = inst_16742__$1);

return statearr_16834;
})();
var statearr_16835_17919 = state_16798__$1;
(statearr_16835_17919[(2)] = null);

(statearr_16835_17919[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16799 === (18))){
var inst_16768 = (state_16798[(2)]);
var state_16798__$1 = state_16798;
var statearr_16836_17920 = state_16798__$1;
(statearr_16836_17920[(2)] = inst_16768);

(statearr_16836_17920[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16799 === (8))){
var inst_16741 = (state_16798[(12)]);
var inst_16742 = (state_16798[(16)]);
var inst_16744 = (inst_16742 < inst_16741);
var inst_16745 = inst_16744;
var state_16798__$1 = state_16798;
if(cljs.core.truth_(inst_16745)){
var statearr_16837_17921 = state_16798__$1;
(statearr_16837_17921[(1)] = (10));

} else {
var statearr_16838_17922 = state_16798__$1;
(statearr_16838_17922[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__15401__auto__ = null;
var cljs$core$async$state_machine__15401__auto____0 = (function (){
var statearr_16839 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_16839[(0)] = cljs$core$async$state_machine__15401__auto__);

(statearr_16839[(1)] = (1));

return statearr_16839;
});
var cljs$core$async$state_machine__15401__auto____1 = (function (state_16798){
while(true){
var ret_value__15402__auto__ = (function (){try{while(true){
var result__15403__auto__ = switch__15400__auto__(state_16798);
if(cljs.core.keyword_identical_QMARK_(result__15403__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__15403__auto__;
}
break;
}
}catch (e16840){var ex__15404__auto__ = e16840;
var statearr_16841_17925 = state_16798;
(statearr_16841_17925[(2)] = ex__15404__auto__);


if(cljs.core.seq((state_16798[(4)]))){
var statearr_16842_17926 = state_16798;
(statearr_16842_17926[(1)] = cljs.core.first((state_16798[(4)])));

} else {
throw ex__15404__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__15402__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__17927 = state_16798;
state_16798 = G__17927;
continue;
} else {
return ret_value__15402__auto__;
}
break;
}
});
cljs$core$async$state_machine__15401__auto__ = function(state_16798){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__15401__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__15401__auto____1.call(this,state_16798);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__15401__auto____0;
cljs$core$async$state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__15401__auto____1;
return cljs$core$async$state_machine__15401__auto__;
})()
})();
var state__15646__auto__ = (function (){var statearr_16843 = f__15645__auto__();
(statearr_16843[(6)] = c__15644__auto___17874);

return statearr_16843;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__15646__auto__);
}));


return p;
}));

(cljs.core.async.pub.cljs$lang$maxFixedArity = 3);

/**
 * Subscribes a channel to a topic of a pub.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.sub = (function cljs$core$async$sub(var_args){
var G__16845 = arguments.length;
switch (G__16845) {
case 3:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3 = (function (p,topic,ch){
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4(p,topic,ch,true);
}));

(cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_(p,topic,ch,close_QMARK_);
}));

(cljs.core.async.sub.cljs$lang$maxFixedArity = 4);

/**
 * Unsubscribes a channel from a topic of a pub
 */
cljs.core.async.unsub = (function cljs$core$async$unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_(p,topic,ch);
});
/**
 * Unsubscribes all channels from a pub, or a topic of a pub
 */
cljs.core.async.unsub_all = (function cljs$core$async$unsub_all(var_args){
var G__16847 = arguments.length;
switch (G__16847) {
case 1:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1 = (function (p){
return cljs.core.async.unsub_all_STAR_(p);
}));

(cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_(p,topic);
}));

(cljs.core.async.unsub_all.cljs$lang$maxFixedArity = 2);

/**
 * Takes a function and a collection of source channels, and returns a
 *   channel which contains the values produced by applying f to the set
 *   of first items taken from each source channel, followed by applying
 *   f to the set of second items from each channel, until any one of the
 *   channels is closed, at which point the output channel will be
 *   closed. The returned channel will be unbuffered by default, or a
 *   buf-or-n can be supplied
 */
cljs.core.async.map = (function cljs$core$async$map(var_args){
var G__16849 = arguments.length;
switch (G__16849) {
case 2:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.map.cljs$core$IFn$_invoke$arity$2 = (function (f,chs){
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3(f,chs,null);
}));

(cljs.core.async.map.cljs$core$IFn$_invoke$arity$3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec(chs);
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var cnt = cljs.core.count(chs__$1);
var rets = cljs.core.object_array.cljs$core$IFn$_invoke$arity$1(cnt);
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = cljs.core.mapv.cljs$core$IFn$_invoke$arity$2((function (i){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,rets.slice((0)));
} else {
return null;
}
});
}),cljs.core.range.cljs$core$IFn$_invoke$arity$1(cnt));
if((cnt === (0))){
cljs.core.async.close_BANG_(out);
} else {
var c__15644__auto___17931 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__15645__auto__ = (function (){var switch__15400__auto__ = (function (state_16892){
var state_val_16893 = (state_16892[(1)]);
if((state_val_16893 === (7))){
var state_16892__$1 = state_16892;
var statearr_16894_17932 = state_16892__$1;
(statearr_16894_17932[(2)] = null);

(statearr_16894_17932[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16893 === (1))){
var state_16892__$1 = state_16892;
var statearr_16895_17933 = state_16892__$1;
(statearr_16895_17933[(2)] = null);

(statearr_16895_17933[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16893 === (4))){
var inst_16852 = (state_16892[(7)]);
var inst_16853 = (state_16892[(8)]);
var inst_16855 = (inst_16853 < inst_16852);
var state_16892__$1 = state_16892;
if(cljs.core.truth_(inst_16855)){
var statearr_16896_17935 = state_16892__$1;
(statearr_16896_17935[(1)] = (6));

} else {
var statearr_16897_17939 = state_16892__$1;
(statearr_16897_17939[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16893 === (15))){
var inst_16878 = (state_16892[(9)]);
var inst_16883 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(f,inst_16878);
var state_16892__$1 = state_16892;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_16892__$1,(17),out,inst_16883);
} else {
if((state_val_16893 === (13))){
var inst_16878 = (state_16892[(9)]);
var inst_16878__$1 = (state_16892[(2)]);
var inst_16879 = cljs.core.some(cljs.core.nil_QMARK_,inst_16878__$1);
var state_16892__$1 = (function (){var statearr_16902 = state_16892;
(statearr_16902[(9)] = inst_16878__$1);

return statearr_16902;
})();
if(cljs.core.truth_(inst_16879)){
var statearr_16903_17940 = state_16892__$1;
(statearr_16903_17940[(1)] = (14));

} else {
var statearr_16904_17941 = state_16892__$1;
(statearr_16904_17941[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16893 === (6))){
var state_16892__$1 = state_16892;
var statearr_16905_17942 = state_16892__$1;
(statearr_16905_17942[(2)] = null);

(statearr_16905_17942[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16893 === (17))){
var inst_16885 = (state_16892[(2)]);
var state_16892__$1 = (function (){var statearr_16910 = state_16892;
(statearr_16910[(10)] = inst_16885);

return statearr_16910;
})();
var statearr_16911_17943 = state_16892__$1;
(statearr_16911_17943[(2)] = null);

(statearr_16911_17943[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16893 === (3))){
var inst_16890 = (state_16892[(2)]);
var state_16892__$1 = state_16892;
return cljs.core.async.impl.ioc_helpers.return_chan(state_16892__$1,inst_16890);
} else {
if((state_val_16893 === (12))){
var _ = (function (){var statearr_16913 = state_16892;
(statearr_16913[(4)] = cljs.core.rest((state_16892[(4)])));

return statearr_16913;
})();
var state_16892__$1 = state_16892;
var ex16909 = (state_16892__$1[(2)]);
var statearr_16915_17944 = state_16892__$1;
(statearr_16915_17944[(5)] = ex16909);


if((ex16909 instanceof Object)){
var statearr_16916_17945 = state_16892__$1;
(statearr_16916_17945[(1)] = (11));

(statearr_16916_17945[(5)] = null);

} else {
throw ex16909;

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16893 === (2))){
var inst_16851 = cljs.core.reset_BANG_(dctr,cnt);
var inst_16852 = cnt;
var inst_16853 = (0);
var state_16892__$1 = (function (){var statearr_16917 = state_16892;
(statearr_16917[(11)] = inst_16851);

(statearr_16917[(7)] = inst_16852);

(statearr_16917[(8)] = inst_16853);

return statearr_16917;
})();
var statearr_16918_17946 = state_16892__$1;
(statearr_16918_17946[(2)] = null);

(statearr_16918_17946[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16893 === (11))){
var inst_16857 = (state_16892[(2)]);
var inst_16858 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec);
var state_16892__$1 = (function (){var statearr_16919 = state_16892;
(statearr_16919[(12)] = inst_16857);

return statearr_16919;
})();
var statearr_16920_17947 = state_16892__$1;
(statearr_16920_17947[(2)] = inst_16858);

(statearr_16920_17947[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16893 === (9))){
var inst_16853 = (state_16892[(8)]);
var _ = (function (){var statearr_16921 = state_16892;
(statearr_16921[(4)] = cljs.core.cons((12),(state_16892[(4)])));

return statearr_16921;
})();
var inst_16864 = (chs__$1.cljs$core$IFn$_invoke$arity$1 ? chs__$1.cljs$core$IFn$_invoke$arity$1(inst_16853) : chs__$1.call(null,inst_16853));
var inst_16865 = (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(inst_16853) : done.call(null,inst_16853));
var inst_16866 = cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2(inst_16864,inst_16865);
var ___$1 = (function (){var statearr_16922 = state_16892;
(statearr_16922[(4)] = cljs.core.rest((state_16892[(4)])));

return statearr_16922;
})();
var state_16892__$1 = state_16892;
var statearr_16923_17948 = state_16892__$1;
(statearr_16923_17948[(2)] = inst_16866);

(statearr_16923_17948[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16893 === (5))){
var inst_16876 = (state_16892[(2)]);
var state_16892__$1 = (function (){var statearr_16924 = state_16892;
(statearr_16924[(13)] = inst_16876);

return statearr_16924;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_16892__$1,(13),dchan);
} else {
if((state_val_16893 === (14))){
var inst_16881 = cljs.core.async.close_BANG_(out);
var state_16892__$1 = state_16892;
var statearr_16925_17949 = state_16892__$1;
(statearr_16925_17949[(2)] = inst_16881);

(statearr_16925_17949[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16893 === (16))){
var inst_16888 = (state_16892[(2)]);
var state_16892__$1 = state_16892;
var statearr_16926_17950 = state_16892__$1;
(statearr_16926_17950[(2)] = inst_16888);

(statearr_16926_17950[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16893 === (10))){
var inst_16853 = (state_16892[(8)]);
var inst_16869 = (state_16892[(2)]);
var inst_16870 = (inst_16853 + (1));
var inst_16853__$1 = inst_16870;
var state_16892__$1 = (function (){var statearr_16927 = state_16892;
(statearr_16927[(8)] = inst_16853__$1);

(statearr_16927[(14)] = inst_16869);

return statearr_16927;
})();
var statearr_16928_17951 = state_16892__$1;
(statearr_16928_17951[(2)] = null);

(statearr_16928_17951[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16893 === (8))){
var inst_16874 = (state_16892[(2)]);
var state_16892__$1 = state_16892;
var statearr_16929_17952 = state_16892__$1;
(statearr_16929_17952[(2)] = inst_16874);

(statearr_16929_17952[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__15401__auto__ = null;
var cljs$core$async$state_machine__15401__auto____0 = (function (){
var statearr_16930 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_16930[(0)] = cljs$core$async$state_machine__15401__auto__);

(statearr_16930[(1)] = (1));

return statearr_16930;
});
var cljs$core$async$state_machine__15401__auto____1 = (function (state_16892){
while(true){
var ret_value__15402__auto__ = (function (){try{while(true){
var result__15403__auto__ = switch__15400__auto__(state_16892);
if(cljs.core.keyword_identical_QMARK_(result__15403__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__15403__auto__;
}
break;
}
}catch (e16931){var ex__15404__auto__ = e16931;
var statearr_16932_17957 = state_16892;
(statearr_16932_17957[(2)] = ex__15404__auto__);


if(cljs.core.seq((state_16892[(4)]))){
var statearr_16933_17958 = state_16892;
(statearr_16933_17958[(1)] = cljs.core.first((state_16892[(4)])));

} else {
throw ex__15404__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__15402__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__17960 = state_16892;
state_16892 = G__17960;
continue;
} else {
return ret_value__15402__auto__;
}
break;
}
});
cljs$core$async$state_machine__15401__auto__ = function(state_16892){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__15401__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__15401__auto____1.call(this,state_16892);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__15401__auto____0;
cljs$core$async$state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__15401__auto____1;
return cljs$core$async$state_machine__15401__auto__;
})()
})();
var state__15646__auto__ = (function (){var statearr_16934 = f__15645__auto__();
(statearr_16934[(6)] = c__15644__auto___17931);

return statearr_16934;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__15646__auto__);
}));

}

return out;
}));

(cljs.core.async.map.cljs$lang$maxFixedArity = 3);

/**
 * Takes a collection of source channels and returns a channel which
 *   contains all values taken from them. The returned channel will be
 *   unbuffered by default, or a buf-or-n can be supplied. The channel
 *   will close after all the source channels have closed.
 */
cljs.core.async.merge = (function cljs$core$async$merge(var_args){
var G__16938 = arguments.length;
switch (G__16938) {
case 1:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1 = (function (chs){
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2(chs,null);
}));

(cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__15644__auto___17962 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__15645__auto__ = (function (){var switch__15400__auto__ = (function (state_16973){
var state_val_16974 = (state_16973[(1)]);
if((state_val_16974 === (7))){
var inst_16951 = (state_16973[(7)]);
var inst_16952 = (state_16973[(8)]);
var inst_16951__$1 = (state_16973[(2)]);
var inst_16952__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_16951__$1,(0),null);
var inst_16953 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_16951__$1,(1),null);
var inst_16954 = (inst_16952__$1 == null);
var state_16973__$1 = (function (){var statearr_16977 = state_16973;
(statearr_16977[(7)] = inst_16951__$1);

(statearr_16977[(9)] = inst_16953);

(statearr_16977[(8)] = inst_16952__$1);

return statearr_16977;
})();
if(cljs.core.truth_(inst_16954)){
var statearr_16978_17964 = state_16973__$1;
(statearr_16978_17964[(1)] = (8));

} else {
var statearr_16979_17965 = state_16973__$1;
(statearr_16979_17965[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16974 === (1))){
var inst_16941 = cljs.core.vec(chs);
var inst_16942 = inst_16941;
var state_16973__$1 = (function (){var statearr_16980 = state_16973;
(statearr_16980[(10)] = inst_16942);

return statearr_16980;
})();
var statearr_16981_17966 = state_16973__$1;
(statearr_16981_17966[(2)] = null);

(statearr_16981_17966[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16974 === (4))){
var inst_16942 = (state_16973[(10)]);
var state_16973__$1 = state_16973;
return cljs.core.async.ioc_alts_BANG_(state_16973__$1,(7),inst_16942);
} else {
if((state_val_16974 === (6))){
var inst_16968 = (state_16973[(2)]);
var state_16973__$1 = state_16973;
var statearr_16982_17967 = state_16973__$1;
(statearr_16982_17967[(2)] = inst_16968);

(statearr_16982_17967[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16974 === (3))){
var inst_16970 = (state_16973[(2)]);
var state_16973__$1 = state_16973;
return cljs.core.async.impl.ioc_helpers.return_chan(state_16973__$1,inst_16970);
} else {
if((state_val_16974 === (2))){
var inst_16942 = (state_16973[(10)]);
var inst_16944 = cljs.core.count(inst_16942);
var inst_16945 = (inst_16944 > (0));
var state_16973__$1 = state_16973;
if(cljs.core.truth_(inst_16945)){
var statearr_16984_17972 = state_16973__$1;
(statearr_16984_17972[(1)] = (4));

} else {
var statearr_16985_17973 = state_16973__$1;
(statearr_16985_17973[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16974 === (11))){
var inst_16942 = (state_16973[(10)]);
var inst_16961 = (state_16973[(2)]);
var tmp16983 = inst_16942;
var inst_16942__$1 = tmp16983;
var state_16973__$1 = (function (){var statearr_16986 = state_16973;
(statearr_16986[(11)] = inst_16961);

(statearr_16986[(10)] = inst_16942__$1);

return statearr_16986;
})();
var statearr_16987_17974 = state_16973__$1;
(statearr_16987_17974[(2)] = null);

(statearr_16987_17974[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16974 === (9))){
var inst_16952 = (state_16973[(8)]);
var state_16973__$1 = state_16973;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_16973__$1,(11),out,inst_16952);
} else {
if((state_val_16974 === (5))){
var inst_16966 = cljs.core.async.close_BANG_(out);
var state_16973__$1 = state_16973;
var statearr_16989_17975 = state_16973__$1;
(statearr_16989_17975[(2)] = inst_16966);

(statearr_16989_17975[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16974 === (10))){
var inst_16964 = (state_16973[(2)]);
var state_16973__$1 = state_16973;
var statearr_16990_17976 = state_16973__$1;
(statearr_16990_17976[(2)] = inst_16964);

(statearr_16990_17976[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_16974 === (8))){
var inst_16951 = (state_16973[(7)]);
var inst_16942 = (state_16973[(10)]);
var inst_16953 = (state_16973[(9)]);
var inst_16952 = (state_16973[(8)]);
var inst_16956 = (function (){var cs = inst_16942;
var vec__16947 = inst_16951;
var v = inst_16952;
var c = inst_16953;
return (function (p1__16935_SHARP_){
return cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(c,p1__16935_SHARP_);
});
})();
var inst_16957 = cljs.core.filterv(inst_16956,inst_16942);
var inst_16942__$1 = inst_16957;
var state_16973__$1 = (function (){var statearr_16992 = state_16973;
(statearr_16992[(10)] = inst_16942__$1);

return statearr_16992;
})();
var statearr_16993_17977 = state_16973__$1;
(statearr_16993_17977[(2)] = null);

(statearr_16993_17977[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__15401__auto__ = null;
var cljs$core$async$state_machine__15401__auto____0 = (function (){
var statearr_16994 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_16994[(0)] = cljs$core$async$state_machine__15401__auto__);

(statearr_16994[(1)] = (1));

return statearr_16994;
});
var cljs$core$async$state_machine__15401__auto____1 = (function (state_16973){
while(true){
var ret_value__15402__auto__ = (function (){try{while(true){
var result__15403__auto__ = switch__15400__auto__(state_16973);
if(cljs.core.keyword_identical_QMARK_(result__15403__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__15403__auto__;
}
break;
}
}catch (e16996){var ex__15404__auto__ = e16996;
var statearr_16997_17978 = state_16973;
(statearr_16997_17978[(2)] = ex__15404__auto__);


if(cljs.core.seq((state_16973[(4)]))){
var statearr_16998_17979 = state_16973;
(statearr_16998_17979[(1)] = cljs.core.first((state_16973[(4)])));

} else {
throw ex__15404__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__15402__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__17980 = state_16973;
state_16973 = G__17980;
continue;
} else {
return ret_value__15402__auto__;
}
break;
}
});
cljs$core$async$state_machine__15401__auto__ = function(state_16973){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__15401__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__15401__auto____1.call(this,state_16973);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__15401__auto____0;
cljs$core$async$state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__15401__auto____1;
return cljs$core$async$state_machine__15401__auto__;
})()
})();
var state__15646__auto__ = (function (){var statearr_16999 = f__15645__auto__();
(statearr_16999[(6)] = c__15644__auto___17962);

return statearr_16999;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__15646__auto__);
}));


return out;
}));

(cljs.core.async.merge.cljs$lang$maxFixedArity = 2);

/**
 * Returns a channel containing the single (collection) result of the
 *   items taken from the channel conjoined to the supplied
 *   collection. ch must close before into produces a result.
 */
cljs.core.async.into = (function cljs$core$async$into(coll,ch){
return cljs.core.async.reduce(cljs.core.conj,coll,ch);
});
/**
 * Returns a channel that will return, at most, n items from ch. After n items
 * have been returned, or ch has been closed, the return chanel will close.
 * 
 *   The output channel is unbuffered by default, unless buf-or-n is given.
 */
cljs.core.async.take = (function cljs$core$async$take(var_args){
var G__17002 = arguments.length;
switch (G__17002) {
case 2:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.take.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3(n,ch,null);
}));

(cljs.core.async.take.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__15644__auto___17982 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__15645__auto__ = (function (){var switch__15400__auto__ = (function (state_17037){
var state_val_17038 = (state_17037[(1)]);
if((state_val_17038 === (7))){
var inst_17008 = (state_17037[(7)]);
var inst_17008__$1 = (state_17037[(2)]);
var inst_17009 = (inst_17008__$1 == null);
var inst_17010 = cljs.core.not(inst_17009);
var state_17037__$1 = (function (){var statearr_17046 = state_17037;
(statearr_17046[(7)] = inst_17008__$1);

return statearr_17046;
})();
if(inst_17010){
var statearr_17048_17983 = state_17037__$1;
(statearr_17048_17983[(1)] = (8));

} else {
var statearr_17049_17984 = state_17037__$1;
(statearr_17049_17984[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17038 === (1))){
var inst_17003 = (0);
var state_17037__$1 = (function (){var statearr_17050 = state_17037;
(statearr_17050[(8)] = inst_17003);

return statearr_17050;
})();
var statearr_17051_17986 = state_17037__$1;
(statearr_17051_17986[(2)] = null);

(statearr_17051_17986[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17038 === (4))){
var state_17037__$1 = state_17037;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_17037__$1,(7),ch);
} else {
if((state_val_17038 === (6))){
var inst_17032 = (state_17037[(2)]);
var state_17037__$1 = state_17037;
var statearr_17058_17987 = state_17037__$1;
(statearr_17058_17987[(2)] = inst_17032);

(statearr_17058_17987[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17038 === (3))){
var inst_17034 = (state_17037[(2)]);
var inst_17035 = cljs.core.async.close_BANG_(out);
var state_17037__$1 = (function (){var statearr_17060 = state_17037;
(statearr_17060[(9)] = inst_17034);

return statearr_17060;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_17037__$1,inst_17035);
} else {
if((state_val_17038 === (2))){
var inst_17003 = (state_17037[(8)]);
var inst_17005 = (inst_17003 < n);
var state_17037__$1 = state_17037;
if(cljs.core.truth_(inst_17005)){
var statearr_17063_17991 = state_17037__$1;
(statearr_17063_17991[(1)] = (4));

} else {
var statearr_17066_17992 = state_17037__$1;
(statearr_17066_17992[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17038 === (11))){
var inst_17003 = (state_17037[(8)]);
var inst_17013 = (state_17037[(2)]);
var inst_17014 = (inst_17003 + (1));
var inst_17003__$1 = inst_17014;
var state_17037__$1 = (function (){var statearr_17069 = state_17037;
(statearr_17069[(8)] = inst_17003__$1);

(statearr_17069[(10)] = inst_17013);

return statearr_17069;
})();
var statearr_17070_17993 = state_17037__$1;
(statearr_17070_17993[(2)] = null);

(statearr_17070_17993[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17038 === (9))){
var state_17037__$1 = state_17037;
var statearr_17071_17994 = state_17037__$1;
(statearr_17071_17994[(2)] = null);

(statearr_17071_17994[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17038 === (5))){
var state_17037__$1 = state_17037;
var statearr_17072_17995 = state_17037__$1;
(statearr_17072_17995[(2)] = null);

(statearr_17072_17995[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17038 === (10))){
var inst_17028 = (state_17037[(2)]);
var state_17037__$1 = state_17037;
var statearr_17074_17996 = state_17037__$1;
(statearr_17074_17996[(2)] = inst_17028);

(statearr_17074_17996[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17038 === (8))){
var inst_17008 = (state_17037[(7)]);
var state_17037__$1 = state_17037;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_17037__$1,(11),out,inst_17008);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__15401__auto__ = null;
var cljs$core$async$state_machine__15401__auto____0 = (function (){
var statearr_17079 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_17079[(0)] = cljs$core$async$state_machine__15401__auto__);

(statearr_17079[(1)] = (1));

return statearr_17079;
});
var cljs$core$async$state_machine__15401__auto____1 = (function (state_17037){
while(true){
var ret_value__15402__auto__ = (function (){try{while(true){
var result__15403__auto__ = switch__15400__auto__(state_17037);
if(cljs.core.keyword_identical_QMARK_(result__15403__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__15403__auto__;
}
break;
}
}catch (e17081){var ex__15404__auto__ = e17081;
var statearr_17082_17997 = state_17037;
(statearr_17082_17997[(2)] = ex__15404__auto__);


if(cljs.core.seq((state_17037[(4)]))){
var statearr_17083_17998 = state_17037;
(statearr_17083_17998[(1)] = cljs.core.first((state_17037[(4)])));

} else {
throw ex__15404__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__15402__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__17999 = state_17037;
state_17037 = G__17999;
continue;
} else {
return ret_value__15402__auto__;
}
break;
}
});
cljs$core$async$state_machine__15401__auto__ = function(state_17037){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__15401__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__15401__auto____1.call(this,state_17037);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__15401__auto____0;
cljs$core$async$state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__15401__auto____1;
return cljs$core$async$state_machine__15401__auto__;
})()
})();
var state__15646__auto__ = (function (){var statearr_17088 = f__15645__auto__();
(statearr_17088[(6)] = c__15644__auto___17982);

return statearr_17088;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__15646__auto__);
}));


return out;
}));

(cljs.core.async.take.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_LT_ = (function cljs$core$async$map_LT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async17095 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async17095 = (function (f,ch,meta17096){
this.f = f;
this.ch = ch;
this.meta17096 = meta17096;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async17095.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_17097,meta17096__$1){
var self__ = this;
var _17097__$1 = this;
return (new cljs.core.async.t_cljs$core$async17095(self__.f,self__.ch,meta17096__$1));
}));

(cljs.core.async.t_cljs$core$async17095.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_17097){
var self__ = this;
var _17097__$1 = this;
return self__.meta17096;
}));

(cljs.core.async.t_cljs$core$async17095.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async17095.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async17095.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async17095.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async17095.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_(self__.ch,(function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async17099 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async17099 = (function (f,ch,meta17096,_,fn1,meta17100){
this.f = f;
this.ch = ch;
this.meta17096 = meta17096;
this._ = _;
this.fn1 = fn1;
this.meta17100 = meta17100;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async17099.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_17101,meta17100__$1){
var self__ = this;
var _17101__$1 = this;
return (new cljs.core.async.t_cljs$core$async17099(self__.f,self__.ch,self__.meta17096,self__._,self__.fn1,meta17100__$1));
}));

(cljs.core.async.t_cljs$core$async17099.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_17101){
var self__ = this;
var _17101__$1 = this;
return self__.meta17100;
}));

(cljs.core.async.t_cljs$core$async17099.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async17099.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.fn1);
}));

(cljs.core.async.t_cljs$core$async17099.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async17099.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit(self__.fn1);
return (function (p1__17090_SHARP_){
var G__17111 = (((p1__17090_SHARP_ == null))?null:(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(p1__17090_SHARP_) : self__.f.call(null,p1__17090_SHARP_)));
return (f1.cljs$core$IFn$_invoke$arity$1 ? f1.cljs$core$IFn$_invoke$arity$1(G__17111) : f1.call(null,G__17111));
});
}));

(cljs.core.async.t_cljs$core$async17099.getBasis = (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta17096","meta17096",-1215561985,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_","_",-1201019570,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"tag","tag",-1290361223),new cljs.core.Symbol("cljs.core.async","t_cljs$core$async17095","cljs.core.async/t_cljs$core$async17095",2117220394,null)], null)),new cljs.core.Symbol(null,"fn1","fn1",895834444,null),new cljs.core.Symbol(null,"meta17100","meta17100",-814962217,null)], null);
}));

(cljs.core.async.t_cljs$core$async17099.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async17099.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async17099");

(cljs.core.async.t_cljs$core$async17099.cljs$lang$ctorPrWriter = (function (this__5330__auto__,writer__5331__auto__,opt__5332__auto__){
return cljs.core._write(writer__5331__auto__,"cljs.core.async/t_cljs$core$async17099");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async17099.
 */
cljs.core.async.__GT_t_cljs$core$async17099 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async17099(f__$1,ch__$1,meta17096__$1,___$2,fn1__$1,meta17100){
return (new cljs.core.async.t_cljs$core$async17099(f__$1,ch__$1,meta17096__$1,___$2,fn1__$1,meta17100));
});

}

return (new cljs.core.async.t_cljs$core$async17099(self__.f,self__.ch,self__.meta17096,___$1,fn1,cljs.core.PersistentArrayMap.EMPTY));
})()
);
if(cljs.core.truth_((function (){var and__5043__auto__ = ret;
if(cljs.core.truth_(and__5043__auto__)){
return (!((cljs.core.deref(ret) == null)));
} else {
return and__5043__auto__;
}
})())){
return cljs.core.async.impl.channels.box((function (){var G__17112 = cljs.core.deref(ret);
return (self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(G__17112) : self__.f.call(null,G__17112));
})());
} else {
return ret;
}
}));

(cljs.core.async.t_cljs$core$async17095.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async17095.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
}));

(cljs.core.async.t_cljs$core$async17095.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta17096","meta17096",-1215561985,null)], null);
}));

(cljs.core.async.t_cljs$core$async17095.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async17095.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async17095");

(cljs.core.async.t_cljs$core$async17095.cljs$lang$ctorPrWriter = (function (this__5330__auto__,writer__5331__auto__,opt__5332__auto__){
return cljs.core._write(writer__5331__auto__,"cljs.core.async/t_cljs$core$async17095");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async17095.
 */
cljs.core.async.__GT_t_cljs$core$async17095 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async17095(f__$1,ch__$1,meta17096){
return (new cljs.core.async.t_cljs$core$async17095(f__$1,ch__$1,meta17096));
});

}

return (new cljs.core.async.t_cljs$core$async17095(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_GT_ = (function cljs$core$async$map_GT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async17113 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async17113 = (function (f,ch,meta17114){
this.f = f;
this.ch = ch;
this.meta17114 = meta17114;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async17113.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_17115,meta17114__$1){
var self__ = this;
var _17115__$1 = this;
return (new cljs.core.async.t_cljs$core$async17113(self__.f,self__.ch,meta17114__$1));
}));

(cljs.core.async.t_cljs$core$async17113.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_17115){
var self__ = this;
var _17115__$1 = this;
return self__.meta17114;
}));

(cljs.core.async.t_cljs$core$async17113.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async17113.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async17113.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async17113.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
}));

(cljs.core.async.t_cljs$core$async17113.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async17113.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(val) : self__.f.call(null,val)),fn1);
}));

(cljs.core.async.t_cljs$core$async17113.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta17114","meta17114",-1542682216,null)], null);
}));

(cljs.core.async.t_cljs$core$async17113.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async17113.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async17113");

(cljs.core.async.t_cljs$core$async17113.cljs$lang$ctorPrWriter = (function (this__5330__auto__,writer__5331__auto__,opt__5332__auto__){
return cljs.core._write(writer__5331__auto__,"cljs.core.async/t_cljs$core$async17113");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async17113.
 */
cljs.core.async.__GT_t_cljs$core$async17113 = (function cljs$core$async$map_GT__$___GT_t_cljs$core$async17113(f__$1,ch__$1,meta17114){
return (new cljs.core.async.t_cljs$core$async17113(f__$1,ch__$1,meta17114));
});

}

return (new cljs.core.async.t_cljs$core$async17113(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_GT_ = (function cljs$core$async$filter_GT_(p,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async17127 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async17127 = (function (p,ch,meta17128){
this.p = p;
this.ch = ch;
this.meta17128 = meta17128;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async17127.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_17129,meta17128__$1){
var self__ = this;
var _17129__$1 = this;
return (new cljs.core.async.t_cljs$core$async17127(self__.p,self__.ch,meta17128__$1));
}));

(cljs.core.async.t_cljs$core$async17127.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_17129){
var self__ = this;
var _17129__$1 = this;
return self__.meta17128;
}));

(cljs.core.async.t_cljs$core$async17127.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async17127.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async17127.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async17127.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async17127.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
}));

(cljs.core.async.t_cljs$core$async17127.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async17127.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.p.cljs$core$IFn$_invoke$arity$1 ? self__.p.cljs$core$IFn$_invoke$arity$1(val) : self__.p.call(null,val)))){
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box(cljs.core.not(cljs.core.async.impl.protocols.closed_QMARK_(self__.ch)));
}
}));

(cljs.core.async.t_cljs$core$async17127.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta17128","meta17128",1955971320,null)], null);
}));

(cljs.core.async.t_cljs$core$async17127.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async17127.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async17127");

(cljs.core.async.t_cljs$core$async17127.cljs$lang$ctorPrWriter = (function (this__5330__auto__,writer__5331__auto__,opt__5332__auto__){
return cljs.core._write(writer__5331__auto__,"cljs.core.async/t_cljs$core$async17127");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async17127.
 */
cljs.core.async.__GT_t_cljs$core$async17127 = (function cljs$core$async$filter_GT__$___GT_t_cljs$core$async17127(p__$1,ch__$1,meta17128){
return (new cljs.core.async.t_cljs$core$async17127(p__$1,ch__$1,meta17128));
});

}

return (new cljs.core.async.t_cljs$core$async17127(p,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_GT_ = (function cljs$core$async$remove_GT_(p,ch){
return cljs.core.async.filter_GT_(cljs.core.complement(p),ch);
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_LT_ = (function cljs$core$async$filter_LT_(var_args){
var G__17131 = arguments.length;
switch (G__17131) {
case 2:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
}));

(cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__15644__auto___18012 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__15645__auto__ = (function (){var switch__15400__auto__ = (function (state_17152){
var state_val_17153 = (state_17152[(1)]);
if((state_val_17153 === (7))){
var inst_17148 = (state_17152[(2)]);
var state_17152__$1 = state_17152;
var statearr_17154_18013 = state_17152__$1;
(statearr_17154_18013[(2)] = inst_17148);

(statearr_17154_18013[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17153 === (1))){
var state_17152__$1 = state_17152;
var statearr_17155_18014 = state_17152__$1;
(statearr_17155_18014[(2)] = null);

(statearr_17155_18014[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17153 === (4))){
var inst_17134 = (state_17152[(7)]);
var inst_17134__$1 = (state_17152[(2)]);
var inst_17135 = (inst_17134__$1 == null);
var state_17152__$1 = (function (){var statearr_17156 = state_17152;
(statearr_17156[(7)] = inst_17134__$1);

return statearr_17156;
})();
if(cljs.core.truth_(inst_17135)){
var statearr_17157_18030 = state_17152__$1;
(statearr_17157_18030[(1)] = (5));

} else {
var statearr_17158_18031 = state_17152__$1;
(statearr_17158_18031[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17153 === (6))){
var inst_17134 = (state_17152[(7)]);
var inst_17139 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_17134) : p.call(null,inst_17134));
var state_17152__$1 = state_17152;
if(cljs.core.truth_(inst_17139)){
var statearr_17159_18032 = state_17152__$1;
(statearr_17159_18032[(1)] = (8));

} else {
var statearr_17160_18033 = state_17152__$1;
(statearr_17160_18033[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17153 === (3))){
var inst_17150 = (state_17152[(2)]);
var state_17152__$1 = state_17152;
return cljs.core.async.impl.ioc_helpers.return_chan(state_17152__$1,inst_17150);
} else {
if((state_val_17153 === (2))){
var state_17152__$1 = state_17152;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_17152__$1,(4),ch);
} else {
if((state_val_17153 === (11))){
var inst_17142 = (state_17152[(2)]);
var state_17152__$1 = state_17152;
var statearr_17161_18034 = state_17152__$1;
(statearr_17161_18034[(2)] = inst_17142);

(statearr_17161_18034[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17153 === (9))){
var state_17152__$1 = state_17152;
var statearr_17162_18035 = state_17152__$1;
(statearr_17162_18035[(2)] = null);

(statearr_17162_18035[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17153 === (5))){
var inst_17137 = cljs.core.async.close_BANG_(out);
var state_17152__$1 = state_17152;
var statearr_17163_18036 = state_17152__$1;
(statearr_17163_18036[(2)] = inst_17137);

(statearr_17163_18036[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17153 === (10))){
var inst_17145 = (state_17152[(2)]);
var state_17152__$1 = (function (){var statearr_17164 = state_17152;
(statearr_17164[(8)] = inst_17145);

return statearr_17164;
})();
var statearr_17165_18037 = state_17152__$1;
(statearr_17165_18037[(2)] = null);

(statearr_17165_18037[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17153 === (8))){
var inst_17134 = (state_17152[(7)]);
var state_17152__$1 = state_17152;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_17152__$1,(11),out,inst_17134);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__15401__auto__ = null;
var cljs$core$async$state_machine__15401__auto____0 = (function (){
var statearr_17166 = [null,null,null,null,null,null,null,null,null];
(statearr_17166[(0)] = cljs$core$async$state_machine__15401__auto__);

(statearr_17166[(1)] = (1));

return statearr_17166;
});
var cljs$core$async$state_machine__15401__auto____1 = (function (state_17152){
while(true){
var ret_value__15402__auto__ = (function (){try{while(true){
var result__15403__auto__ = switch__15400__auto__(state_17152);
if(cljs.core.keyword_identical_QMARK_(result__15403__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__15403__auto__;
}
break;
}
}catch (e17167){var ex__15404__auto__ = e17167;
var statearr_17168_18038 = state_17152;
(statearr_17168_18038[(2)] = ex__15404__auto__);


if(cljs.core.seq((state_17152[(4)]))){
var statearr_17169_18039 = state_17152;
(statearr_17169_18039[(1)] = cljs.core.first((state_17152[(4)])));

} else {
throw ex__15404__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__15402__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__18040 = state_17152;
state_17152 = G__18040;
continue;
} else {
return ret_value__15402__auto__;
}
break;
}
});
cljs$core$async$state_machine__15401__auto__ = function(state_17152){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__15401__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__15401__auto____1.call(this,state_17152);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__15401__auto____0;
cljs$core$async$state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__15401__auto____1;
return cljs$core$async$state_machine__15401__auto__;
})()
})();
var state__15646__auto__ = (function (){var statearr_17170 = f__15645__auto__();
(statearr_17170[(6)] = c__15644__auto___18012);

return statearr_17170;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__15646__auto__);
}));


return out;
}));

(cljs.core.async.filter_LT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_LT_ = (function cljs$core$async$remove_LT_(var_args){
var G__17172 = arguments.length;
switch (G__17172) {
case 2:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
}));

(cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(cljs.core.complement(p),ch,buf_or_n);
}));

(cljs.core.async.remove_LT_.cljs$lang$maxFixedArity = 3);

cljs.core.async.mapcat_STAR_ = (function cljs$core$async$mapcat_STAR_(f,in$,out){
var c__15644__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__15645__auto__ = (function (){var switch__15400__auto__ = (function (state_17234){
var state_val_17235 = (state_17234[(1)]);
if((state_val_17235 === (7))){
var inst_17230 = (state_17234[(2)]);
var state_17234__$1 = state_17234;
var statearr_17236_18045 = state_17234__$1;
(statearr_17236_18045[(2)] = inst_17230);

(statearr_17236_18045[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17235 === (20))){
var inst_17200 = (state_17234[(7)]);
var inst_17211 = (state_17234[(2)]);
var inst_17212 = cljs.core.next(inst_17200);
var inst_17186 = inst_17212;
var inst_17187 = null;
var inst_17188 = (0);
var inst_17189 = (0);
var state_17234__$1 = (function (){var statearr_17237 = state_17234;
(statearr_17237[(8)] = inst_17187);

(statearr_17237[(9)] = inst_17186);

(statearr_17237[(10)] = inst_17188);

(statearr_17237[(11)] = inst_17211);

(statearr_17237[(12)] = inst_17189);

return statearr_17237;
})();
var statearr_17238_18050 = state_17234__$1;
(statearr_17238_18050[(2)] = null);

(statearr_17238_18050[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17235 === (1))){
var state_17234__$1 = state_17234;
var statearr_17239_18054 = state_17234__$1;
(statearr_17239_18054[(2)] = null);

(statearr_17239_18054[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17235 === (4))){
var inst_17175 = (state_17234[(13)]);
var inst_17175__$1 = (state_17234[(2)]);
var inst_17176 = (inst_17175__$1 == null);
var state_17234__$1 = (function (){var statearr_17240 = state_17234;
(statearr_17240[(13)] = inst_17175__$1);

return statearr_17240;
})();
if(cljs.core.truth_(inst_17176)){
var statearr_17241_18055 = state_17234__$1;
(statearr_17241_18055[(1)] = (5));

} else {
var statearr_17242_18056 = state_17234__$1;
(statearr_17242_18056[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17235 === (15))){
var state_17234__$1 = state_17234;
var statearr_17246_18057 = state_17234__$1;
(statearr_17246_18057[(2)] = null);

(statearr_17246_18057[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17235 === (21))){
var state_17234__$1 = state_17234;
var statearr_17247_18058 = state_17234__$1;
(statearr_17247_18058[(2)] = null);

(statearr_17247_18058[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17235 === (13))){
var inst_17187 = (state_17234[(8)]);
var inst_17186 = (state_17234[(9)]);
var inst_17188 = (state_17234[(10)]);
var inst_17189 = (state_17234[(12)]);
var inst_17196 = (state_17234[(2)]);
var inst_17197 = (inst_17189 + (1));
var tmp17243 = inst_17187;
var tmp17244 = inst_17186;
var tmp17245 = inst_17188;
var inst_17186__$1 = tmp17244;
var inst_17187__$1 = tmp17243;
var inst_17188__$1 = tmp17245;
var inst_17189__$1 = inst_17197;
var state_17234__$1 = (function (){var statearr_17248 = state_17234;
(statearr_17248[(8)] = inst_17187__$1);

(statearr_17248[(14)] = inst_17196);

(statearr_17248[(9)] = inst_17186__$1);

(statearr_17248[(10)] = inst_17188__$1);

(statearr_17248[(12)] = inst_17189__$1);

return statearr_17248;
})();
var statearr_17249_18062 = state_17234__$1;
(statearr_17249_18062[(2)] = null);

(statearr_17249_18062[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17235 === (22))){
var state_17234__$1 = state_17234;
var statearr_17250_18063 = state_17234__$1;
(statearr_17250_18063[(2)] = null);

(statearr_17250_18063[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17235 === (6))){
var inst_17175 = (state_17234[(13)]);
var inst_17184 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_17175) : f.call(null,inst_17175));
var inst_17185 = cljs.core.seq(inst_17184);
var inst_17186 = inst_17185;
var inst_17187 = null;
var inst_17188 = (0);
var inst_17189 = (0);
var state_17234__$1 = (function (){var statearr_17251 = state_17234;
(statearr_17251[(8)] = inst_17187);

(statearr_17251[(9)] = inst_17186);

(statearr_17251[(10)] = inst_17188);

(statearr_17251[(12)] = inst_17189);

return statearr_17251;
})();
var statearr_17252_18064 = state_17234__$1;
(statearr_17252_18064[(2)] = null);

(statearr_17252_18064[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17235 === (17))){
var inst_17200 = (state_17234[(7)]);
var inst_17204 = cljs.core.chunk_first(inst_17200);
var inst_17205 = cljs.core.chunk_rest(inst_17200);
var inst_17206 = cljs.core.count(inst_17204);
var inst_17186 = inst_17205;
var inst_17187 = inst_17204;
var inst_17188 = inst_17206;
var inst_17189 = (0);
var state_17234__$1 = (function (){var statearr_17253 = state_17234;
(statearr_17253[(8)] = inst_17187);

(statearr_17253[(9)] = inst_17186);

(statearr_17253[(10)] = inst_17188);

(statearr_17253[(12)] = inst_17189);

return statearr_17253;
})();
var statearr_17254_18068 = state_17234__$1;
(statearr_17254_18068[(2)] = null);

(statearr_17254_18068[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17235 === (3))){
var inst_17232 = (state_17234[(2)]);
var state_17234__$1 = state_17234;
return cljs.core.async.impl.ioc_helpers.return_chan(state_17234__$1,inst_17232);
} else {
if((state_val_17235 === (12))){
var inst_17220 = (state_17234[(2)]);
var state_17234__$1 = state_17234;
var statearr_17255_18069 = state_17234__$1;
(statearr_17255_18069[(2)] = inst_17220);

(statearr_17255_18069[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17235 === (2))){
var state_17234__$1 = state_17234;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_17234__$1,(4),in$);
} else {
if((state_val_17235 === (23))){
var inst_17228 = (state_17234[(2)]);
var state_17234__$1 = state_17234;
var statearr_17256_18089 = state_17234__$1;
(statearr_17256_18089[(2)] = inst_17228);

(statearr_17256_18089[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17235 === (19))){
var inst_17215 = (state_17234[(2)]);
var state_17234__$1 = state_17234;
var statearr_17257_18090 = state_17234__$1;
(statearr_17257_18090[(2)] = inst_17215);

(statearr_17257_18090[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17235 === (11))){
var inst_17186 = (state_17234[(9)]);
var inst_17200 = (state_17234[(7)]);
var inst_17200__$1 = cljs.core.seq(inst_17186);
var state_17234__$1 = (function (){var statearr_17258 = state_17234;
(statearr_17258[(7)] = inst_17200__$1);

return statearr_17258;
})();
if(inst_17200__$1){
var statearr_17259_18091 = state_17234__$1;
(statearr_17259_18091[(1)] = (14));

} else {
var statearr_17260_18092 = state_17234__$1;
(statearr_17260_18092[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17235 === (9))){
var inst_17222 = (state_17234[(2)]);
var inst_17223 = cljs.core.async.impl.protocols.closed_QMARK_(out);
var state_17234__$1 = (function (){var statearr_17261 = state_17234;
(statearr_17261[(15)] = inst_17222);

return statearr_17261;
})();
if(cljs.core.truth_(inst_17223)){
var statearr_17262_18093 = state_17234__$1;
(statearr_17262_18093[(1)] = (21));

} else {
var statearr_17263_18094 = state_17234__$1;
(statearr_17263_18094[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17235 === (5))){
var inst_17178 = cljs.core.async.close_BANG_(out);
var state_17234__$1 = state_17234;
var statearr_17264_18095 = state_17234__$1;
(statearr_17264_18095[(2)] = inst_17178);

(statearr_17264_18095[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17235 === (14))){
var inst_17200 = (state_17234[(7)]);
var inst_17202 = cljs.core.chunked_seq_QMARK_(inst_17200);
var state_17234__$1 = state_17234;
if(inst_17202){
var statearr_17265_18096 = state_17234__$1;
(statearr_17265_18096[(1)] = (17));

} else {
var statearr_17266_18097 = state_17234__$1;
(statearr_17266_18097[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17235 === (16))){
var inst_17218 = (state_17234[(2)]);
var state_17234__$1 = state_17234;
var statearr_17267_18098 = state_17234__$1;
(statearr_17267_18098[(2)] = inst_17218);

(statearr_17267_18098[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17235 === (10))){
var inst_17187 = (state_17234[(8)]);
var inst_17189 = (state_17234[(12)]);
var inst_17194 = cljs.core._nth(inst_17187,inst_17189);
var state_17234__$1 = state_17234;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_17234__$1,(13),out,inst_17194);
} else {
if((state_val_17235 === (18))){
var inst_17200 = (state_17234[(7)]);
var inst_17209 = cljs.core.first(inst_17200);
var state_17234__$1 = state_17234;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_17234__$1,(20),out,inst_17209);
} else {
if((state_val_17235 === (8))){
var inst_17188 = (state_17234[(10)]);
var inst_17189 = (state_17234[(12)]);
var inst_17191 = (inst_17189 < inst_17188);
var inst_17192 = inst_17191;
var state_17234__$1 = state_17234;
if(cljs.core.truth_(inst_17192)){
var statearr_17268_18105 = state_17234__$1;
(statearr_17268_18105[(1)] = (10));

} else {
var statearr_17269_18106 = state_17234__$1;
(statearr_17269_18106[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mapcat_STAR__$_state_machine__15401__auto__ = null;
var cljs$core$async$mapcat_STAR__$_state_machine__15401__auto____0 = (function (){
var statearr_17270 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_17270[(0)] = cljs$core$async$mapcat_STAR__$_state_machine__15401__auto__);

(statearr_17270[(1)] = (1));

return statearr_17270;
});
var cljs$core$async$mapcat_STAR__$_state_machine__15401__auto____1 = (function (state_17234){
while(true){
var ret_value__15402__auto__ = (function (){try{while(true){
var result__15403__auto__ = switch__15400__auto__(state_17234);
if(cljs.core.keyword_identical_QMARK_(result__15403__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__15403__auto__;
}
break;
}
}catch (e17271){var ex__15404__auto__ = e17271;
var statearr_17272_18107 = state_17234;
(statearr_17272_18107[(2)] = ex__15404__auto__);


if(cljs.core.seq((state_17234[(4)]))){
var statearr_17273_18108 = state_17234;
(statearr_17273_18108[(1)] = cljs.core.first((state_17234[(4)])));

} else {
throw ex__15404__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__15402__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__18109 = state_17234;
state_17234 = G__18109;
continue;
} else {
return ret_value__15402__auto__;
}
break;
}
});
cljs$core$async$mapcat_STAR__$_state_machine__15401__auto__ = function(state_17234){
switch(arguments.length){
case 0:
return cljs$core$async$mapcat_STAR__$_state_machine__15401__auto____0.call(this);
case 1:
return cljs$core$async$mapcat_STAR__$_state_machine__15401__auto____1.call(this,state_17234);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mapcat_STAR__$_state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mapcat_STAR__$_state_machine__15401__auto____0;
cljs$core$async$mapcat_STAR__$_state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mapcat_STAR__$_state_machine__15401__auto____1;
return cljs$core$async$mapcat_STAR__$_state_machine__15401__auto__;
})()
})();
var state__15646__auto__ = (function (){var statearr_17274 = f__15645__auto__();
(statearr_17274[(6)] = c__15644__auto__);

return statearr_17274;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__15646__auto__);
}));

return c__15644__auto__;
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_LT_ = (function cljs$core$async$mapcat_LT_(var_args){
var G__17276 = arguments.length;
switch (G__17276) {
case 2:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = (function (f,in$){
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3(f,in$,null);
}));

(cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return out;
}));

(cljs.core.async.mapcat_LT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_GT_ = (function cljs$core$async$mapcat_GT_(var_args){
var G__17278 = arguments.length;
switch (G__17278) {
case 2:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = (function (f,out){
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3(f,out,null);
}));

(cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return in$;
}));

(cljs.core.async.mapcat_GT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.unique = (function cljs$core$async$unique(var_args){
var G__17280 = arguments.length;
switch (G__17280) {
case 1:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1 = (function (ch){
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2(ch,null);
}));

(cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__15644__auto___18113 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__15645__auto__ = (function (){var switch__15400__auto__ = (function (state_17304){
var state_val_17305 = (state_17304[(1)]);
if((state_val_17305 === (7))){
var inst_17299 = (state_17304[(2)]);
var state_17304__$1 = state_17304;
var statearr_17306_18114 = state_17304__$1;
(statearr_17306_18114[(2)] = inst_17299);

(statearr_17306_18114[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17305 === (1))){
var inst_17281 = null;
var state_17304__$1 = (function (){var statearr_17307 = state_17304;
(statearr_17307[(7)] = inst_17281);

return statearr_17307;
})();
var statearr_17308_18115 = state_17304__$1;
(statearr_17308_18115[(2)] = null);

(statearr_17308_18115[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17305 === (4))){
var inst_17284 = (state_17304[(8)]);
var inst_17284__$1 = (state_17304[(2)]);
var inst_17285 = (inst_17284__$1 == null);
var inst_17286 = cljs.core.not(inst_17285);
var state_17304__$1 = (function (){var statearr_17309 = state_17304;
(statearr_17309[(8)] = inst_17284__$1);

return statearr_17309;
})();
if(inst_17286){
var statearr_17310_18116 = state_17304__$1;
(statearr_17310_18116[(1)] = (5));

} else {
var statearr_17311_18117 = state_17304__$1;
(statearr_17311_18117[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17305 === (6))){
var state_17304__$1 = state_17304;
var statearr_17312_18118 = state_17304__$1;
(statearr_17312_18118[(2)] = null);

(statearr_17312_18118[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17305 === (3))){
var inst_17301 = (state_17304[(2)]);
var inst_17302 = cljs.core.async.close_BANG_(out);
var state_17304__$1 = (function (){var statearr_17313 = state_17304;
(statearr_17313[(9)] = inst_17301);

return statearr_17313;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_17304__$1,inst_17302);
} else {
if((state_val_17305 === (2))){
var state_17304__$1 = state_17304;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_17304__$1,(4),ch);
} else {
if((state_val_17305 === (11))){
var inst_17284 = (state_17304[(8)]);
var inst_17293 = (state_17304[(2)]);
var inst_17281 = inst_17284;
var state_17304__$1 = (function (){var statearr_17314 = state_17304;
(statearr_17314[(10)] = inst_17293);

(statearr_17314[(7)] = inst_17281);

return statearr_17314;
})();
var statearr_17315_18119 = state_17304__$1;
(statearr_17315_18119[(2)] = null);

(statearr_17315_18119[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17305 === (9))){
var inst_17284 = (state_17304[(8)]);
var state_17304__$1 = state_17304;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_17304__$1,(11),out,inst_17284);
} else {
if((state_val_17305 === (5))){
var inst_17284 = (state_17304[(8)]);
var inst_17281 = (state_17304[(7)]);
var inst_17288 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_17284,inst_17281);
var state_17304__$1 = state_17304;
if(inst_17288){
var statearr_17317_18120 = state_17304__$1;
(statearr_17317_18120[(1)] = (8));

} else {
var statearr_17318_18121 = state_17304__$1;
(statearr_17318_18121[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17305 === (10))){
var inst_17296 = (state_17304[(2)]);
var state_17304__$1 = state_17304;
var statearr_17319_18122 = state_17304__$1;
(statearr_17319_18122[(2)] = inst_17296);

(statearr_17319_18122[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17305 === (8))){
var inst_17281 = (state_17304[(7)]);
var tmp17316 = inst_17281;
var inst_17281__$1 = tmp17316;
var state_17304__$1 = (function (){var statearr_17320 = state_17304;
(statearr_17320[(7)] = inst_17281__$1);

return statearr_17320;
})();
var statearr_17321_18123 = state_17304__$1;
(statearr_17321_18123[(2)] = null);

(statearr_17321_18123[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__15401__auto__ = null;
var cljs$core$async$state_machine__15401__auto____0 = (function (){
var statearr_17322 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_17322[(0)] = cljs$core$async$state_machine__15401__auto__);

(statearr_17322[(1)] = (1));

return statearr_17322;
});
var cljs$core$async$state_machine__15401__auto____1 = (function (state_17304){
while(true){
var ret_value__15402__auto__ = (function (){try{while(true){
var result__15403__auto__ = switch__15400__auto__(state_17304);
if(cljs.core.keyword_identical_QMARK_(result__15403__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__15403__auto__;
}
break;
}
}catch (e17323){var ex__15404__auto__ = e17323;
var statearr_17324_18124 = state_17304;
(statearr_17324_18124[(2)] = ex__15404__auto__);


if(cljs.core.seq((state_17304[(4)]))){
var statearr_17325_18125 = state_17304;
(statearr_17325_18125[(1)] = cljs.core.first((state_17304[(4)])));

} else {
throw ex__15404__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__15402__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__18126 = state_17304;
state_17304 = G__18126;
continue;
} else {
return ret_value__15402__auto__;
}
break;
}
});
cljs$core$async$state_machine__15401__auto__ = function(state_17304){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__15401__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__15401__auto____1.call(this,state_17304);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__15401__auto____0;
cljs$core$async$state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__15401__auto____1;
return cljs$core$async$state_machine__15401__auto__;
})()
})();
var state__15646__auto__ = (function (){var statearr_17326 = f__15645__auto__();
(statearr_17326[(6)] = c__15644__auto___18113);

return statearr_17326;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__15646__auto__);
}));


return out;
}));

(cljs.core.async.unique.cljs$lang$maxFixedArity = 2);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition = (function cljs$core$async$partition(var_args){
var G__17328 = arguments.length;
switch (G__17328) {
case 2:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3(n,ch,null);
}));

(cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__15644__auto___18128 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__15645__auto__ = (function (){var switch__15400__auto__ = (function (state_17366){
var state_val_17367 = (state_17366[(1)]);
if((state_val_17367 === (7))){
var inst_17362 = (state_17366[(2)]);
var state_17366__$1 = state_17366;
var statearr_17368_18129 = state_17366__$1;
(statearr_17368_18129[(2)] = inst_17362);

(statearr_17368_18129[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17367 === (1))){
var inst_17329 = (new Array(n));
var inst_17330 = inst_17329;
var inst_17331 = (0);
var state_17366__$1 = (function (){var statearr_17369 = state_17366;
(statearr_17369[(7)] = inst_17331);

(statearr_17369[(8)] = inst_17330);

return statearr_17369;
})();
var statearr_17370_18130 = state_17366__$1;
(statearr_17370_18130[(2)] = null);

(statearr_17370_18130[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17367 === (4))){
var inst_17334 = (state_17366[(9)]);
var inst_17334__$1 = (state_17366[(2)]);
var inst_17335 = (inst_17334__$1 == null);
var inst_17336 = cljs.core.not(inst_17335);
var state_17366__$1 = (function (){var statearr_17371 = state_17366;
(statearr_17371[(9)] = inst_17334__$1);

return statearr_17371;
})();
if(inst_17336){
var statearr_17372_18131 = state_17366__$1;
(statearr_17372_18131[(1)] = (5));

} else {
var statearr_17373_18132 = state_17366__$1;
(statearr_17373_18132[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17367 === (15))){
var inst_17356 = (state_17366[(2)]);
var state_17366__$1 = state_17366;
var statearr_17374_18133 = state_17366__$1;
(statearr_17374_18133[(2)] = inst_17356);

(statearr_17374_18133[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17367 === (13))){
var state_17366__$1 = state_17366;
var statearr_17375_18134 = state_17366__$1;
(statearr_17375_18134[(2)] = null);

(statearr_17375_18134[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17367 === (6))){
var inst_17331 = (state_17366[(7)]);
var inst_17352 = (inst_17331 > (0));
var state_17366__$1 = state_17366;
if(cljs.core.truth_(inst_17352)){
var statearr_17376_18135 = state_17366__$1;
(statearr_17376_18135[(1)] = (12));

} else {
var statearr_17377_18136 = state_17366__$1;
(statearr_17377_18136[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17367 === (3))){
var inst_17364 = (state_17366[(2)]);
var state_17366__$1 = state_17366;
return cljs.core.async.impl.ioc_helpers.return_chan(state_17366__$1,inst_17364);
} else {
if((state_val_17367 === (12))){
var inst_17330 = (state_17366[(8)]);
var inst_17354 = cljs.core.vec(inst_17330);
var state_17366__$1 = state_17366;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_17366__$1,(15),out,inst_17354);
} else {
if((state_val_17367 === (2))){
var state_17366__$1 = state_17366;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_17366__$1,(4),ch);
} else {
if((state_val_17367 === (11))){
var inst_17346 = (state_17366[(2)]);
var inst_17347 = (new Array(n));
var inst_17330 = inst_17347;
var inst_17331 = (0);
var state_17366__$1 = (function (){var statearr_17378 = state_17366;
(statearr_17378[(7)] = inst_17331);

(statearr_17378[(8)] = inst_17330);

(statearr_17378[(10)] = inst_17346);

return statearr_17378;
})();
var statearr_17379_18137 = state_17366__$1;
(statearr_17379_18137[(2)] = null);

(statearr_17379_18137[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17367 === (9))){
var inst_17330 = (state_17366[(8)]);
var inst_17344 = cljs.core.vec(inst_17330);
var state_17366__$1 = state_17366;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_17366__$1,(11),out,inst_17344);
} else {
if((state_val_17367 === (5))){
var inst_17331 = (state_17366[(7)]);
var inst_17339 = (state_17366[(11)]);
var inst_17334 = (state_17366[(9)]);
var inst_17330 = (state_17366[(8)]);
var inst_17338 = (inst_17330[inst_17331] = inst_17334);
var inst_17339__$1 = (inst_17331 + (1));
var inst_17340 = (inst_17339__$1 < n);
var state_17366__$1 = (function (){var statearr_17380 = state_17366;
(statearr_17380[(12)] = inst_17338);

(statearr_17380[(11)] = inst_17339__$1);

return statearr_17380;
})();
if(cljs.core.truth_(inst_17340)){
var statearr_17381_18138 = state_17366__$1;
(statearr_17381_18138[(1)] = (8));

} else {
var statearr_17382_18139 = state_17366__$1;
(statearr_17382_18139[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17367 === (14))){
var inst_17359 = (state_17366[(2)]);
var inst_17360 = cljs.core.async.close_BANG_(out);
var state_17366__$1 = (function (){var statearr_17384 = state_17366;
(statearr_17384[(13)] = inst_17359);

return statearr_17384;
})();
var statearr_17385_18140 = state_17366__$1;
(statearr_17385_18140[(2)] = inst_17360);

(statearr_17385_18140[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17367 === (10))){
var inst_17350 = (state_17366[(2)]);
var state_17366__$1 = state_17366;
var statearr_17386_18141 = state_17366__$1;
(statearr_17386_18141[(2)] = inst_17350);

(statearr_17386_18141[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17367 === (8))){
var inst_17339 = (state_17366[(11)]);
var inst_17330 = (state_17366[(8)]);
var tmp17383 = inst_17330;
var inst_17330__$1 = tmp17383;
var inst_17331 = inst_17339;
var state_17366__$1 = (function (){var statearr_17387 = state_17366;
(statearr_17387[(7)] = inst_17331);

(statearr_17387[(8)] = inst_17330__$1);

return statearr_17387;
})();
var statearr_17388_18142 = state_17366__$1;
(statearr_17388_18142[(2)] = null);

(statearr_17388_18142[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__15401__auto__ = null;
var cljs$core$async$state_machine__15401__auto____0 = (function (){
var statearr_17389 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_17389[(0)] = cljs$core$async$state_machine__15401__auto__);

(statearr_17389[(1)] = (1));

return statearr_17389;
});
var cljs$core$async$state_machine__15401__auto____1 = (function (state_17366){
while(true){
var ret_value__15402__auto__ = (function (){try{while(true){
var result__15403__auto__ = switch__15400__auto__(state_17366);
if(cljs.core.keyword_identical_QMARK_(result__15403__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__15403__auto__;
}
break;
}
}catch (e17390){var ex__15404__auto__ = e17390;
var statearr_17391_18149 = state_17366;
(statearr_17391_18149[(2)] = ex__15404__auto__);


if(cljs.core.seq((state_17366[(4)]))){
var statearr_17392_18150 = state_17366;
(statearr_17392_18150[(1)] = cljs.core.first((state_17366[(4)])));

} else {
throw ex__15404__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__15402__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__18151 = state_17366;
state_17366 = G__18151;
continue;
} else {
return ret_value__15402__auto__;
}
break;
}
});
cljs$core$async$state_machine__15401__auto__ = function(state_17366){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__15401__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__15401__auto____1.call(this,state_17366);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__15401__auto____0;
cljs$core$async$state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__15401__auto____1;
return cljs$core$async$state_machine__15401__auto__;
})()
})();
var state__15646__auto__ = (function (){var statearr_17393 = f__15645__auto__();
(statearr_17393[(6)] = c__15644__auto___18128);

return statearr_17393;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__15646__auto__);
}));


return out;
}));

(cljs.core.async.partition.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition_by = (function cljs$core$async$partition_by(var_args){
var G__17395 = arguments.length;
switch (G__17395) {
case 2:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2 = (function (f,ch){
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3(f,ch,null);
}));

(cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__15644__auto___18153 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__15645__auto__ = (function (){var switch__15400__auto__ = (function (state_17440){
var state_val_17441 = (state_17440[(1)]);
if((state_val_17441 === (7))){
var inst_17436 = (state_17440[(2)]);
var state_17440__$1 = state_17440;
var statearr_17442_18154 = state_17440__$1;
(statearr_17442_18154[(2)] = inst_17436);

(statearr_17442_18154[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17441 === (1))){
var inst_17396 = [];
var inst_17397 = inst_17396;
var inst_17398 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_17440__$1 = (function (){var statearr_17443 = state_17440;
(statearr_17443[(7)] = inst_17397);

(statearr_17443[(8)] = inst_17398);

return statearr_17443;
})();
var statearr_17444_18155 = state_17440__$1;
(statearr_17444_18155[(2)] = null);

(statearr_17444_18155[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17441 === (4))){
var inst_17401 = (state_17440[(9)]);
var inst_17401__$1 = (state_17440[(2)]);
var inst_17402 = (inst_17401__$1 == null);
var inst_17403 = cljs.core.not(inst_17402);
var state_17440__$1 = (function (){var statearr_17445 = state_17440;
(statearr_17445[(9)] = inst_17401__$1);

return statearr_17445;
})();
if(inst_17403){
var statearr_17446_18156 = state_17440__$1;
(statearr_17446_18156[(1)] = (5));

} else {
var statearr_17447_18157 = state_17440__$1;
(statearr_17447_18157[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17441 === (15))){
var inst_17397 = (state_17440[(7)]);
var inst_17428 = cljs.core.vec(inst_17397);
var state_17440__$1 = state_17440;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_17440__$1,(18),out,inst_17428);
} else {
if((state_val_17441 === (13))){
var inst_17423 = (state_17440[(2)]);
var state_17440__$1 = state_17440;
var statearr_17448_18164 = state_17440__$1;
(statearr_17448_18164[(2)] = inst_17423);

(statearr_17448_18164[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17441 === (6))){
var inst_17397 = (state_17440[(7)]);
var inst_17425 = inst_17397.length;
var inst_17426 = (inst_17425 > (0));
var state_17440__$1 = state_17440;
if(cljs.core.truth_(inst_17426)){
var statearr_17449_18165 = state_17440__$1;
(statearr_17449_18165[(1)] = (15));

} else {
var statearr_17450_18166 = state_17440__$1;
(statearr_17450_18166[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17441 === (17))){
var inst_17433 = (state_17440[(2)]);
var inst_17434 = cljs.core.async.close_BANG_(out);
var state_17440__$1 = (function (){var statearr_17451 = state_17440;
(statearr_17451[(10)] = inst_17433);

return statearr_17451;
})();
var statearr_17452_18167 = state_17440__$1;
(statearr_17452_18167[(2)] = inst_17434);

(statearr_17452_18167[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17441 === (3))){
var inst_17438 = (state_17440[(2)]);
var state_17440__$1 = state_17440;
return cljs.core.async.impl.ioc_helpers.return_chan(state_17440__$1,inst_17438);
} else {
if((state_val_17441 === (12))){
var inst_17397 = (state_17440[(7)]);
var inst_17416 = cljs.core.vec(inst_17397);
var state_17440__$1 = state_17440;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_17440__$1,(14),out,inst_17416);
} else {
if((state_val_17441 === (2))){
var state_17440__$1 = state_17440;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_17440__$1,(4),ch);
} else {
if((state_val_17441 === (11))){
var inst_17397 = (state_17440[(7)]);
var inst_17405 = (state_17440[(11)]);
var inst_17401 = (state_17440[(9)]);
var inst_17413 = inst_17397.push(inst_17401);
var tmp17453 = inst_17397;
var inst_17397__$1 = tmp17453;
var inst_17398 = inst_17405;
var state_17440__$1 = (function (){var statearr_17454 = state_17440;
(statearr_17454[(7)] = inst_17397__$1);

(statearr_17454[(12)] = inst_17413);

(statearr_17454[(8)] = inst_17398);

return statearr_17454;
})();
var statearr_17455_18168 = state_17440__$1;
(statearr_17455_18168[(2)] = null);

(statearr_17455_18168[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17441 === (9))){
var inst_17398 = (state_17440[(8)]);
var inst_17409 = cljs.core.keyword_identical_QMARK_(inst_17398,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var state_17440__$1 = state_17440;
var statearr_17456_18169 = state_17440__$1;
(statearr_17456_18169[(2)] = inst_17409);

(statearr_17456_18169[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17441 === (5))){
var inst_17398 = (state_17440[(8)]);
var inst_17406 = (state_17440[(13)]);
var inst_17405 = (state_17440[(11)]);
var inst_17401 = (state_17440[(9)]);
var inst_17405__$1 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_17401) : f.call(null,inst_17401));
var inst_17406__$1 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_17405__$1,inst_17398);
var state_17440__$1 = (function (){var statearr_17457 = state_17440;
(statearr_17457[(13)] = inst_17406__$1);

(statearr_17457[(11)] = inst_17405__$1);

return statearr_17457;
})();
if(inst_17406__$1){
var statearr_17458_18170 = state_17440__$1;
(statearr_17458_18170[(1)] = (8));

} else {
var statearr_17459_18171 = state_17440__$1;
(statearr_17459_18171[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17441 === (14))){
var inst_17405 = (state_17440[(11)]);
var inst_17401 = (state_17440[(9)]);
var inst_17418 = (state_17440[(2)]);
var inst_17419 = [];
var inst_17420 = inst_17419.push(inst_17401);
var inst_17397 = inst_17419;
var inst_17398 = inst_17405;
var state_17440__$1 = (function (){var statearr_17460 = state_17440;
(statearr_17460[(14)] = inst_17418);

(statearr_17460[(7)] = inst_17397);

(statearr_17460[(15)] = inst_17420);

(statearr_17460[(8)] = inst_17398);

return statearr_17460;
})();
var statearr_17461_18172 = state_17440__$1;
(statearr_17461_18172[(2)] = null);

(statearr_17461_18172[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17441 === (16))){
var state_17440__$1 = state_17440;
var statearr_17462_18173 = state_17440__$1;
(statearr_17462_18173[(2)] = null);

(statearr_17462_18173[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17441 === (10))){
var inst_17411 = (state_17440[(2)]);
var state_17440__$1 = state_17440;
if(cljs.core.truth_(inst_17411)){
var statearr_17463_18174 = state_17440__$1;
(statearr_17463_18174[(1)] = (11));

} else {
var statearr_17464_18175 = state_17440__$1;
(statearr_17464_18175[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17441 === (18))){
var inst_17430 = (state_17440[(2)]);
var state_17440__$1 = state_17440;
var statearr_17465_18176 = state_17440__$1;
(statearr_17465_18176[(2)] = inst_17430);

(statearr_17465_18176[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_17441 === (8))){
var inst_17406 = (state_17440[(13)]);
var state_17440__$1 = state_17440;
var statearr_17466_18177 = state_17440__$1;
(statearr_17466_18177[(2)] = inst_17406);

(statearr_17466_18177[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__15401__auto__ = null;
var cljs$core$async$state_machine__15401__auto____0 = (function (){
var statearr_17467 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_17467[(0)] = cljs$core$async$state_machine__15401__auto__);

(statearr_17467[(1)] = (1));

return statearr_17467;
});
var cljs$core$async$state_machine__15401__auto____1 = (function (state_17440){
while(true){
var ret_value__15402__auto__ = (function (){try{while(true){
var result__15403__auto__ = switch__15400__auto__(state_17440);
if(cljs.core.keyword_identical_QMARK_(result__15403__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__15403__auto__;
}
break;
}
}catch (e17468){var ex__15404__auto__ = e17468;
var statearr_17469_18178 = state_17440;
(statearr_17469_18178[(2)] = ex__15404__auto__);


if(cljs.core.seq((state_17440[(4)]))){
var statearr_17470_18179 = state_17440;
(statearr_17470_18179[(1)] = cljs.core.first((state_17440[(4)])));

} else {
throw ex__15404__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__15402__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__18180 = state_17440;
state_17440 = G__18180;
continue;
} else {
return ret_value__15402__auto__;
}
break;
}
});
cljs$core$async$state_machine__15401__auto__ = function(state_17440){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__15401__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__15401__auto____1.call(this,state_17440);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__15401__auto____0;
cljs$core$async$state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__15401__auto____1;
return cljs$core$async$state_machine__15401__auto__;
})()
})();
var state__15646__auto__ = (function (){var statearr_17471 = f__15645__auto__();
(statearr_17471[(6)] = c__15644__auto___18153);

return statearr_17471;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__15646__auto__);
}));


return out;
}));

(cljs.core.async.partition_by.cljs$lang$maxFixedArity = 3);


//# sourceMappingURL=cljs.core.async.js.map
