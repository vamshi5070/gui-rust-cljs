goog.provide('shadow.dom');
shadow.dom.transition_supported_QMARK_ = (((typeof window !== 'undefined'))?goog.style.transition.isSupported():null);

/**
 * @interface
 */
shadow.dom.IElement = function(){};

var shadow$dom$IElement$_to_dom$dyn_18197 = (function (this$){
var x__5393__auto__ = (((this$ == null))?null:this$);
var m__5394__auto__ = (shadow.dom._to_dom[goog.typeOf(x__5393__auto__)]);
if((!((m__5394__auto__ == null)))){
return (m__5394__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5394__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__5394__auto__.call(null,this$));
} else {
var m__5392__auto__ = (shadow.dom._to_dom["_"]);
if((!((m__5392__auto__ == null)))){
return (m__5392__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5392__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__5392__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("IElement.-to-dom",this$);
}
}
});
shadow.dom._to_dom = (function shadow$dom$_to_dom(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$IElement$_to_dom$arity$1 == null)))))){
return this$.shadow$dom$IElement$_to_dom$arity$1(this$);
} else {
return shadow$dom$IElement$_to_dom$dyn_18197(this$);
}
});


/**
 * @interface
 */
shadow.dom.SVGElement = function(){};

var shadow$dom$SVGElement$_to_svg$dyn_18198 = (function (this$){
var x__5393__auto__ = (((this$ == null))?null:this$);
var m__5394__auto__ = (shadow.dom._to_svg[goog.typeOf(x__5393__auto__)]);
if((!((m__5394__auto__ == null)))){
return (m__5394__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5394__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__5394__auto__.call(null,this$));
} else {
var m__5392__auto__ = (shadow.dom._to_svg["_"]);
if((!((m__5392__auto__ == null)))){
return (m__5392__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5392__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__5392__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("SVGElement.-to-svg",this$);
}
}
});
shadow.dom._to_svg = (function shadow$dom$_to_svg(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$SVGElement$_to_svg$arity$1 == null)))))){
return this$.shadow$dom$SVGElement$_to_svg$arity$1(this$);
} else {
return shadow$dom$SVGElement$_to_svg$dyn_18198(this$);
}
});

shadow.dom.lazy_native_coll_seq = (function shadow$dom$lazy_native_coll_seq(coll,idx){
if((idx < coll.length)){
return (new cljs.core.LazySeq(null,(function (){
return cljs.core.cons((coll[idx]),(function (){var G__17504 = coll;
var G__17505 = (idx + (1));
return (shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2 ? shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2(G__17504,G__17505) : shadow.dom.lazy_native_coll_seq.call(null,G__17504,G__17505));
})());
}),null,null));
} else {
return null;
}
});

/**
* @constructor
 * @implements {cljs.core.IIndexed}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IDeref}
 * @implements {shadow.dom.IElement}
*/
shadow.dom.NativeColl = (function (coll){
this.coll = coll;
this.cljs$lang$protocol_mask$partition0$ = 8421394;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(shadow.dom.NativeColl.prototype.cljs$core$IDeref$_deref$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
}));

(shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$2 = (function (this$,n){
var self__ = this;
var this$__$1 = this;
return (self__.coll[n]);
}));

(shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$3 = (function (this$,n,not_found){
var self__ = this;
var this$__$1 = this;
var or__5045__auto__ = (self__.coll[n]);
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return not_found;
}
}));

(shadow.dom.NativeColl.prototype.cljs$core$ICounted$_count$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll.length;
}));

(shadow.dom.NativeColl.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return shadow.dom.lazy_native_coll_seq(self__.coll,(0));
}));

(shadow.dom.NativeColl.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.dom.NativeColl.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
}));

(shadow.dom.NativeColl.getBasis = (function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"coll","coll",-1006698606,null)], null);
}));

(shadow.dom.NativeColl.cljs$lang$type = true);

(shadow.dom.NativeColl.cljs$lang$ctorStr = "shadow.dom/NativeColl");

(shadow.dom.NativeColl.cljs$lang$ctorPrWriter = (function (this__5330__auto__,writer__5331__auto__,opt__5332__auto__){
return cljs.core._write(writer__5331__auto__,"shadow.dom/NativeColl");
}));

/**
 * Positional factory function for shadow.dom/NativeColl.
 */
shadow.dom.__GT_NativeColl = (function shadow$dom$__GT_NativeColl(coll){
return (new shadow.dom.NativeColl(coll));
});

shadow.dom.native_coll = (function shadow$dom$native_coll(coll){
return (new shadow.dom.NativeColl(coll));
});
shadow.dom.dom_node = (function shadow$dom$dom_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$IElement$))))?true:false):false)){
return el.shadow$dom$IElement$_to_dom$arity$1(null);
} else {
if(typeof el === 'string'){
return document.createTextNode(el);
} else {
if(typeof el === 'number'){
return document.createTextNode(cljs.core.str.cljs$core$IFn$_invoke$arity$1(el));
} else {
return el;

}
}
}
}
});
shadow.dom.query_one = (function shadow$dom$query_one(var_args){
var G__17528 = arguments.length;
switch (G__17528) {
case 1:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return document.querySelector(sel);
}));

(shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return shadow.dom.dom_node(root).querySelector(sel);
}));

(shadow.dom.query_one.cljs$lang$maxFixedArity = 2);

shadow.dom.query = (function shadow$dom$query(var_args){
var G__17533 = arguments.length;
switch (G__17533) {
case 1:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.query.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return (new shadow.dom.NativeColl(document.querySelectorAll(sel)));
}));

(shadow.dom.query.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(root).querySelectorAll(sel)));
}));

(shadow.dom.query.cljs$lang$maxFixedArity = 2);

shadow.dom.by_id = (function shadow$dom$by_id(var_args){
var G__17546 = arguments.length;
switch (G__17546) {
case 2:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2 = (function (id,el){
return shadow.dom.dom_node(el).getElementById(id);
}));

(shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1 = (function (id){
return document.getElementById(id);
}));

(shadow.dom.by_id.cljs$lang$maxFixedArity = 2);

shadow.dom.build = shadow.dom.dom_node;
shadow.dom.ev_stop = (function shadow$dom$ev_stop(var_args){
var G__17554 = arguments.length;
switch (G__17554) {
case 1:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1 = (function (e){
if(cljs.core.truth_(e.stopPropagation)){
e.stopPropagation();

e.preventDefault();
} else {
(e.cancelBubble = true);

(e.returnValue = false);
}

return e;
}));

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2 = (function (e,el){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
}));

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4 = (function (e,el,scope,owner){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
}));

(shadow.dom.ev_stop.cljs$lang$maxFixedArity = 4);

/**
 * check wether a parent node (or the document) contains the child
 */
shadow.dom.contains_QMARK_ = (function shadow$dom$contains_QMARK_(var_args){
var G__17568 = arguments.length;
switch (G__17568) {
case 1:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1 = (function (el){
return goog.dom.contains(document,shadow.dom.dom_node(el));
}));

(shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2 = (function (parent,el){
return goog.dom.contains(shadow.dom.dom_node(parent),shadow.dom.dom_node(el));
}));

(shadow.dom.contains_QMARK_.cljs$lang$maxFixedArity = 2);

shadow.dom.add_class = (function shadow$dom$add_class(el,cls){
return goog.dom.classlist.add(shadow.dom.dom_node(el),cls);
});
shadow.dom.remove_class = (function shadow$dom$remove_class(el,cls){
return goog.dom.classlist.remove(shadow.dom.dom_node(el),cls);
});
shadow.dom.toggle_class = (function shadow$dom$toggle_class(var_args){
var G__17575 = arguments.length;
switch (G__17575) {
case 2:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2 = (function (el,cls){
return goog.dom.classlist.toggle(shadow.dom.dom_node(el),cls);
}));

(shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3 = (function (el,cls,v){
if(cljs.core.truth_(v)){
return shadow.dom.add_class(el,cls);
} else {
return shadow.dom.remove_class(el,cls);
}
}));

(shadow.dom.toggle_class.cljs$lang$maxFixedArity = 3);

shadow.dom.dom_listen = (cljs.core.truth_((function (){var or__5045__auto__ = (!((typeof document !== 'undefined')));
if(or__5045__auto__){
return or__5045__auto__;
} else {
return document.addEventListener;
}
})())?(function shadow$dom$dom_listen_good(el,ev,handler){
return el.addEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_ie(el,ev,handler){
try{return el.attachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),(function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
}));
}catch (e17584){if((e17584 instanceof Object)){
var e = e17584;
return console.log("didnt support attachEvent",el,e);
} else {
throw e17584;

}
}}));
shadow.dom.dom_listen_remove = (cljs.core.truth_((function (){var or__5045__auto__ = (!((typeof document !== 'undefined')));
if(or__5045__auto__){
return or__5045__auto__;
} else {
return document.removeEventListener;
}
})())?(function shadow$dom$dom_listen_remove_good(el,ev,handler){
return el.removeEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_remove_ie(el,ev,handler){
return el.detachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),handler);
}));
shadow.dom.on_query = (function shadow$dom$on_query(root_el,ev,selector,handler){
var seq__17593 = cljs.core.seq(shadow.dom.query.cljs$core$IFn$_invoke$arity$2(selector,root_el));
var chunk__17594 = null;
var count__17595 = (0);
var i__17596 = (0);
while(true){
if((i__17596 < count__17595)){
var el = chunk__17594.cljs$core$IIndexed$_nth$arity$2(null,i__17596);
var handler_18205__$1 = ((function (seq__17593,chunk__17594,count__17595,i__17596,el){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__17593,chunk__17594,count__17595,i__17596,el))
;
shadow.dom.dom_listen(el,cljs.core.name(ev),handler_18205__$1);


var G__18206 = seq__17593;
var G__18207 = chunk__17594;
var G__18208 = count__17595;
var G__18209 = (i__17596 + (1));
seq__17593 = G__18206;
chunk__17594 = G__18207;
count__17595 = G__18208;
i__17596 = G__18209;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__17593);
if(temp__5804__auto__){
var seq__17593__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__17593__$1)){
var c__5568__auto__ = cljs.core.chunk_first(seq__17593__$1);
var G__18210 = cljs.core.chunk_rest(seq__17593__$1);
var G__18211 = c__5568__auto__;
var G__18212 = cljs.core.count(c__5568__auto__);
var G__18213 = (0);
seq__17593 = G__18210;
chunk__17594 = G__18211;
count__17595 = G__18212;
i__17596 = G__18213;
continue;
} else {
var el = cljs.core.first(seq__17593__$1);
var handler_18214__$1 = ((function (seq__17593,chunk__17594,count__17595,i__17596,el,seq__17593__$1,temp__5804__auto__){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__17593,chunk__17594,count__17595,i__17596,el,seq__17593__$1,temp__5804__auto__))
;
shadow.dom.dom_listen(el,cljs.core.name(ev),handler_18214__$1);


var G__18215 = cljs.core.next(seq__17593__$1);
var G__18216 = null;
var G__18217 = (0);
var G__18218 = (0);
seq__17593 = G__18215;
chunk__17594 = G__18216;
count__17595 = G__18217;
i__17596 = G__18218;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.on = (function shadow$dom$on(var_args){
var G__17612 = arguments.length;
switch (G__17612) {
case 3:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.on.cljs$core$IFn$_invoke$arity$3 = (function (el,ev,handler){
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4(el,ev,handler,false);
}));

(shadow.dom.on.cljs$core$IFn$_invoke$arity$4 = (function (el,ev,handler,capture){
if(cljs.core.vector_QMARK_(ev)){
return shadow.dom.on_query(el,cljs.core.first(ev),cljs.core.second(ev),handler);
} else {
var handler__$1 = (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});
return shadow.dom.dom_listen(shadow.dom.dom_node(el),cljs.core.name(ev),handler__$1);
}
}));

(shadow.dom.on.cljs$lang$maxFixedArity = 4);

shadow.dom.remove_event_handler = (function shadow$dom$remove_event_handler(el,ev,handler){
return shadow.dom.dom_listen_remove(shadow.dom.dom_node(el),cljs.core.name(ev),handler);
});
shadow.dom.add_event_listeners = (function shadow$dom$add_event_listeners(el,events){
var seq__17626 = cljs.core.seq(events);
var chunk__17627 = null;
var count__17628 = (0);
var i__17629 = (0);
while(true){
if((i__17629 < count__17628)){
var vec__17641 = chunk__17627.cljs$core$IIndexed$_nth$arity$2(null,i__17629);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__17641,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__17641,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__18220 = seq__17626;
var G__18221 = chunk__17627;
var G__18222 = count__17628;
var G__18223 = (i__17629 + (1));
seq__17626 = G__18220;
chunk__17627 = G__18221;
count__17628 = G__18222;
i__17629 = G__18223;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__17626);
if(temp__5804__auto__){
var seq__17626__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__17626__$1)){
var c__5568__auto__ = cljs.core.chunk_first(seq__17626__$1);
var G__18224 = cljs.core.chunk_rest(seq__17626__$1);
var G__18225 = c__5568__auto__;
var G__18226 = cljs.core.count(c__5568__auto__);
var G__18227 = (0);
seq__17626 = G__18224;
chunk__17627 = G__18225;
count__17628 = G__18226;
i__17629 = G__18227;
continue;
} else {
var vec__17648 = cljs.core.first(seq__17626__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__17648,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__17648,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__18228 = cljs.core.next(seq__17626__$1);
var G__18229 = null;
var G__18230 = (0);
var G__18231 = (0);
seq__17626 = G__18228;
chunk__17627 = G__18229;
count__17628 = G__18230;
i__17629 = G__18231;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_style = (function shadow$dom$set_style(el,styles){
var dom = shadow.dom.dom_node(el);
var seq__17655 = cljs.core.seq(styles);
var chunk__17656 = null;
var count__17657 = (0);
var i__17658 = (0);
while(true){
if((i__17658 < count__17657)){
var vec__17669 = chunk__17656.cljs$core$IIndexed$_nth$arity$2(null,i__17658);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__17669,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__17669,(1),null);
goog.style.setStyle(dom,cljs.core.name(k),(((v == null))?"":v));


var G__18232 = seq__17655;
var G__18233 = chunk__17656;
var G__18234 = count__17657;
var G__18235 = (i__17658 + (1));
seq__17655 = G__18232;
chunk__17656 = G__18233;
count__17657 = G__18234;
i__17658 = G__18235;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__17655);
if(temp__5804__auto__){
var seq__17655__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__17655__$1)){
var c__5568__auto__ = cljs.core.chunk_first(seq__17655__$1);
var G__18236 = cljs.core.chunk_rest(seq__17655__$1);
var G__18237 = c__5568__auto__;
var G__18238 = cljs.core.count(c__5568__auto__);
var G__18239 = (0);
seq__17655 = G__18236;
chunk__17656 = G__18237;
count__17657 = G__18238;
i__17658 = G__18239;
continue;
} else {
var vec__17675 = cljs.core.first(seq__17655__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__17675,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__17675,(1),null);
goog.style.setStyle(dom,cljs.core.name(k),(((v == null))?"":v));


var G__18240 = cljs.core.next(seq__17655__$1);
var G__18241 = null;
var G__18242 = (0);
var G__18243 = (0);
seq__17655 = G__18240;
chunk__17656 = G__18241;
count__17657 = G__18242;
i__17658 = G__18243;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_attr_STAR_ = (function shadow$dom$set_attr_STAR_(el,key,value){
var G__17682_18244 = key;
var G__17682_18245__$1 = (((G__17682_18244 instanceof cljs.core.Keyword))?G__17682_18244.fqn:null);
switch (G__17682_18245__$1) {
case "id":
(el.id = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));

break;
case "class":
(el.className = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));

break;
case "for":
(el.htmlFor = value);

break;
case "cellpadding":
el.setAttribute("cellPadding",value);

break;
case "cellspacing":
el.setAttribute("cellSpacing",value);

break;
case "colspan":
el.setAttribute("colSpan",value);

break;
case "frameborder":
el.setAttribute("frameBorder",value);

break;
case "height":
el.setAttribute("height",value);

break;
case "maxlength":
el.setAttribute("maxLength",value);

break;
case "role":
el.setAttribute("role",value);

break;
case "rowspan":
el.setAttribute("rowSpan",value);

break;
case "type":
el.setAttribute("type",value);

break;
case "usemap":
el.setAttribute("useMap",value);

break;
case "valign":
el.setAttribute("vAlign",value);

break;
case "width":
el.setAttribute("width",value);

break;
case "on":
shadow.dom.add_event_listeners(el,value);

break;
case "style":
if((value == null)){
} else {
if(typeof value === 'string'){
el.setAttribute("style",value);
} else {
if(cljs.core.map_QMARK_(value)){
shadow.dom.set_style(el,value);
} else {
goog.style.setStyle(el,value);

}
}
}

break;
default:
var ks_18247 = cljs.core.name(key);
if(cljs.core.truth_((function (){var or__5045__auto__ = goog.string.startsWith(ks_18247,"data-");
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return goog.string.startsWith(ks_18247,"aria-");
}
})())){
el.setAttribute(ks_18247,value);
} else {
(el[ks_18247] = value);
}

}

return el;
});
shadow.dom.set_attrs = (function shadow$dom$set_attrs(el,attrs){
return cljs.core.reduce_kv((function (el__$1,key,value){
shadow.dom.set_attr_STAR_(el__$1,key,value);

return el__$1;
}),shadow.dom.dom_node(el),attrs);
});
shadow.dom.set_attr = (function shadow$dom$set_attr(el,key,value){
return shadow.dom.set_attr_STAR_(shadow.dom.dom_node(el),key,value);
});
shadow.dom.has_class_QMARK_ = (function shadow$dom$has_class_QMARK_(el,cls){
return goog.dom.classlist.contains(shadow.dom.dom_node(el),cls);
});
shadow.dom.merge_class_string = (function shadow$dom$merge_class_string(current,extra_class){
if(cljs.core.seq(current)){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(current)," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(extra_class)].join('');
} else {
return extra_class;
}
});
shadow.dom.parse_tag = (function shadow$dom$parse_tag(spec){
var spec__$1 = cljs.core.name(spec);
var fdot = spec__$1.indexOf(".");
var fhash = spec__$1.indexOf("#");
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)))){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1,null,null], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fdot),null,clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1))),null], null);
} else {
if((fhash > fdot)){
throw ["cant have id after class?",spec__$1].join('');
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1)),fdot),clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);

}
}
}
}
});
shadow.dom.create_dom_node = (function shadow$dom$create_dom_node(tag_def,p__17701){
var map__17702 = p__17701;
var map__17702__$1 = cljs.core.__destructure_map(map__17702);
var props = map__17702__$1;
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__17702__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
var tag_props = ({});
var vec__17703 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__17703,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__17703,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__17703,(2),null);
if(cljs.core.truth_(tag_id)){
(tag_props["id"] = tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
(tag_props["class"] = shadow.dom.merge_class_string(class$,tag_classes));
} else {
}

var G__17706 = goog.dom.createDom(tag_name,tag_props);
shadow.dom.set_attrs(G__17706,cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(props,new cljs.core.Keyword(null,"class","class",-2030961996)));

return G__17706;
});
shadow.dom.append = (function shadow$dom$append(var_args){
var G__17712 = arguments.length;
switch (G__17712) {
case 1:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.append.cljs$core$IFn$_invoke$arity$1 = (function (node){
if(cljs.core.truth_(node)){
var temp__5804__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5804__auto__)){
var n = temp__5804__auto__;
document.body.appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
}));

(shadow.dom.append.cljs$core$IFn$_invoke$arity$2 = (function (el,node){
if(cljs.core.truth_(node)){
var temp__5804__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5804__auto__)){
var n = temp__5804__auto__;
shadow.dom.dom_node(el).appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
}));

(shadow.dom.append.cljs$lang$maxFixedArity = 2);

shadow.dom.destructure_node = (function shadow$dom$destructure_node(create_fn,p__17721){
var vec__17723 = p__17721;
var seq__17724 = cljs.core.seq(vec__17723);
var first__17725 = cljs.core.first(seq__17724);
var seq__17724__$1 = cljs.core.next(seq__17724);
var nn = first__17725;
var first__17725__$1 = cljs.core.first(seq__17724__$1);
var seq__17724__$2 = cljs.core.next(seq__17724__$1);
var np = first__17725__$1;
var nc = seq__17724__$2;
var node = vec__17723;
if((nn instanceof cljs.core.Keyword)){
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("invalid dom node",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"node","node",581201198),node], null));
}

if((((np == null)) && ((nc == null)))){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__17727 = nn;
var G__17728 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__17727,G__17728) : create_fn.call(null,G__17727,G__17728));
})(),cljs.core.List.EMPTY], null);
} else {
if(cljs.core.map_QMARK_(np)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(nn,np) : create_fn.call(null,nn,np)),nc], null);
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__17730 = nn;
var G__17731 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__17730,G__17731) : create_fn.call(null,G__17730,G__17731));
})(),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(nc,np)], null);

}
}
});
shadow.dom.make_dom_node = (function shadow$dom$make_dom_node(structure){
var vec__17736 = shadow.dom.destructure_node(shadow.dom.create_dom_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__17736,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__17736,(1),null);
var seq__17739_18251 = cljs.core.seq(node_children);
var chunk__17740_18252 = null;
var count__17741_18253 = (0);
var i__17742_18254 = (0);
while(true){
if((i__17742_18254 < count__17741_18253)){
var child_struct_18255 = chunk__17740_18252.cljs$core$IIndexed$_nth$arity$2(null,i__17742_18254);
var children_18256 = shadow.dom.dom_node(child_struct_18255);
if(cljs.core.seq_QMARK_(children_18256)){
var seq__17791_18257 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_18256));
var chunk__17793_18258 = null;
var count__17794_18259 = (0);
var i__17795_18260 = (0);
while(true){
if((i__17795_18260 < count__17794_18259)){
var child_18261 = chunk__17793_18258.cljs$core$IIndexed$_nth$arity$2(null,i__17795_18260);
if(cljs.core.truth_(child_18261)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_18261);


var G__18262 = seq__17791_18257;
var G__18263 = chunk__17793_18258;
var G__18264 = count__17794_18259;
var G__18265 = (i__17795_18260 + (1));
seq__17791_18257 = G__18262;
chunk__17793_18258 = G__18263;
count__17794_18259 = G__18264;
i__17795_18260 = G__18265;
continue;
} else {
var G__18266 = seq__17791_18257;
var G__18267 = chunk__17793_18258;
var G__18268 = count__17794_18259;
var G__18269 = (i__17795_18260 + (1));
seq__17791_18257 = G__18266;
chunk__17793_18258 = G__18267;
count__17794_18259 = G__18268;
i__17795_18260 = G__18269;
continue;
}
} else {
var temp__5804__auto___18270 = cljs.core.seq(seq__17791_18257);
if(temp__5804__auto___18270){
var seq__17791_18271__$1 = temp__5804__auto___18270;
if(cljs.core.chunked_seq_QMARK_(seq__17791_18271__$1)){
var c__5568__auto___18272 = cljs.core.chunk_first(seq__17791_18271__$1);
var G__18273 = cljs.core.chunk_rest(seq__17791_18271__$1);
var G__18274 = c__5568__auto___18272;
var G__18275 = cljs.core.count(c__5568__auto___18272);
var G__18276 = (0);
seq__17791_18257 = G__18273;
chunk__17793_18258 = G__18274;
count__17794_18259 = G__18275;
i__17795_18260 = G__18276;
continue;
} else {
var child_18277 = cljs.core.first(seq__17791_18271__$1);
if(cljs.core.truth_(child_18277)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_18277);


var G__18278 = cljs.core.next(seq__17791_18271__$1);
var G__18279 = null;
var G__18280 = (0);
var G__18281 = (0);
seq__17791_18257 = G__18278;
chunk__17793_18258 = G__18279;
count__17794_18259 = G__18280;
i__17795_18260 = G__18281;
continue;
} else {
var G__18282 = cljs.core.next(seq__17791_18271__$1);
var G__18283 = null;
var G__18284 = (0);
var G__18285 = (0);
seq__17791_18257 = G__18282;
chunk__17793_18258 = G__18283;
count__17794_18259 = G__18284;
i__17795_18260 = G__18285;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_18256);
}


var G__18286 = seq__17739_18251;
var G__18287 = chunk__17740_18252;
var G__18288 = count__17741_18253;
var G__18289 = (i__17742_18254 + (1));
seq__17739_18251 = G__18286;
chunk__17740_18252 = G__18287;
count__17741_18253 = G__18288;
i__17742_18254 = G__18289;
continue;
} else {
var temp__5804__auto___18290 = cljs.core.seq(seq__17739_18251);
if(temp__5804__auto___18290){
var seq__17739_18291__$1 = temp__5804__auto___18290;
if(cljs.core.chunked_seq_QMARK_(seq__17739_18291__$1)){
var c__5568__auto___18292 = cljs.core.chunk_first(seq__17739_18291__$1);
var G__18293 = cljs.core.chunk_rest(seq__17739_18291__$1);
var G__18294 = c__5568__auto___18292;
var G__18295 = cljs.core.count(c__5568__auto___18292);
var G__18296 = (0);
seq__17739_18251 = G__18293;
chunk__17740_18252 = G__18294;
count__17741_18253 = G__18295;
i__17742_18254 = G__18296;
continue;
} else {
var child_struct_18297 = cljs.core.first(seq__17739_18291__$1);
var children_18299 = shadow.dom.dom_node(child_struct_18297);
if(cljs.core.seq_QMARK_(children_18299)){
var seq__17804_18300 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_18299));
var chunk__17806_18301 = null;
var count__17807_18302 = (0);
var i__17808_18303 = (0);
while(true){
if((i__17808_18303 < count__17807_18302)){
var child_18304 = chunk__17806_18301.cljs$core$IIndexed$_nth$arity$2(null,i__17808_18303);
if(cljs.core.truth_(child_18304)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_18304);


var G__18305 = seq__17804_18300;
var G__18306 = chunk__17806_18301;
var G__18307 = count__17807_18302;
var G__18308 = (i__17808_18303 + (1));
seq__17804_18300 = G__18305;
chunk__17806_18301 = G__18306;
count__17807_18302 = G__18307;
i__17808_18303 = G__18308;
continue;
} else {
var G__18309 = seq__17804_18300;
var G__18310 = chunk__17806_18301;
var G__18311 = count__17807_18302;
var G__18312 = (i__17808_18303 + (1));
seq__17804_18300 = G__18309;
chunk__17806_18301 = G__18310;
count__17807_18302 = G__18311;
i__17808_18303 = G__18312;
continue;
}
} else {
var temp__5804__auto___18313__$1 = cljs.core.seq(seq__17804_18300);
if(temp__5804__auto___18313__$1){
var seq__17804_18314__$1 = temp__5804__auto___18313__$1;
if(cljs.core.chunked_seq_QMARK_(seq__17804_18314__$1)){
var c__5568__auto___18315 = cljs.core.chunk_first(seq__17804_18314__$1);
var G__18316 = cljs.core.chunk_rest(seq__17804_18314__$1);
var G__18317 = c__5568__auto___18315;
var G__18318 = cljs.core.count(c__5568__auto___18315);
var G__18319 = (0);
seq__17804_18300 = G__18316;
chunk__17806_18301 = G__18317;
count__17807_18302 = G__18318;
i__17808_18303 = G__18319;
continue;
} else {
var child_18320 = cljs.core.first(seq__17804_18314__$1);
if(cljs.core.truth_(child_18320)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_18320);


var G__18321 = cljs.core.next(seq__17804_18314__$1);
var G__18322 = null;
var G__18323 = (0);
var G__18324 = (0);
seq__17804_18300 = G__18321;
chunk__17806_18301 = G__18322;
count__17807_18302 = G__18323;
i__17808_18303 = G__18324;
continue;
} else {
var G__18325 = cljs.core.next(seq__17804_18314__$1);
var G__18326 = null;
var G__18327 = (0);
var G__18328 = (0);
seq__17804_18300 = G__18325;
chunk__17806_18301 = G__18326;
count__17807_18302 = G__18327;
i__17808_18303 = G__18328;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_18299);
}


var G__18329 = cljs.core.next(seq__17739_18291__$1);
var G__18330 = null;
var G__18331 = (0);
var G__18332 = (0);
seq__17739_18251 = G__18329;
chunk__17740_18252 = G__18330;
count__17741_18253 = G__18331;
i__17742_18254 = G__18332;
continue;
}
} else {
}
}
break;
}

return node;
});
(cljs.core.Keyword.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.Keyword.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$__$1], null));
}));

(cljs.core.PersistentVector.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.PersistentVector.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(this$__$1);
}));

(cljs.core.LazySeq.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.LazySeq.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_dom,this$__$1);
}));
if(cljs.core.truth_(((typeof HTMLElement) != 'undefined'))){
(HTMLElement.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(HTMLElement.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
}));
} else {
}
if(cljs.core.truth_(((typeof DocumentFragment) != 'undefined'))){
(DocumentFragment.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(DocumentFragment.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
}));
} else {
}
/**
 * clear node children
 */
shadow.dom.reset = (function shadow$dom$reset(node){
return goog.dom.removeChildren(shadow.dom.dom_node(node));
});
shadow.dom.remove = (function shadow$dom$remove(node){
if((((!((node == null))))?(((((node.cljs$lang$protocol_mask$partition0$ & (8388608))) || ((cljs.core.PROTOCOL_SENTINEL === node.cljs$core$ISeqable$))))?true:false):false)){
var seq__17819 = cljs.core.seq(node);
var chunk__17820 = null;
var count__17821 = (0);
var i__17822 = (0);
while(true){
if((i__17822 < count__17821)){
var n = chunk__17820.cljs$core$IIndexed$_nth$arity$2(null,i__17822);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__18334 = seq__17819;
var G__18335 = chunk__17820;
var G__18336 = count__17821;
var G__18337 = (i__17822 + (1));
seq__17819 = G__18334;
chunk__17820 = G__18335;
count__17821 = G__18336;
i__17822 = G__18337;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__17819);
if(temp__5804__auto__){
var seq__17819__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__17819__$1)){
var c__5568__auto__ = cljs.core.chunk_first(seq__17819__$1);
var G__18338 = cljs.core.chunk_rest(seq__17819__$1);
var G__18339 = c__5568__auto__;
var G__18340 = cljs.core.count(c__5568__auto__);
var G__18341 = (0);
seq__17819 = G__18338;
chunk__17820 = G__18339;
count__17821 = G__18340;
i__17822 = G__18341;
continue;
} else {
var n = cljs.core.first(seq__17819__$1);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__18342 = cljs.core.next(seq__17819__$1);
var G__18343 = null;
var G__18344 = (0);
var G__18345 = (0);
seq__17819 = G__18342;
chunk__17820 = G__18343;
count__17821 = G__18344;
i__17822 = G__18345;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return goog.dom.removeNode(node);
}
});
shadow.dom.replace_node = (function shadow$dom$replace_node(old,new$){
return goog.dom.replaceNode(shadow.dom.dom_node(new$),shadow.dom.dom_node(old));
});
shadow.dom.text = (function shadow$dom$text(var_args){
var G__17829 = arguments.length;
switch (G__17829) {
case 2:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.text.cljs$core$IFn$_invoke$arity$2 = (function (el,new_text){
return (shadow.dom.dom_node(el).innerText = new_text);
}));

(shadow.dom.text.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.dom_node(el).innerText;
}));

(shadow.dom.text.cljs$lang$maxFixedArity = 2);

shadow.dom.check = (function shadow$dom$check(var_args){
var G__17838 = arguments.length;
switch (G__17838) {
case 1:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.check.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2(el,true);
}));

(shadow.dom.check.cljs$core$IFn$_invoke$arity$2 = (function (el,checked){
return (shadow.dom.dom_node(el).checked = checked);
}));

(shadow.dom.check.cljs$lang$maxFixedArity = 2);

shadow.dom.checked_QMARK_ = (function shadow$dom$checked_QMARK_(el){
return shadow.dom.dom_node(el).checked;
});
shadow.dom.form_elements = (function shadow$dom$form_elements(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).elements));
});
shadow.dom.children = (function shadow$dom$children(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).children));
});
shadow.dom.child_nodes = (function shadow$dom$child_nodes(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).childNodes));
});
shadow.dom.attr = (function shadow$dom$attr(var_args){
var G__17853 = arguments.length;
switch (G__17853) {
case 2:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.attr.cljs$core$IFn$_invoke$arity$2 = (function (el,key){
return shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
}));

(shadow.dom.attr.cljs$core$IFn$_invoke$arity$3 = (function (el,key,default$){
var or__5045__auto__ = shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return default$;
}
}));

(shadow.dom.attr.cljs$lang$maxFixedArity = 3);

shadow.dom.del_attr = (function shadow$dom$del_attr(el,key){
return shadow.dom.dom_node(el).removeAttribute(cljs.core.name(key));
});
shadow.dom.data = (function shadow$dom$data(el,key){
return shadow.dom.dom_node(el).getAttribute(["data-",cljs.core.name(key)].join(''));
});
shadow.dom.set_data = (function shadow$dom$set_data(el,key,value){
return shadow.dom.dom_node(el).setAttribute(["data-",cljs.core.name(key)].join(''),cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));
});
shadow.dom.set_html = (function shadow$dom$set_html(node,text){
return (shadow.dom.dom_node(node).innerHTML = text);
});
shadow.dom.get_html = (function shadow$dom$get_html(node){
return shadow.dom.dom_node(node).innerHTML;
});
shadow.dom.fragment = (function shadow$dom$fragment(var_args){
var args__5775__auto__ = [];
var len__5769__auto___18350 = arguments.length;
var i__5770__auto___18351 = (0);
while(true){
if((i__5770__auto___18351 < len__5769__auto___18350)){
args__5775__auto__.push((arguments[i__5770__auto___18351]));

var G__18352 = (i__5770__auto___18351 + (1));
i__5770__auto___18351 = G__18352;
continue;
} else {
}
break;
}

var argseq__5776__auto__ = ((((0) < args__5775__auto__.length))?(new cljs.core.IndexedSeq(args__5775__auto__.slice((0)),(0),null)):null);
return shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic(argseq__5776__auto__);
});

(shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic = (function (nodes){
var fragment = document.createDocumentFragment();
var seq__17869_18353 = cljs.core.seq(nodes);
var chunk__17870_18354 = null;
var count__17871_18355 = (0);
var i__17872_18356 = (0);
while(true){
if((i__17872_18356 < count__17871_18355)){
var node_18358 = chunk__17870_18354.cljs$core$IIndexed$_nth$arity$2(null,i__17872_18356);
fragment.appendChild(shadow.dom._to_dom(node_18358));


var G__18359 = seq__17869_18353;
var G__18360 = chunk__17870_18354;
var G__18361 = count__17871_18355;
var G__18362 = (i__17872_18356 + (1));
seq__17869_18353 = G__18359;
chunk__17870_18354 = G__18360;
count__17871_18355 = G__18361;
i__17872_18356 = G__18362;
continue;
} else {
var temp__5804__auto___18363 = cljs.core.seq(seq__17869_18353);
if(temp__5804__auto___18363){
var seq__17869_18364__$1 = temp__5804__auto___18363;
if(cljs.core.chunked_seq_QMARK_(seq__17869_18364__$1)){
var c__5568__auto___18365 = cljs.core.chunk_first(seq__17869_18364__$1);
var G__18366 = cljs.core.chunk_rest(seq__17869_18364__$1);
var G__18367 = c__5568__auto___18365;
var G__18368 = cljs.core.count(c__5568__auto___18365);
var G__18369 = (0);
seq__17869_18353 = G__18366;
chunk__17870_18354 = G__18367;
count__17871_18355 = G__18368;
i__17872_18356 = G__18369;
continue;
} else {
var node_18370 = cljs.core.first(seq__17869_18364__$1);
fragment.appendChild(shadow.dom._to_dom(node_18370));


var G__18371 = cljs.core.next(seq__17869_18364__$1);
var G__18372 = null;
var G__18373 = (0);
var G__18374 = (0);
seq__17869_18353 = G__18371;
chunk__17870_18354 = G__18372;
count__17871_18355 = G__18373;
i__17872_18356 = G__18374;
continue;
}
} else {
}
}
break;
}

return (new shadow.dom.NativeColl(fragment));
}));

(shadow.dom.fragment.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(shadow.dom.fragment.cljs$lang$applyTo = (function (seq17865){
var self__5755__auto__ = this;
return self__5755__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq17865));
}));

/**
 * given a html string, eval all <script> tags and return the html without the scripts
 * don't do this for everything, only content you trust.
 */
shadow.dom.eval_scripts = (function shadow$dom$eval_scripts(s){
var scripts = cljs.core.re_seq(/<script[^>]*?>(.+?)<\/script>/,s);
var seq__17883_18375 = cljs.core.seq(scripts);
var chunk__17884_18376 = null;
var count__17885_18377 = (0);
var i__17886_18378 = (0);
while(true){
if((i__17886_18378 < count__17885_18377)){
var vec__17900_18380 = chunk__17884_18376.cljs$core$IIndexed$_nth$arity$2(null,i__17886_18378);
var script_tag_18381 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__17900_18380,(0),null);
var script_body_18382 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__17900_18380,(1),null);
eval(script_body_18382);


var G__18384 = seq__17883_18375;
var G__18385 = chunk__17884_18376;
var G__18386 = count__17885_18377;
var G__18387 = (i__17886_18378 + (1));
seq__17883_18375 = G__18384;
chunk__17884_18376 = G__18385;
count__17885_18377 = G__18386;
i__17886_18378 = G__18387;
continue;
} else {
var temp__5804__auto___18388 = cljs.core.seq(seq__17883_18375);
if(temp__5804__auto___18388){
var seq__17883_18389__$1 = temp__5804__auto___18388;
if(cljs.core.chunked_seq_QMARK_(seq__17883_18389__$1)){
var c__5568__auto___18390 = cljs.core.chunk_first(seq__17883_18389__$1);
var G__18391 = cljs.core.chunk_rest(seq__17883_18389__$1);
var G__18392 = c__5568__auto___18390;
var G__18393 = cljs.core.count(c__5568__auto___18390);
var G__18394 = (0);
seq__17883_18375 = G__18391;
chunk__17884_18376 = G__18392;
count__17885_18377 = G__18393;
i__17886_18378 = G__18394;
continue;
} else {
var vec__17905_18395 = cljs.core.first(seq__17883_18389__$1);
var script_tag_18396 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__17905_18395,(0),null);
var script_body_18397 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__17905_18395,(1),null);
eval(script_body_18397);


var G__18398 = cljs.core.next(seq__17883_18389__$1);
var G__18399 = null;
var G__18400 = (0);
var G__18401 = (0);
seq__17883_18375 = G__18398;
chunk__17884_18376 = G__18399;
count__17885_18377 = G__18400;
i__17886_18378 = G__18401;
continue;
}
} else {
}
}
break;
}

return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (s__$1,p__17909){
var vec__17910 = p__17909;
var script_tag = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__17910,(0),null);
var script_body = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__17910,(1),null);
return clojure.string.replace(s__$1,script_tag,"");
}),s,scripts);
});
shadow.dom.str__GT_fragment = (function shadow$dom$str__GT_fragment(s){
var el = document.createElement("div");
(el.innerHTML = s);

return (new shadow.dom.NativeColl(goog.dom.childrenToNode_(document,el)));
});
shadow.dom.node_name = (function shadow$dom$node_name(el){
return shadow.dom.dom_node(el).nodeName;
});
shadow.dom.ancestor_by_class = (function shadow$dom$ancestor_by_class(el,cls){
return goog.dom.getAncestorByClass(shadow.dom.dom_node(el),cls);
});
shadow.dom.ancestor_by_tag = (function shadow$dom$ancestor_by_tag(var_args){
var G__17924 = arguments.length;
switch (G__17924) {
case 2:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2 = (function (el,tag){
return goog.dom.getAncestorByTagNameAndClass(shadow.dom.dom_node(el),cljs.core.name(tag));
}));

(shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3 = (function (el,tag,cls){
return goog.dom.getAncestorByTagNameAndClass(shadow.dom.dom_node(el),cljs.core.name(tag),cljs.core.name(cls));
}));

(shadow.dom.ancestor_by_tag.cljs$lang$maxFixedArity = 3);

shadow.dom.get_value = (function shadow$dom$get_value(dom){
return goog.dom.forms.getValue(shadow.dom.dom_node(dom));
});
shadow.dom.set_value = (function shadow$dom$set_value(dom,value){
return goog.dom.forms.setValue(shadow.dom.dom_node(dom),value);
});
shadow.dom.px = (function shadow$dom$px(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1((value | (0))),"px"].join('');
});
shadow.dom.pct = (function shadow$dom$pct(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(value),"%"].join('');
});
shadow.dom.remove_style_STAR_ = (function shadow$dom$remove_style_STAR_(el,style){
return el.style.removeProperty(cljs.core.name(style));
});
shadow.dom.remove_style = (function shadow$dom$remove_style(el,style){
var el__$1 = shadow.dom.dom_node(el);
return shadow.dom.remove_style_STAR_(el__$1,style);
});
shadow.dom.remove_styles = (function shadow$dom$remove_styles(el,style_keys){
var el__$1 = shadow.dom.dom_node(el);
var seq__17934 = cljs.core.seq(style_keys);
var chunk__17936 = null;
var count__17937 = (0);
var i__17938 = (0);
while(true){
if((i__17938 < count__17937)){
var it = chunk__17936.cljs$core$IIndexed$_nth$arity$2(null,i__17938);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__18407 = seq__17934;
var G__18408 = chunk__17936;
var G__18409 = count__17937;
var G__18410 = (i__17938 + (1));
seq__17934 = G__18407;
chunk__17936 = G__18408;
count__17937 = G__18409;
i__17938 = G__18410;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__17934);
if(temp__5804__auto__){
var seq__17934__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__17934__$1)){
var c__5568__auto__ = cljs.core.chunk_first(seq__17934__$1);
var G__18412 = cljs.core.chunk_rest(seq__17934__$1);
var G__18413 = c__5568__auto__;
var G__18414 = cljs.core.count(c__5568__auto__);
var G__18415 = (0);
seq__17934 = G__18412;
chunk__17936 = G__18413;
count__17937 = G__18414;
i__17938 = G__18415;
continue;
} else {
var it = cljs.core.first(seq__17934__$1);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__18417 = cljs.core.next(seq__17934__$1);
var G__18418 = null;
var G__18419 = (0);
var G__18420 = (0);
seq__17934 = G__18417;
chunk__17936 = G__18418;
count__17937 = G__18419;
i__17938 = G__18420;
continue;
}
} else {
return null;
}
}
break;
}
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Coordinate = (function (x,y,__meta,__extmap,__hash){
this.x = x;
this.y = y;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
(shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__5343__auto__,k__5344__auto__){
var self__ = this;
var this__5343__auto____$1 = this;
return this__5343__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__5344__auto__,null);
}));

(shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__5345__auto__,k17954,else__5346__auto__){
var self__ = this;
var this__5345__auto____$1 = this;
var G__17963 = k17954;
var G__17963__$1 = (((G__17963 instanceof cljs.core.Keyword))?G__17963.fqn:null);
switch (G__17963__$1) {
case "x":
return self__.x;

break;
case "y":
return self__.y;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k17954,else__5346__auto__);

}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__5363__auto__,f__5364__auto__,init__5365__auto__){
var self__ = this;
var this__5363__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ret__5366__auto__,p__17968){
var vec__17969 = p__17968;
var k__5367__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__17969,(0),null);
var v__5368__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__17969,(1),null);
return (f__5364__auto__.cljs$core$IFn$_invoke$arity$3 ? f__5364__auto__.cljs$core$IFn$_invoke$arity$3(ret__5366__auto__,k__5367__auto__,v__5368__auto__) : f__5364__auto__.call(null,ret__5366__auto__,k__5367__auto__,v__5368__auto__));
}),init__5365__auto__,this__5363__auto____$1);
}));

(shadow.dom.Coordinate.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__5358__auto__,writer__5359__auto__,opts__5360__auto__){
var self__ = this;
var this__5358__auto____$1 = this;
var pr_pair__5361__auto__ = (function (keyval__5362__auto__){
return cljs.core.pr_sequential_writer(writer__5359__auto__,cljs.core.pr_writer,""," ","",opts__5360__auto__,keyval__5362__auto__);
});
return cljs.core.pr_sequential_writer(writer__5359__auto__,pr_pair__5361__auto__,"#shadow.dom.Coordinate{",", ","}",opts__5360__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"x","x",2099068185),self__.x],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"y","y",-1757859776),self__.y],null))], null),self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__17953){
var self__ = this;
var G__17953__$1 = this;
return (new cljs.core.RecordIter((0),G__17953__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"x","x",2099068185),new cljs.core.Keyword(null,"y","y",-1757859776)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__5341__auto__){
var self__ = this;
var this__5341__auto____$1 = this;
return self__.__meta;
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__5338__auto__){
var self__ = this;
var this__5338__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,self__.__extmap,self__.__hash));
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__5347__auto__){
var self__ = this;
var this__5347__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__5339__auto__){
var self__ = this;
var this__5339__auto____$1 = this;
var h__5154__auto__ = self__.__hash;
if((!((h__5154__auto__ == null)))){
return h__5154__auto__;
} else {
var h__5154__auto____$1 = (function (coll__5340__auto__){
return (145542109 ^ cljs.core.hash_unordered_coll(coll__5340__auto__));
})(this__5339__auto____$1);
(self__.__hash = h__5154__auto____$1);

return h__5154__auto____$1;
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this17955,other17956){
var self__ = this;
var this17955__$1 = this;
return (((!((other17956 == null)))) && ((((this17955__$1.constructor === other17956.constructor)) && (((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this17955__$1.x,other17956.x)) && (((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this17955__$1.y,other17956.y)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this17955__$1.__extmap,other17956.__extmap)))))))));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__5353__auto__,k__5354__auto__){
var self__ = this;
var this__5353__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"y","y",-1757859776),null,new cljs.core.Keyword(null,"x","x",2099068185),null], null), null),k__5354__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__5353__auto____$1),self__.__meta),k__5354__auto__);
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__5354__auto__)),null));
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IAssociative$_contains_key_QMARK_$arity$2 = (function (this__5350__auto__,k17954){
var self__ = this;
var this__5350__auto____$1 = this;
var G__17985 = k17954;
var G__17985__$1 = (((G__17985 instanceof cljs.core.Keyword))?G__17985.fqn:null);
switch (G__17985__$1) {
case "x":
case "y":
return true;

break;
default:
return cljs.core.contains_QMARK_(self__.__extmap,k17954);

}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__5351__auto__,k__5352__auto__,G__17953){
var self__ = this;
var this__5351__auto____$1 = this;
var pred__17988 = cljs.core.keyword_identical_QMARK_;
var expr__17989 = k__5352__auto__;
if(cljs.core.truth_((pred__17988.cljs$core$IFn$_invoke$arity$2 ? pred__17988.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"x","x",2099068185),expr__17989) : pred__17988.call(null,new cljs.core.Keyword(null,"x","x",2099068185),expr__17989)))){
return (new shadow.dom.Coordinate(G__17953,self__.y,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((pred__17988.cljs$core$IFn$_invoke$arity$2 ? pred__17988.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"y","y",-1757859776),expr__17989) : pred__17988.call(null,new cljs.core.Keyword(null,"y","y",-1757859776),expr__17989)))){
return (new shadow.dom.Coordinate(self__.x,G__17953,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__5352__auto__,G__17953),null));
}
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__5356__auto__){
var self__ = this;
var this__5356__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"x","x",2099068185),self__.x,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"y","y",-1757859776),self__.y,null))], null),self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__5342__auto__,G__17953){
var self__ = this;
var this__5342__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,G__17953,self__.__extmap,self__.__hash));
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__5348__auto__,entry__5349__auto__){
var self__ = this;
var this__5348__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__5349__auto__)){
return this__5348__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth(entry__5349__auto__,(0)),cljs.core._nth(entry__5349__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__5348__auto____$1,entry__5349__auto__);
}
}));

(shadow.dom.Coordinate.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"x","x",-555367584,null),new cljs.core.Symbol(null,"y","y",-117328249,null)], null);
}));

(shadow.dom.Coordinate.cljs$lang$type = true);

(shadow.dom.Coordinate.cljs$lang$ctorPrSeq = (function (this__5389__auto__){
return (new cljs.core.List(null,"shadow.dom/Coordinate",null,(1),null));
}));

(shadow.dom.Coordinate.cljs$lang$ctorPrWriter = (function (this__5389__auto__,writer__5390__auto__){
return cljs.core._write(writer__5390__auto__,"shadow.dom/Coordinate");
}));

/**
 * Positional factory function for shadow.dom/Coordinate.
 */
shadow.dom.__GT_Coordinate = (function shadow$dom$__GT_Coordinate(x,y){
return (new shadow.dom.Coordinate(x,y,null,null,null));
});

/**
 * Factory function for shadow.dom/Coordinate, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Coordinate = (function shadow$dom$map__GT_Coordinate(G__17959){
var extmap__5385__auto__ = (function (){var G__18000 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__17959,new cljs.core.Keyword(null,"x","x",2099068185),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"y","y",-1757859776)], 0));
if(cljs.core.record_QMARK_(G__17959)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__18000);
} else {
return G__18000;
}
})();
return (new shadow.dom.Coordinate(new cljs.core.Keyword(null,"x","x",2099068185).cljs$core$IFn$_invoke$arity$1(G__17959),new cljs.core.Keyword(null,"y","y",-1757859776).cljs$core$IFn$_invoke$arity$1(G__17959),null,cljs.core.not_empty(extmap__5385__auto__),null));
});

shadow.dom.get_position = (function shadow$dom$get_position(el){
var pos = goog.style.getPosition(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_client_position = (function shadow$dom$get_client_position(el){
var pos = goog.style.getClientPosition(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_page_offset = (function shadow$dom$get_page_offset(el){
var pos = goog.style.getPageOffset(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Size = (function (w,h,__meta,__extmap,__hash){
this.w = w;
this.h = h;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
(shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__5343__auto__,k__5344__auto__){
var self__ = this;
var this__5343__auto____$1 = this;
return this__5343__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__5344__auto__,null);
}));

(shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__5345__auto__,k18002,else__5346__auto__){
var self__ = this;
var this__5345__auto____$1 = this;
var G__18006 = k18002;
var G__18006__$1 = (((G__18006 instanceof cljs.core.Keyword))?G__18006.fqn:null);
switch (G__18006__$1) {
case "w":
return self__.w;

break;
case "h":
return self__.h;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k18002,else__5346__auto__);

}
}));

(shadow.dom.Size.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__5363__auto__,f__5364__auto__,init__5365__auto__){
var self__ = this;
var this__5363__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ret__5366__auto__,p__18007){
var vec__18008 = p__18007;
var k__5367__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__18008,(0),null);
var v__5368__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__18008,(1),null);
return (f__5364__auto__.cljs$core$IFn$_invoke$arity$3 ? f__5364__auto__.cljs$core$IFn$_invoke$arity$3(ret__5366__auto__,k__5367__auto__,v__5368__auto__) : f__5364__auto__.call(null,ret__5366__auto__,k__5367__auto__,v__5368__auto__));
}),init__5365__auto__,this__5363__auto____$1);
}));

(shadow.dom.Size.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__5358__auto__,writer__5359__auto__,opts__5360__auto__){
var self__ = this;
var this__5358__auto____$1 = this;
var pr_pair__5361__auto__ = (function (keyval__5362__auto__){
return cljs.core.pr_sequential_writer(writer__5359__auto__,cljs.core.pr_writer,""," ","",opts__5360__auto__,keyval__5362__auto__);
});
return cljs.core.pr_sequential_writer(writer__5359__auto__,pr_pair__5361__auto__,"#shadow.dom.Size{",", ","}",opts__5360__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"w","w",354169001),self__.w],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"h","h",1109658740),self__.h],null))], null),self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__18001){
var self__ = this;
var G__18001__$1 = this;
return (new cljs.core.RecordIter((0),G__18001__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"w","w",354169001),new cljs.core.Keyword(null,"h","h",1109658740)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
}));

(shadow.dom.Size.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__5341__auto__){
var self__ = this;
var this__5341__auto____$1 = this;
return self__.__meta;
}));

(shadow.dom.Size.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__5338__auto__){
var self__ = this;
var this__5338__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,self__.__extmap,self__.__hash));
}));

(shadow.dom.Size.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__5347__auto__){
var self__ = this;
var this__5347__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__5339__auto__){
var self__ = this;
var this__5339__auto____$1 = this;
var h__5154__auto__ = self__.__hash;
if((!((h__5154__auto__ == null)))){
return h__5154__auto__;
} else {
var h__5154__auto____$1 = (function (coll__5340__auto__){
return (-1228019642 ^ cljs.core.hash_unordered_coll(coll__5340__auto__));
})(this__5339__auto____$1);
(self__.__hash = h__5154__auto____$1);

return h__5154__auto____$1;
}
}));

(shadow.dom.Size.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this18003,other18004){
var self__ = this;
var this18003__$1 = this;
return (((!((other18004 == null)))) && ((((this18003__$1.constructor === other18004.constructor)) && (((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this18003__$1.w,other18004.w)) && (((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this18003__$1.h,other18004.h)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this18003__$1.__extmap,other18004.__extmap)))))))));
}));

(shadow.dom.Size.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__5353__auto__,k__5354__auto__){
var self__ = this;
var this__5353__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"w","w",354169001),null,new cljs.core.Keyword(null,"h","h",1109658740),null], null), null),k__5354__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__5353__auto____$1),self__.__meta),k__5354__auto__);
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__5354__auto__)),null));
}
}));

(shadow.dom.Size.prototype.cljs$core$IAssociative$_contains_key_QMARK_$arity$2 = (function (this__5350__auto__,k18002){
var self__ = this;
var this__5350__auto____$1 = this;
var G__18015 = k18002;
var G__18015__$1 = (((G__18015 instanceof cljs.core.Keyword))?G__18015.fqn:null);
switch (G__18015__$1) {
case "w":
case "h":
return true;

break;
default:
return cljs.core.contains_QMARK_(self__.__extmap,k18002);

}
}));

(shadow.dom.Size.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__5351__auto__,k__5352__auto__,G__18001){
var self__ = this;
var this__5351__auto____$1 = this;
var pred__18016 = cljs.core.keyword_identical_QMARK_;
var expr__18017 = k__5352__auto__;
if(cljs.core.truth_((pred__18016.cljs$core$IFn$_invoke$arity$2 ? pred__18016.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"w","w",354169001),expr__18017) : pred__18016.call(null,new cljs.core.Keyword(null,"w","w",354169001),expr__18017)))){
return (new shadow.dom.Size(G__18001,self__.h,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((pred__18016.cljs$core$IFn$_invoke$arity$2 ? pred__18016.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"h","h",1109658740),expr__18017) : pred__18016.call(null,new cljs.core.Keyword(null,"h","h",1109658740),expr__18017)))){
return (new shadow.dom.Size(self__.w,G__18001,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__5352__auto__,G__18001),null));
}
}
}));

(shadow.dom.Size.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__5356__auto__){
var self__ = this;
var this__5356__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"w","w",354169001),self__.w,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"h","h",1109658740),self__.h,null))], null),self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__5342__auto__,G__18001){
var self__ = this;
var this__5342__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,G__18001,self__.__extmap,self__.__hash));
}));

(shadow.dom.Size.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__5348__auto__,entry__5349__auto__){
var self__ = this;
var this__5348__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__5349__auto__)){
return this__5348__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth(entry__5349__auto__,(0)),cljs.core._nth(entry__5349__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__5348__auto____$1,entry__5349__auto__);
}
}));

(shadow.dom.Size.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"w","w",1994700528,null),new cljs.core.Symbol(null,"h","h",-1544777029,null)], null);
}));

(shadow.dom.Size.cljs$lang$type = true);

(shadow.dom.Size.cljs$lang$ctorPrSeq = (function (this__5389__auto__){
return (new cljs.core.List(null,"shadow.dom/Size",null,(1),null));
}));

(shadow.dom.Size.cljs$lang$ctorPrWriter = (function (this__5389__auto__,writer__5390__auto__){
return cljs.core._write(writer__5390__auto__,"shadow.dom/Size");
}));

/**
 * Positional factory function for shadow.dom/Size.
 */
shadow.dom.__GT_Size = (function shadow$dom$__GT_Size(w,h){
return (new shadow.dom.Size(w,h,null,null,null));
});

/**
 * Factory function for shadow.dom/Size, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Size = (function shadow$dom$map__GT_Size(G__18005){
var extmap__5385__auto__ = (function (){var G__18019 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__18005,new cljs.core.Keyword(null,"w","w",354169001),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"h","h",1109658740)], 0));
if(cljs.core.record_QMARK_(G__18005)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__18019);
} else {
return G__18019;
}
})();
return (new shadow.dom.Size(new cljs.core.Keyword(null,"w","w",354169001).cljs$core$IFn$_invoke$arity$1(G__18005),new cljs.core.Keyword(null,"h","h",1109658740).cljs$core$IFn$_invoke$arity$1(G__18005),null,cljs.core.not_empty(extmap__5385__auto__),null));
});

shadow.dom.size__GT_clj = (function shadow$dom$size__GT_clj(size){
return (new shadow.dom.Size(size.width,size.height,null,null,null));
});
shadow.dom.get_size = (function shadow$dom$get_size(el){
return shadow.dom.size__GT_clj(goog.style.getSize(shadow.dom.dom_node(el)));
});
shadow.dom.get_height = (function shadow$dom$get_height(el){
return shadow.dom.get_size(el).h;
});
shadow.dom.get_viewport_size = (function shadow$dom$get_viewport_size(){
return shadow.dom.size__GT_clj(goog.dom.getViewportSize());
});
shadow.dom.first_child = (function shadow$dom$first_child(el){
return (shadow.dom.dom_node(el).children[(0)]);
});
shadow.dom.select_option_values = (function shadow$dom$select_option_values(el){
var native$ = shadow.dom.dom_node(el);
var opts = (native$["options"]);
var a__5633__auto__ = opts;
var l__5634__auto__ = a__5633__auto__.length;
var i = (0);
var ret = cljs.core.PersistentVector.EMPTY;
while(true){
if((i < l__5634__auto__)){
var G__18436 = (i + (1));
var G__18437 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,(opts[i]["value"]));
i = G__18436;
ret = G__18437;
continue;
} else {
return ret;
}
break;
}
});
shadow.dom.build_url = (function shadow$dom$build_url(path,query_params){
if(cljs.core.empty_QMARK_(query_params)){
return path;
} else {
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(path),"?",clojure.string.join.cljs$core$IFn$_invoke$arity$2("&",cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p__18024){
var vec__18025 = p__18024;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__18025,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__18025,(1),null);
return [cljs.core.name(k),"=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(encodeURIComponent(cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)))].join('');
}),query_params))].join('');
}
});
shadow.dom.redirect = (function shadow$dom$redirect(var_args){
var G__18029 = arguments.length;
switch (G__18029) {
case 1:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1 = (function (path){
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2(path,cljs.core.PersistentArrayMap.EMPTY);
}));

(shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2 = (function (path,query_params){
return (document["location"]["href"] = shadow.dom.build_url(path,query_params));
}));

(shadow.dom.redirect.cljs$lang$maxFixedArity = 2);

shadow.dom.reload_BANG_ = (function shadow$dom$reload_BANG_(){
return (document.location.href = document.location.href);
});
shadow.dom.tag_name = (function shadow$dom$tag_name(el){
var dom = shadow.dom.dom_node(el);
return dom.tagName;
});
shadow.dom.insert_after = (function shadow$dom$insert_after(ref,new$){
var new_node = shadow.dom.dom_node(new$);
goog.dom.insertSiblingAfter(new_node,shadow.dom.dom_node(ref));

return new_node;
});
shadow.dom.insert_before = (function shadow$dom$insert_before(ref,new$){
var new_node = shadow.dom.dom_node(new$);
goog.dom.insertSiblingBefore(new_node,shadow.dom.dom_node(ref));

return new_node;
});
shadow.dom.insert_first = (function shadow$dom$insert_first(ref,new$){
var temp__5802__auto__ = shadow.dom.dom_node(ref).firstChild;
if(cljs.core.truth_(temp__5802__auto__)){
var child = temp__5802__auto__;
return shadow.dom.insert_before(child,new$);
} else {
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2(ref,new$);
}
});
shadow.dom.index_of = (function shadow$dom$index_of(el){
var el__$1 = shadow.dom.dom_node(el);
var i = (0);
while(true){
var ps = el__$1.previousSibling;
if((ps == null)){
return i;
} else {
var G__18442 = ps;
var G__18443 = (i + (1));
el__$1 = G__18442;
i = G__18443;
continue;
}
break;
}
});
shadow.dom.get_parent = (function shadow$dom$get_parent(el){
return goog.dom.getParentElement(shadow.dom.dom_node(el));
});
shadow.dom.parents = (function shadow$dom$parents(el){
var parent = shadow.dom.get_parent(el);
if(cljs.core.truth_(parent)){
return cljs.core.cons(parent,(new cljs.core.LazySeq(null,(function (){
return (shadow.dom.parents.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.parents.cljs$core$IFn$_invoke$arity$1(parent) : shadow.dom.parents.call(null,parent));
}),null,null)));
} else {
return null;
}
});
shadow.dom.matches = (function shadow$dom$matches(el,sel){
return shadow.dom.dom_node(el).matches(sel);
});
shadow.dom.get_next_sibling = (function shadow$dom$get_next_sibling(el){
return goog.dom.getNextElementSibling(shadow.dom.dom_node(el));
});
shadow.dom.get_previous_sibling = (function shadow$dom$get_previous_sibling(el){
return goog.dom.getPreviousElementSibling(shadow.dom.dom_node(el));
});
shadow.dom.xmlns = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 2, ["svg","http://www.w3.org/2000/svg","xlink","http://www.w3.org/1999/xlink"], null));
shadow.dom.create_svg_node = (function shadow$dom$create_svg_node(tag_def,props){
var vec__18042 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__18042,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__18042,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__18042,(2),null);
var el = document.createElementNS("http://www.w3.org/2000/svg",tag_name);
if(cljs.core.truth_(tag_id)){
el.setAttribute("id",tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
el.setAttribute("class",shadow.dom.merge_class_string(new cljs.core.Keyword(null,"class","class",-2030961996).cljs$core$IFn$_invoke$arity$1(props),tag_classes));
} else {
}

var seq__18046_18444 = cljs.core.seq(props);
var chunk__18047_18445 = null;
var count__18048_18446 = (0);
var i__18049_18447 = (0);
while(true){
if((i__18049_18447 < count__18048_18446)){
var vec__18065_18448 = chunk__18047_18445.cljs$core$IIndexed$_nth$arity$2(null,i__18049_18447);
var k_18449 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__18065_18448,(0),null);
var v_18450 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__18065_18448,(1),null);
el.setAttributeNS((function (){var temp__5804__auto__ = cljs.core.namespace(k_18449);
if(cljs.core.truth_(temp__5804__auto__)){
var ns = temp__5804__auto__;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_18449),v_18450);


var G__18451 = seq__18046_18444;
var G__18452 = chunk__18047_18445;
var G__18453 = count__18048_18446;
var G__18454 = (i__18049_18447 + (1));
seq__18046_18444 = G__18451;
chunk__18047_18445 = G__18452;
count__18048_18446 = G__18453;
i__18049_18447 = G__18454;
continue;
} else {
var temp__5804__auto___18455 = cljs.core.seq(seq__18046_18444);
if(temp__5804__auto___18455){
var seq__18046_18459__$1 = temp__5804__auto___18455;
if(cljs.core.chunked_seq_QMARK_(seq__18046_18459__$1)){
var c__5568__auto___18460 = cljs.core.chunk_first(seq__18046_18459__$1);
var G__18461 = cljs.core.chunk_rest(seq__18046_18459__$1);
var G__18462 = c__5568__auto___18460;
var G__18463 = cljs.core.count(c__5568__auto___18460);
var G__18464 = (0);
seq__18046_18444 = G__18461;
chunk__18047_18445 = G__18462;
count__18048_18446 = G__18463;
i__18049_18447 = G__18464;
continue;
} else {
var vec__18070_18465 = cljs.core.first(seq__18046_18459__$1);
var k_18466 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__18070_18465,(0),null);
var v_18467 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__18070_18465,(1),null);
el.setAttributeNS((function (){var temp__5804__auto____$1 = cljs.core.namespace(k_18466);
if(cljs.core.truth_(temp__5804__auto____$1)){
var ns = temp__5804__auto____$1;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_18466),v_18467);


var G__18468 = cljs.core.next(seq__18046_18459__$1);
var G__18469 = null;
var G__18470 = (0);
var G__18471 = (0);
seq__18046_18444 = G__18468;
chunk__18047_18445 = G__18469;
count__18048_18446 = G__18470;
i__18049_18447 = G__18471;
continue;
}
} else {
}
}
break;
}

return el;
});
shadow.dom.svg_node = (function shadow$dom$svg_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$SVGElement$))))?true:false):false)){
return el.shadow$dom$SVGElement$_to_svg$arity$1(null);
} else {
return el;

}
}
});
shadow.dom.make_svg_node = (function shadow$dom$make_svg_node(structure){
var vec__18074 = shadow.dom.destructure_node(shadow.dom.create_svg_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__18074,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__18074,(1),null);
var seq__18077_18472 = cljs.core.seq(node_children);
var chunk__18079_18473 = null;
var count__18080_18474 = (0);
var i__18081_18475 = (0);
while(true){
if((i__18081_18475 < count__18080_18474)){
var child_struct_18476 = chunk__18079_18473.cljs$core$IIndexed$_nth$arity$2(null,i__18081_18475);
if((!((child_struct_18476 == null)))){
if(typeof child_struct_18476 === 'string'){
var text_18477 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_18477),child_struct_18476].join(''));
} else {
var children_18478 = shadow.dom.svg_node(child_struct_18476);
if(cljs.core.seq_QMARK_(children_18478)){
var seq__18143_18479 = cljs.core.seq(children_18478);
var chunk__18145_18480 = null;
var count__18146_18481 = (0);
var i__18147_18482 = (0);
while(true){
if((i__18147_18482 < count__18146_18481)){
var child_18483 = chunk__18145_18480.cljs$core$IIndexed$_nth$arity$2(null,i__18147_18482);
if(cljs.core.truth_(child_18483)){
node.appendChild(child_18483);


var G__18484 = seq__18143_18479;
var G__18485 = chunk__18145_18480;
var G__18486 = count__18146_18481;
var G__18487 = (i__18147_18482 + (1));
seq__18143_18479 = G__18484;
chunk__18145_18480 = G__18485;
count__18146_18481 = G__18486;
i__18147_18482 = G__18487;
continue;
} else {
var G__18488 = seq__18143_18479;
var G__18489 = chunk__18145_18480;
var G__18490 = count__18146_18481;
var G__18491 = (i__18147_18482 + (1));
seq__18143_18479 = G__18488;
chunk__18145_18480 = G__18489;
count__18146_18481 = G__18490;
i__18147_18482 = G__18491;
continue;
}
} else {
var temp__5804__auto___18492 = cljs.core.seq(seq__18143_18479);
if(temp__5804__auto___18492){
var seq__18143_18494__$1 = temp__5804__auto___18492;
if(cljs.core.chunked_seq_QMARK_(seq__18143_18494__$1)){
var c__5568__auto___18497 = cljs.core.chunk_first(seq__18143_18494__$1);
var G__18498 = cljs.core.chunk_rest(seq__18143_18494__$1);
var G__18499 = c__5568__auto___18497;
var G__18500 = cljs.core.count(c__5568__auto___18497);
var G__18501 = (0);
seq__18143_18479 = G__18498;
chunk__18145_18480 = G__18499;
count__18146_18481 = G__18500;
i__18147_18482 = G__18501;
continue;
} else {
var child_18502 = cljs.core.first(seq__18143_18494__$1);
if(cljs.core.truth_(child_18502)){
node.appendChild(child_18502);


var G__18503 = cljs.core.next(seq__18143_18494__$1);
var G__18504 = null;
var G__18505 = (0);
var G__18506 = (0);
seq__18143_18479 = G__18503;
chunk__18145_18480 = G__18504;
count__18146_18481 = G__18505;
i__18147_18482 = G__18506;
continue;
} else {
var G__18507 = cljs.core.next(seq__18143_18494__$1);
var G__18508 = null;
var G__18509 = (0);
var G__18510 = (0);
seq__18143_18479 = G__18507;
chunk__18145_18480 = G__18508;
count__18146_18481 = G__18509;
i__18147_18482 = G__18510;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_18478);
}
}


var G__18511 = seq__18077_18472;
var G__18512 = chunk__18079_18473;
var G__18513 = count__18080_18474;
var G__18514 = (i__18081_18475 + (1));
seq__18077_18472 = G__18511;
chunk__18079_18473 = G__18512;
count__18080_18474 = G__18513;
i__18081_18475 = G__18514;
continue;
} else {
var G__18515 = seq__18077_18472;
var G__18516 = chunk__18079_18473;
var G__18517 = count__18080_18474;
var G__18518 = (i__18081_18475 + (1));
seq__18077_18472 = G__18515;
chunk__18079_18473 = G__18516;
count__18080_18474 = G__18517;
i__18081_18475 = G__18518;
continue;
}
} else {
var temp__5804__auto___18519 = cljs.core.seq(seq__18077_18472);
if(temp__5804__auto___18519){
var seq__18077_18520__$1 = temp__5804__auto___18519;
if(cljs.core.chunked_seq_QMARK_(seq__18077_18520__$1)){
var c__5568__auto___18521 = cljs.core.chunk_first(seq__18077_18520__$1);
var G__18522 = cljs.core.chunk_rest(seq__18077_18520__$1);
var G__18523 = c__5568__auto___18521;
var G__18524 = cljs.core.count(c__5568__auto___18521);
var G__18525 = (0);
seq__18077_18472 = G__18522;
chunk__18079_18473 = G__18523;
count__18080_18474 = G__18524;
i__18081_18475 = G__18525;
continue;
} else {
var child_struct_18526 = cljs.core.first(seq__18077_18520__$1);
if((!((child_struct_18526 == null)))){
if(typeof child_struct_18526 === 'string'){
var text_18527 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_18527),child_struct_18526].join(''));
} else {
var children_18528 = shadow.dom.svg_node(child_struct_18526);
if(cljs.core.seq_QMARK_(children_18528)){
var seq__18158_18529 = cljs.core.seq(children_18528);
var chunk__18160_18530 = null;
var count__18161_18531 = (0);
var i__18162_18532 = (0);
while(true){
if((i__18162_18532 < count__18161_18531)){
var child_18533 = chunk__18160_18530.cljs$core$IIndexed$_nth$arity$2(null,i__18162_18532);
if(cljs.core.truth_(child_18533)){
node.appendChild(child_18533);


var G__18534 = seq__18158_18529;
var G__18535 = chunk__18160_18530;
var G__18536 = count__18161_18531;
var G__18537 = (i__18162_18532 + (1));
seq__18158_18529 = G__18534;
chunk__18160_18530 = G__18535;
count__18161_18531 = G__18536;
i__18162_18532 = G__18537;
continue;
} else {
var G__18538 = seq__18158_18529;
var G__18539 = chunk__18160_18530;
var G__18540 = count__18161_18531;
var G__18541 = (i__18162_18532 + (1));
seq__18158_18529 = G__18538;
chunk__18160_18530 = G__18539;
count__18161_18531 = G__18540;
i__18162_18532 = G__18541;
continue;
}
} else {
var temp__5804__auto___18542__$1 = cljs.core.seq(seq__18158_18529);
if(temp__5804__auto___18542__$1){
var seq__18158_18543__$1 = temp__5804__auto___18542__$1;
if(cljs.core.chunked_seq_QMARK_(seq__18158_18543__$1)){
var c__5568__auto___18544 = cljs.core.chunk_first(seq__18158_18543__$1);
var G__18545 = cljs.core.chunk_rest(seq__18158_18543__$1);
var G__18546 = c__5568__auto___18544;
var G__18547 = cljs.core.count(c__5568__auto___18544);
var G__18548 = (0);
seq__18158_18529 = G__18545;
chunk__18160_18530 = G__18546;
count__18161_18531 = G__18547;
i__18162_18532 = G__18548;
continue;
} else {
var child_18549 = cljs.core.first(seq__18158_18543__$1);
if(cljs.core.truth_(child_18549)){
node.appendChild(child_18549);


var G__18550 = cljs.core.next(seq__18158_18543__$1);
var G__18551 = null;
var G__18552 = (0);
var G__18553 = (0);
seq__18158_18529 = G__18550;
chunk__18160_18530 = G__18551;
count__18161_18531 = G__18552;
i__18162_18532 = G__18553;
continue;
} else {
var G__18554 = cljs.core.next(seq__18158_18543__$1);
var G__18555 = null;
var G__18556 = (0);
var G__18557 = (0);
seq__18158_18529 = G__18554;
chunk__18160_18530 = G__18555;
count__18161_18531 = G__18556;
i__18162_18532 = G__18557;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_18528);
}
}


var G__18558 = cljs.core.next(seq__18077_18520__$1);
var G__18559 = null;
var G__18560 = (0);
var G__18561 = (0);
seq__18077_18472 = G__18558;
chunk__18079_18473 = G__18559;
count__18080_18474 = G__18560;
i__18081_18475 = G__18561;
continue;
} else {
var G__18562 = cljs.core.next(seq__18077_18520__$1);
var G__18563 = null;
var G__18564 = (0);
var G__18565 = (0);
seq__18077_18472 = G__18562;
chunk__18079_18473 = G__18563;
count__18080_18474 = G__18564;
i__18081_18475 = G__18565;
continue;
}
}
} else {
}
}
break;
}

return node;
});
(shadow.dom.SVGElement["string"] = true);

(shadow.dom._to_svg["string"] = (function (this$){
if((this$ instanceof cljs.core.Keyword)){
return shadow.dom.make_svg_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$], null));
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("strings cannot be in svgs",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"this","this",-611633625),this$], null));
}
}));

(cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_svg_node(this$__$1);
}));

(cljs.core.LazySeq.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.LazySeq.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_svg,this$__$1);
}));

(shadow.dom.SVGElement["null"] = true);

(shadow.dom._to_svg["null"] = (function (_){
return null;
}));
shadow.dom.svg = (function shadow$dom$svg(var_args){
var args__5775__auto__ = [];
var len__5769__auto___18566 = arguments.length;
var i__5770__auto___18567 = (0);
while(true){
if((i__5770__auto___18567 < len__5769__auto___18566)){
args__5775__auto__.push((arguments[i__5770__auto___18567]));

var G__18568 = (i__5770__auto___18567 + (1));
i__5770__auto___18567 = G__18568;
continue;
} else {
}
break;
}

var argseq__5776__auto__ = ((((1) < args__5775__auto__.length))?(new cljs.core.IndexedSeq(args__5775__auto__.slice((1)),(0),null)):null);
return shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__5776__auto__);
});

(shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic = (function (attrs,children){
return shadow.dom._to_svg(cljs.core.vec(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),attrs], null),children)));
}));

(shadow.dom.svg.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(shadow.dom.svg.cljs$lang$applyTo = (function (seq18181){
var G__18182 = cljs.core.first(seq18181);
var seq18181__$1 = cljs.core.next(seq18181);
var self__5754__auto__ = this;
return self__5754__auto__.cljs$core$IFn$_invoke$arity$variadic(G__18182,seq18181__$1);
}));

/**
 * returns a channel for events on el
 * transform-fn should be a (fn [e el] some-val) where some-val will be put on the chan
 * once-or-cleanup handles the removal of the event handler
 * - true: remove after one event
 * - false: never removed
 * - chan: remove on msg/close
 */
shadow.dom.event_chan = (function shadow$dom$event_chan(var_args){
var G__18184 = arguments.length;
switch (G__18184) {
case 2:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2 = (function (el,event){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,null,false);
}));

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3 = (function (el,event,xf){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,xf,false);
}));

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4 = (function (el,event,xf,once_or_cleanup){
var buf = cljs.core.async.sliding_buffer((1));
var chan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2(buf,xf);
var event_fn = (function shadow$dom$event_fn(e){
cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(chan,e);

if(once_or_cleanup === true){
shadow.dom.remove_event_handler(el,event,shadow$dom$event_fn);

return cljs.core.async.close_BANG_(chan);
} else {
return null;
}
});
shadow.dom.dom_listen(shadow.dom.dom_node(el),cljs.core.name(event),event_fn);

if(cljs.core.truth_((function (){var and__5043__auto__ = once_or_cleanup;
if(cljs.core.truth_(and__5043__auto__)){
return (!(once_or_cleanup === true));
} else {
return and__5043__auto__;
}
})())){
var c__15644__auto___18574 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__15645__auto__ = (function (){var switch__15400__auto__ = (function (state_18189){
var state_val_18190 = (state_18189[(1)]);
if((state_val_18190 === (1))){
var state_18189__$1 = state_18189;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_18189__$1,(2),once_or_cleanup);
} else {
if((state_val_18190 === (2))){
var inst_18186 = (state_18189[(2)]);
var inst_18187 = shadow.dom.remove_event_handler(el,event,event_fn);
var state_18189__$1 = (function (){var statearr_18191 = state_18189;
(statearr_18191[(7)] = inst_18186);

return statearr_18191;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_18189__$1,inst_18187);
} else {
return null;
}
}
});
return (function() {
var shadow$dom$state_machine__15401__auto__ = null;
var shadow$dom$state_machine__15401__auto____0 = (function (){
var statearr_18192 = [null,null,null,null,null,null,null,null];
(statearr_18192[(0)] = shadow$dom$state_machine__15401__auto__);

(statearr_18192[(1)] = (1));

return statearr_18192;
});
var shadow$dom$state_machine__15401__auto____1 = (function (state_18189){
while(true){
var ret_value__15402__auto__ = (function (){try{while(true){
var result__15403__auto__ = switch__15400__auto__(state_18189);
if(cljs.core.keyword_identical_QMARK_(result__15403__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__15403__auto__;
}
break;
}
}catch (e18193){var ex__15404__auto__ = e18193;
var statearr_18194_18575 = state_18189;
(statearr_18194_18575[(2)] = ex__15404__auto__);


if(cljs.core.seq((state_18189[(4)]))){
var statearr_18195_18576 = state_18189;
(statearr_18195_18576[(1)] = cljs.core.first((state_18189[(4)])));

} else {
throw ex__15404__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__15402__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__18577 = state_18189;
state_18189 = G__18577;
continue;
} else {
return ret_value__15402__auto__;
}
break;
}
});
shadow$dom$state_machine__15401__auto__ = function(state_18189){
switch(arguments.length){
case 0:
return shadow$dom$state_machine__15401__auto____0.call(this);
case 1:
return shadow$dom$state_machine__15401__auto____1.call(this,state_18189);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
shadow$dom$state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$0 = shadow$dom$state_machine__15401__auto____0;
shadow$dom$state_machine__15401__auto__.cljs$core$IFn$_invoke$arity$1 = shadow$dom$state_machine__15401__auto____1;
return shadow$dom$state_machine__15401__auto__;
})()
})();
var state__15646__auto__ = (function (){var statearr_18196 = f__15645__auto__();
(statearr_18196[(6)] = c__15644__auto___18574);

return statearr_18196;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__15646__auto__);
}));

} else {
}

return chan;
}));

(shadow.dom.event_chan.cljs$lang$maxFixedArity = 4);


//# sourceMappingURL=shadow.dom.js.map
